<?php

namespace common\components;

use yii\base\Component;
use yii\helpers\Json;
use common\models\faregrabbr\FlightSearchForm;
use common\models\faregrabbr\HotelSearchForm;
use common\models\faregrabbr\Flight;
use common\models\faregrabbr\Hotel;
use common\models\faregrabbr\HotelDetails;
use common\models\faregrabbr\Payment;
use common\models\faregrabbr\Rate;
use common\models\faregrabbr\HotelBookRequest;
use common\models\User;
use Yii;

class Faregrabbr extends Component {

    public $baseUrl;
    public $commission;

    public $flightSearchUrl;
    public $flightBookUrl;
    public $hotelSearchUrl;
    public $hotelDetailsUrl;
    public $hotelBookUrl;
    public $tripSearchUrl;
    public $remoteAutoCompletePlacesUrl;

    public function getFlights(FlightSearchForm $flightSearchForm)
    {
        $url = $this->baseUrl.$this->flightSearchUrl;
        $query_data = $flightSearchForm;
        $query_data = Json::encode($query_data);
		$search = Yii::$app->faregrabbrApi->query($url, $query_data);
		if($search=="" || $search == NULL || !isset($search) || $search=="\n") return "";
		$result = Json::decode($search); 
        return $result;
    }

    public function bookFlight(Flight $flight, Payment $payment, $passengers)
    {
        $url = $this->baseUrl.$this->flightBookUrl;
        $query_data['flight'] = $flight;
        $query_data['payment'] = $payment;
        $query_data['commission'] = $this->commission;
        $query_data['passenger'] = $passengers;
        $query_data = Json::encode($query_data);
        $result = Json::decode(Yii::$app->faregrabbrApi->curlQuery($url, $query_data)); 
        return $result;
    }

    public function getHotels(HotelSearchForm $hotelSearchForm)
    {
        $url = $this->baseUrl.$this->hotelSearchUrl;
        $query_data = $hotelSearchForm;
        $query_data = Json::encode($query_data);        
        $result = Json::decode(Yii::$app->faregrabbrApi->query($url, $query_data)); 
        return $result;
    }
	
	public function loadNextHotels(HotelSearchForm $hotelSearchForm, $nextRequestReference)
    {
        $url = $this->baseUrl.$this->hotelSearchUrl;
        $query_data = $hotelSearchForm;
        $query_data = Json::encode($query_data);
		$query_data = Json::decode($query_data);
		$query_data['nextRequestReference'] = $nextRequestReference;
		$query_data = Json::encode($query_data);
        $result = Json::decode(Yii::$app->faregrabbrApi->query($url, $query_data)); 
        return $result;
    }

    public function getDetails($hotelCode, $hotelChain, $checkin, $numrooms, $checkout)
    {
        $url = $this->baseUrl.$this->hotelDetailsUrl;
        $query_data['hotelCode'] = $hotelCode;
        $query_data['hotelChain'] = $hotelChain;
        $query_data['checkin'] = $checkin;
        $query_data['numrooms'] = (int)$numrooms;
        $query_data['checkout'] = $checkout;
        $query_data = Json::encode($query_data);
        $result = Json::decode(Yii::$app->faregrabbrApi->query($url, $query_data)); 
        return $result;
    }

    public function bookHotel(HotelBookRequest $hotelBookRequest)
    {
        $url = $this->baseUrl.$this->hotelBookUrl;
        $hotelBookRequest->commission = $this->commission;
        $query_data = Json::encode($hotelBookRequest);
        $result = Json::decode(Yii::$app->faregrabbrApi->curlQuery($url, $query_data)); 
        return $result;
    }

    public function getTrips(User $user)
    {
        $result = Json::decode(); 
        return $this->baseUrl.$this->tripSearchUrl;
    }

    public function autoCompletePlaces($text)
    {
        $url = $this->baseUrl.$this->remoteAutoCompletePlacesUrl."?text=".$text;
        $result = Json::decode(Yii::$app->faregrabbrApi->query($url, null));
        return $result;

    }

    public function cacheKey($object)
    {
        $string = (string)Yii::$app->user->id;
        foreach ($object as $key => $value) {
            $string .= "-".$value;
        }
        return $string;
    }

}