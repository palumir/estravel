<?php

namespace common\adapters;
use Yii;

class GoogleMapsDistanceMatrix {

    public static function getDistanceList($address, $addressList)
    {
		for($i = 0; $i < count($addressList); $i++) {
			$addressList[$i] = urlencode($addressList[$i]);
		}
		
		$response = array();
		
		// Split into requests of size 25 (max Google allows) and merge.
		$counter = 0;
		while(count($addressList) > 0) {
			
			// Build new array pop off our input array
			$searchArray = array();
			for($i = 0; $i < 25 && $i < count($addressList); $i++) {
				$searchArray[] = array_pop($addressList);
			}
			
			$addressListString = implode($searchArray, "|");
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . urlencode($address) . "&destinations=" . $addressListString . "&key=AIzaSyC5RHGssv-NWV2-iua3sVyeoY7MHAMPh3A");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$currResponse = json_decode(curl_exec($ch),1);
			curl_close($ch);
			if($counter == 0) $response = $currResponse;
			else $response["rows"][0]["elements"] = array_merge($response["rows"][0]["elements"], $currResponse["rows"][0]["elements"]);
			$counter++;
		}
		
		
		return $response;
    }


}