<?php

namespace common\adapters;

use yii\base\Model;
use common\adapters\FaregrabbrAdapter;

// class FaregrabbrFlightBooking extends FaregrabbrAdapter
// {

//     public $url;

//     public function __construct($flight, $payment, $passenger)
//     {
//         $this->url = parent::REMOTE_FLIGHT_BOOKING_URL;
//         $this->setRequestParams($flight, $payment, $passenger);

//         if ($this->validate() && $this->queryFaregrabbr($this->url)) {
//             $this->FareDataObj = $this->data['FareDataObj'];
//             $this->Passengers = $this->data['Passengers'];
//             $this->RecordLocators = $this->data['RecordLocators'];
//             $this->status = $this->data['status'];
//             $this->statustext = $this->data['statustext'];
//             $this->PricingKey = $this->data['PricingKey'];
//             $this->ordernumber = $this->data['ordernumber'];

//         }

//     }

//     public function validate()
//     {
//         if (parent::validate()) {
//             if (isset($this->params)) {
//                 return true;
//             } else {
//                 $this->addError("Validation error", "Your input could not be validated; please contact the website Administrator.");
//                 return false;
//             }
//         }
//     }

//     public function bookFlight()
//     {
//         if ($this->validate() && $this->queryFaregrabbr($this->url)) {

//             return true;
//         } else {
//             return false;
//         }
//     }

//     private function setRequestParams($flight, $payment, $passengers = [])
//     {
//         // flight params
//         $this->params['flight']['origin'] = $flight->origin;
//         $this->params['flight']['destination'] = $flight->destination;
//         $this->params['flight']['base_fare'] = $flight->base_fare;
//         $this->params['flight']['taxes_and_fees'] = $flight->taxes_and_fees;
//         $this->params['flight']['total_fare'] = $flight->total_fare;
//         $this->params['flight']['departure_date'] = $flight->departure_date;
//         foreach ($flight->departureflightsegments as $segment) {
//             $this->params['flight']['departureflightsegments'][] = $segment;            
//         }
//         foreach ($flight->returnflightsegments as $segment) {
//             $this->params['flight']['returnflightsegments'][] = $segment;            
//         }

//         // payment params
//         $this->params['payment']['expiration'] = $payment->expiration;
//         $this->params['payment']['name'] = $payment->name;
//         $this->params['payment']['number'] = $payment->number;
//         $this->params['payment']['code'] = $payment->code;
//         $this->params['payment']['address_line1'] = $payment->address_line1;
//         $this->params['payment']['city'] = $payment->city;
//         $this->params['payment']['state'] = $payment->state;
//         $this->params['payment']['zip'] = $payment->zip;

//         // passenger params
//         foreach ($passengers as $passenger) {
//             $this->params['passenger'][] = $passenger;            
//         }



//         // API requires return to not be sent to return search without return flights
//         if (!empty($form->return)) {$this->params['return'] = $form->return;}
//     }

// }