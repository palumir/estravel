<?php

namespace common\adapters;

interface Faregrabbr {

    public function validate();

    public function buildContext();

    public function queryFaregrabbr($url);

    public function hasError();

    public function addError($name, $message);

}