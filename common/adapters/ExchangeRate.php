<?php

namespace common\adapters;

class ExchangeRate {
	
	public $rates;
	
	public function __construct() {
		$URL = "http://api.fixer.io/latest?base=USD";
		$this->rates = json_decode(file_get_contents($URL),1)['rates'];
	}
	
	public function convertToUSD($amount) {
		$number = (float)preg_replace("/([^0-9\\.])/i", "", $amount);
		$currencyCode = str_replace(preg_replace("/([^0-9\\.])/i", "", $amount), "", $amount);
		if(!array_key_exists($currencyCode, $this->rates)) return $amount;
		$conversionRate = $this->rates[$currencyCode];
		$number = round($number/$conversionRate,2);
		return "Approx. $" . $number;
	}
	
}
