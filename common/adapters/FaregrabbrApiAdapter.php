<?php

namespace common\adapters;

use yii\helpers\Json;
use Yii;

class FaregrabbrApiAdapter {

    public function query($url, $query_data)
    {
        set_time_limit(120);
        $context = $this->buildContext($query_data);
        $response = file_get_contents($url, false, $context);
		
        if ($response === FALSE) { 
            return false;
        } else {
            return $response;
        }
    }

    public function curlQuery($url, $query_data)
    {
        // temp use this, api is being difficult
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 60,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $query_data,
          CURLOPT_HTTPHEADER => array(
            "authorization: " . Yii::$app->params['authToken'],
            "cache-control: no-cache",
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            // WARNING, CURL ERRORS ARE NOT HANDLED RIGHHT NOW
            //return false;
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

    public function buildContext($query_data)
    {
       // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => ["authorization: " . Yii::$app->params['authToken'], "Content-type: application/json\r\n"],
                'method'  => 'POST',
                // if query_data is an object, public properties are used
                'content' => $query_data,
            ),
        );
        $context = stream_context_create($options);

        return $context;
    }

}