<?php

namespace common\adapters;
use Yii;

class GoogleMapsGeocoding {

    public static function geocode($address)
    {
		if($address == NULL) {
			$newAddress = "";
		}
		else if(is_string($address)) {
			$newAddress = $address;
		}
		else if(is_array($address)) {
			$newAddress = implode($address);
		}
		else {
			$newAddress= "";
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBo2eSOq6i9QKGeq4snYXSbsZW6BdIcU_4&address=" . urlencode($newAddress));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
    }


}

