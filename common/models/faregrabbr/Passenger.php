<?php

namespace common\models\faregrabbr;

use Yii;
use yii\base\Model;

class Passenger extends Model 
{

    public $index;
    public $firstName;
    public $middleName;
    public $lastName;
    public $gender;
    public $DOB;
    public $email;
    public $phoneNumber;
	// public $loyalty_cards; // not implemented yet

    public function __construct($index, $passenger, $config = [])
    {
        $this->index = $index;
        $this->firstName = $passenger->first_name;
        $this->middleName = $passenger->middle_name;
        $this->lastName = $passenger->last_name;
        $this->gender = $passenger->gender;
        $this->DOB = $passenger->date_of_birth;
        $this->email = $passenger->email;
        $this->phoneNumber = $passenger->number;

        parent::__construct($config);
    }

    // /**
    //  * @inheritdoc
    //  */
    // public function rules()
    // {
    //     return [
    //         [[
    //         	'id', 
    //         ], 'required'],
    //         [['id'], 'number'],
    //     ];
    // }

}