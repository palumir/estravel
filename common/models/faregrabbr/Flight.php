<?php

namespace common\models\faregrabbr;

use common\models\faregrabbr\FlightSegment;
use common\models\Airport;
use yii\base\Model;
use Yii;

class Flight extends Model {


	public $cacheKey;
	public $event_id;


	public $origin;
	public $destination;
	public $departure_date;
	public $adults;
	public $children;
	public $infants;
	public $class;
	
	public $connectionIndicator;

	public $base_fare;
	public $taxes_and_fees;
	public $total_fare;
	public $departureflightsegments;
	public $returnflightsegments;

	public $depart_airline_name;
	public $depart_duration;
	public $depart_end_time;
	public $depart_num_connections;
	public $depart_start_time;
	public $departbarlength;
	public $departure_airline;
	public $departure_connections;
	public $departure_times;
	public $fare;
	public $return_airline;
	public $return_airline_name;
	public $return_connections;
	public $return_duration;
	public $return_end_time;
	public $return_num_connections;
	public $return_start_time;
	public $return_times;
	public $returnbarlength;


	public function __construct($event_id, $origin, $destination, $departure_date, $adults, $children, $infants, $searchedFlightArray, $config = [])
	{
		$this->setCacheKey();
		$this->event_id = $event_id;
		// taken from the search form because shitty Faregrabbr does not
		// return them with the search results... wtf
		$this->origin = (string)$origin;
		$this->destination = (string)$destination;
		// this date format will have to be modified again because Faregrabbr
		$this->departure_date = $departure_date;
		$this->adults = (int)$adults;
		$this->children = (int)$children;
		$this->infants = (int)$infants;
		$this->loadSearchedFlightData($searchedFlightArray);

		if ($this->validate()) {
			Yii::$app->cache->set($this->cacheKey, $this, 3600);			
		}

		parent::__construct($config);
	}


	// no validation because Faregrabbr provided shit documentation

	private function setCacheKey()
	{
		if (!isset($this->cacheKey)) {
	        // one flight cached at one time per logged in user
	        if (!Yii::$app->user->isGuest) {
				$this->cacheKey = "f" . "-" . Yii::$app->user->id;	        	
	        } else {
	        	// create a temporary unique id and store it as a cookie
	        	$cookies = Yii::$app->response->cookies;
	        	$tempId = $cookies->getValue('esports.travel.temp_id', false);
	        	if (!$tempId) {
		        	$tempId = Yii::$app->security->generateRandomString(8);
		        	$cookies->add(new \yii\web\Cookie(['name' => 'esports.travel.temp_id', 'value' => $tempId]));
	        	}
	        	$this->cacheKey = "f" . "-" . $tempId;
	        }
		}
	}

	private function loadSearchedFlightData($searchedFlightArray)
	{

		// normally we could just load the attributes like this and be done with it
		$this->setAttributes($searchedFlightArray, false); // lets load what we can at least

		// but because Faregrabbr is shit we can't
		// Here shit with shitty data from Faregrabbr
		// 1. clean the base_fare and cast to decimal
		if(isset($searchedFlightArray['base_fare'])) $this->base_fare = (double)str_replace("$", "", $searchedFlightArray['base_fare']);
		if(isset($searchedFlightArray['taxes_and_fees'])) $this->taxes_and_fees = (double)str_replace("$", "", $searchedFlightArray['taxes_and_fees']);
		if(isset($searchedFlightArray['fare'])) $this->total_fare = (double)str_replace("$", "", $searchedFlightArray['fare']);
		
		// class is returned so we take it from here, not from the form
		if (isset($searchedFlightArray['class'])) {
			$this->class = $searchedFlightArray['class'];
		}

		// setAttributes will load an array into returnflightsegments but we'd rather have objects now
		// There is also a naming issue with departureflightsegments and departflightsegments
		// a discrepancy coming from Faregrabbr.
		$this->departureflightsegments = [];
		$this->returnflightsegments = [];
		if (isset($searchedFlightArray['departflightsegments'])) {
			foreach ($searchedFlightArray['departflightsegments'] as $departFlightSegmentArray) {
				$departFlightSegment = new FlightSegment($departFlightSegmentArray);
				$this->departureflightsegments[] = $departFlightSegment;
			}
		}
		if (isset($searchedFlightArray['departureflightsegments'])) {
			foreach ($searchedFlightArray['departureflightsegments'] as $departFlightSegmentArray) {
				$departFlightSegment = new FlightSegment($departFlightSegmentArray);
				$this->departureflightsegments[] = $departFlightSegment;
			}
		}
		if (isset($searchedFlightArray['returnflightsegments'])) {
			foreach ($searchedFlightArray['returnflightsegments'] as $returnFlightSegmentArray) {
				$returnFlightSegment = new FlightSegment($returnFlightSegmentArray);
				$this->returnflightsegments[] = $returnFlightSegment;
			}
		}
	}

    public function getOriginAirport()
    {
        return Airport::find()->where('iata_code=:iata_code', [':iata_code' => $this->origin])->one();
    }

    public function getDestinationAirport()
    {
        return Airport::find()->where('iata_code=:iata_code', [':iata_code' => $this->destination])->one();
    }
}