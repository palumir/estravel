<?php

namespace common\models\faregrabbr;

use Yii;
use yii\base\Model;
use common\models\CreditCardForm;

class Payment extends Model 
{
    public $expiration;
	public $name;
	public $number;
	public $code;
	public $address_line1;
	public $city;
	public $state;
	public $zip;

    public function __construct(CreditCardForm $creditCardForm, $config = [])
    {
        $this->expiration = $creditCardForm->month . "-" . $creditCardForm->year;
        $this->name = $creditCardForm->name;
        $this->number = $creditCardForm->number;
        $this->code = $creditCardForm->code;
        $this->address_line1 = $creditCardForm->addressLine1;
        $this->city = $creditCardForm->city;
        $this->state = $creditCardForm->state;
        $this->zip = $creditCardForm->zip;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'expiration',
                'name',
                'number',
                'code',
                'address_line1',
                'city',
                'state',
                'zip',
            ], 'required'],
            [['code'], 'number'],
            [['number'], 'number'],
            [['number'], 'string', 'max' => 16, 'min' => 16,],
            [['code'], 'string', 'max' => 3, 'min' => 3,],
            [['expiration'], 'string', 'max' => 7, 'min' => 7,],
            [['name', 'address_line1', 'city', 'state', 'zip'], 'string'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number' => 'Card Number',
            'name' => 'Name on card',
            'expiration' => 'Expiration',
            'code' => 'Security Code',
            'address_line1' => 'Address',
            'city' => 'City',
            'state' => 'State',
            'zip' => 'ZIP',
        ];
    }
}