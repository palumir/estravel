<?php

namespace common\models\faregrabbr;

use Yii;
use yii\base\Model;
use yii\web\ServerErrorHttpException;
use common\models\faregrabbr\Rate;
use common\models\faregrabbr\Media;

class HotelDetails extends Model 
{

    public $cacheKey;
    public $event_id;

    public $guests;
    public $numrooms;
    public $rate;

    public $hotelCode;
	public $code;
    public $hotelChain;
    public $chain;
    public $hotelName;
    public $name;
    public $address;
    public $phoneNumber;
    public $checkinTime;
    public $checkoutTime;
    public $checkin;
    public $checkout;
    public $description;
    public $rates = [];
    public $medias = [];
	public $success;
	public $currencyCode;

    public function __construct($event_id, $hotelCode, $hotelChain, $checkin, $numrooms, $guests, $checkout, $data = null, $config = [], $currencyCode = null)
    {
        $this->setCacheKey();
        $this->event_id = $event_id;
        $this->numrooms = (int)$numrooms;
        $this->guests = (int)$guests;

        $details = Yii::$app->faregrabbr->getDetails($hotelCode, $hotelChain, $checkin, $numrooms, $checkout);
        /*if (!$details) {
             throw new ServerErrorHttpException("Error Processing Request: " . $details, 500);
         } elseif (!$details['success']) {
             throw new ServerErrorHttpException($details['statustext'], 500);
         }*/

        $this->hotelCode = $details['hotelCode'];
        $this->code = $details['hotelCode'];
        $this->hotelChain = $details['hotelChain'];
        $this->chain = $details['hotelChain'];
        $this->hotelName = $details['hotelName'];
        $this->name = $details['hotelName'];
        $this->address = $details['address'];
        $this->phoneNumber = $details['phoneNumber'];
        $this->checkinTime = $details['checkinTime'];
        $this->checkoutTime = $details['checkoutTime'];
        $this->checkin = Yii::$app->formatter->asDate($checkin, "php:Y-m-d");
        $this->checkout = Yii::$app->formatter->asDate($checkout, "php:Y-m-d");
        $this->description = $details['description'];
		if($currencyCode!=null) $this->currencyCode = $currencyCode;
        if (is_array($details['rates'])) {
            usort($details['rates'], array($this, "sortByTotalCost"));
            foreach ($details['rates'] as $rate) {
                $this->rates[] = new Rate($rate);
            }
        }
        if (is_array($details['media'])) {
            foreach ($details['media'] as $media) {
                $this->medias[] = new Media($media);
            }
        }
        $this->success = $details['success'];

        if ($this->validate()) {
            Yii::$app->cache->set($this->cacheKey, $this, 3600);            
        }

        parent::__construct($config);
    }

    private function setCacheKey()
    {
        if (!isset($this->cacheKey)) {
            // one flight cached at one time per logged in user
            if (!Yii::$app->user->isGuest) {
                $this->cacheKey = "h" . "-" . Yii::$app->user->id;              
            } else {
                // create a temporary unique id and store it as a cookie
                $cookies = Yii::$app->response->cookies;
                $tempId = $cookies->getValue('esports.travel.temp_id', false);
                if (!$tempId) {
                    $tempId = Yii::$app->security->generateRandomString(8);
                    $cookies->add(new \yii\web\Cookie(['name' => 'esports.travel.temp_id', 'value' => $tempId]));
                }
                $this->cacheKey = "h" . "-" . $tempId;
            }
        }
    }

    private static function sortByTotalCost($arr1, $arr2)
    {
        if (array_key_exists("total_cost", $arr1)) {
            $valueOne = (double)str_replace("$", "", $arr1['total_cost']);
        } else {
            return 0;
        }
        if (array_key_exists("total_cost", $arr2)) {
            $valueTwo = (double)str_replace("$", "", $arr2['total_cost']);
        } else {
            return 0;
        }

        if ($valueOne == $valueTwo) {
            return 0;
        }
        return ($valueOne < $valueTwo) ? -1 : 1;
    }

}