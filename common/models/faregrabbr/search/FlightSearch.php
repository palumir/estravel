<?php

namespace common\models\faregrabbr\search;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use common\models\faregrabbr\Flight;
use common\components\Faregrabbr;
use \DateTime;

function convertToCorrectDate($date) {
		if($date==NULL) return "";
		return date('Y-m-d',strtotime($date));
}

class FlightSearch extends Flight
{

    public $flights;
    public $summary;
    public $airlines;
	

    public function __construct($flightSearchForm = null)
    {
		
        if ($flightSearchForm) {
			
			$flightSearchForm->return = convertToCorrectDate($flightSearchForm->return);
			$flightSearchForm->departure = convertToCorrectDate($flightSearchForm->departure);
			
            $cacheKey = Yii::$app->faregrabbr->cacheKey($flightSearchForm);

			// Don't search for returns if it's not a round trip
			if($flightSearchForm->roundTrip == false) $flightSearchForm->return = "";
				
            // if the search results are cached, get them from cache
            if (Yii::$app->cache->exists($cacheKey)) {
                $data = Yii::$app->cache->get($cacheKey);
                if (array_key_exists("searchresult", $data)) {
                    $this->flights = $data['searchresult'];
                }
                if (array_key_exists("summary", $data)) {
                    $this->summary = $data['summary'];
                }
                if (array_key_exists("airlines", $data)) {
                    $this->airlines = $data['airlines'];
                }
            } 
			else {
                // query the api for a new search on flights
                $data = Yii::$app->faregrabbr->getFlights($flightSearchForm);
                if (is_array($data)) {
					
                    Yii::$app->cache->set($cacheKey, $data, 360);
                    if (array_key_exists("searchresult", $data)) {
                        $this->flights = $data['searchresult'];
                    }
                    if (array_key_exists("summary", $data)) {
                        $this->summary = $data['summary'];
                    }
                    if (array_key_exists("airlines", $data)) {
                        $this->airlines = $data['airlines'];
                    }
                }
            }
			
			// Remove the indirect flights from $data.
			if($flightSearchForm->directFlights) {
				$newData = array();
				foreach($this->flights as $flight) {
					if((!isset($flight["departflightsegments"]) || count($flight["departflightsegments"]) <= 1) && (!isset($flight["returnflightsegments"]) || count($flight["returnflightsegments"]) <= 1)) {
						$newData[] = $flight;
					}
				}
				$this->flights = $newData;
			}
			
			// Return flight options
			if(isset($flight["depart_start_time"])) {
				
				// Remove departure flights that aren't within our selected time.
				$newData = array();
				foreach($this->flights as $flight) {
					
					$departDateObject = new DateTime($flight["depart_start_time"]);
					$strippedOfHours = $departDateObject->format('Y-m-d');
					$selectedTimeMin = strtotime($strippedOfHours) + 60*60*$flightSearchForm->departureFlightTimeMin;
					$selectedTimeMax = strtotime($strippedOfHours) + 60*60*$flightSearchForm->departureFlightTimeMax;
					$actualTime = strtotime($flight["depart_start_time"]);
					
					// Only include it if it's within an hour of our selected time.
					if($selectedTimeMin <= $actualTime && $selectedTimeMax >= $actualTime) {
						$newData[] = $flight;
					}
				}
				$this->flights = $newData;
				
				
				// Remove depart flights that don't have the correct return journey duration
				$newData = array();
				foreach($this->flights as $flight) {
					
					$departJourneyDuration = $flight["depart_duration"]*60;
					$selectedTimeMin = $flightSearchForm->departureJourneyDurationMin*60*60;
					$selectedTimeMax = $flightSearchForm->departureJourneyDurationMax*60*60;
					
					// Only include it if it's within an hour of our selected time.
					if($selectedTimeMin <= $departJourneyDuration && $selectedTimeMax >= $departJourneyDuration) {
						$newData[] = $flight;
					}
				}
				$this->flights = $newData;
			}
			
			// Return flight options
			if(isset($flight["return_start_time"])) {
				// Remove return flights that aren't within our selected time.
				$newData = array();
				foreach($this->flights as $flight) {
					
					$returnDateObject = new DateTime($flight["return_start_time"]);
					$strippedOfHours = $returnDateObject->format('Y-m-d');
					$selectedTimeMin = strtotime($strippedOfHours) + 60*60*$flightSearchForm->returnFlightTimeMin;
					$selectedTimeMax = strtotime($strippedOfHours) + 60*60*$flightSearchForm->returnFlightTimeMax;
					$actualTime = strtotime($flight["return_start_time"]);
					
					// Only include it if it's within an hour of our selected time.
					if($selectedTimeMin <= $actualTime && $selectedTimeMax >= $actualTime) {
						$newData[] = $flight;
					}
				}
				$this->flights = $newData;
				
				// Remove return flights that don't have the correct return journey duration
				$newData = array();
				foreach($this->flights as $flight) {
					
					$returnJourneyDuration = $flight["return_duration"]*60;
					$selectedTimeMin = $flightSearchForm->returnJourneyDurationMin*60*60;
					$selectedTimeMax = $flightSearchForm->returnJourneyDurationMax*60*60;
					
					// Only include it if it's within an hour of our selected time.
					if($selectedTimeMin <= $returnJourneyDuration && $selectedTimeMax >= $returnJourneyDuration) {
						$newData[] = $flight;
					}
				}
				$this->flights = $newData;
			}
		}
    }

    // /**
    //  * @inheritdoc
    //  */
    // public function rules()
    // {
    //     return [
    //         [['id', 'admin_id', 'published'], 'integer'],
    //         [['title', 'body', 'tags', 'created_at', 'updated_at'], 'safe'],
    //     ];
    // }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->flights,
            // 'sort' => [
            //     'attributes' => ['depart_airline_name' => SORT_ASC],
            // ],
            'pagination' => 
                [
                    'pageSize' => 10,
                ],
        ]);

        $this->load($params);

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'admin_id' => $this->admin_id,
        //     'published' => $this->published,
        //     'created_at' => $this->created_at,
        //     'updated_at' => $this->updated_at,
        // ]);

        // $query->andFilterWhere(['like', 'title', $this->title])
        //     ->andFilterWhere(['like', 'body', $this->body])
        //     ->andFilterWhere(['like', 'tags', $this->tags]);

        return $dataProvider;
    }
}
