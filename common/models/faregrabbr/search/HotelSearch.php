<?php

namespace common\models\faregrabbr\search;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use common\models\Airport;
use common\models\faregrabbr\HotelDetails;
use common\components\Faregrabbr;
use common\adapters\GoogleMapsDistanceMatrix;

function convertToCorrectDate($date) {
		if($date==NULL) return "";
		return date('Y-m-d',strtotime($date));
}

function getDistances($data, $eventAddress) {
						
	// Gather addresses
	$hotelAddresses = array();
	for($i = 0; $i < count($data); $i++) {
		$hotelAddresses[] = [
							"address" => $data[$i]["briefAddress"] . ", " . Airport::findByIata($data[$i]["location"])['city'],
							"cacheKey" => "Distance Between: " . $data[$i]["briefAddress"] . ", " . Airport::findByIata($data[$i]["location"])['city'] . " and " . $eventAddress
							];
	}
	
	$searchAddresses = array();
	
	// Check which addresses are cached.
	foreach($hotelAddresses as $hotelAddress) {
		if(!Yii::$app->cache->exists($hotelAddress["cacheKey"]) || Yii::$app->cache->get($hotelAddress["cacheKey"]) == "N/A") {
			$searchAddresses[] = $hotelAddress;
		}
	}
	
	// Make a list of just addresses
	$justAddresses = array();
	foreach($searchAddresses as $address) {
		$justAddresses[] = $address["address"];
	}
	
	// Call Google's API for the search addresses.
	if(count($searchAddresses)>0) {
	
		$distanceList = GoogleMapsDistanceMatrix::getDistanceList($eventAddress, $justAddresses);
		
		if($distanceList["status"] == "OK") {
			// Cache the distances.
			$x = 0;
			foreach($searchAddresses as $searchAddress) {
				if($distanceList["rows"][0]["elements"][$x]["status"] == "OK") {
					Yii::$app->cache->set($searchAddress["cacheKey"], $distanceList["rows"][0]["elements"][$x]["distance"]["text"], 48*60*60);
				}
				else {
					Yii::$app->cache->set($searchAddress["cacheKey"], "N/A", 48*60*60);
				}
				$x++;
			}
		}
	}
	
	// Get the hotel distances from cache.
	for($i = 0; $i < count($data); $i++) {
		$data[$i]['distanceFromVenue'] =  Yii::$app->cache->get($hotelAddresses[$i]["cacheKey"]);
	}
	return $data;
}

class HotelSearch extends HotelDetails
{

    public $hotels;
    public $currency;
    public $status;
	public $nextRequestReference;
	public $currentRequestReference;

    public function __construct($hotelSearchForm = null, $load_more = null, $eventAddress, $eventAirport)
    {

        if ($hotelSearchForm) {
			$hotelSearchForm->checkin = convertToCorrectDate($hotelSearchForm->checkin);
			$hotelSearchForm->checkout = convertToCorrectDate($hotelSearchForm->checkout);
			
            $cacheKey = Yii::$app->faregrabbr->cacheKey($hotelSearchForm);
			
			//$data = Yii::$app->cache->set($cacheKey,NULL);
			
			$recache = false;

            // if the search results are cached, get them from cache
            if (Yii::$app->cache->exists($cacheKey) && Yii::$app->cache->get($cacheKey) != NULL) {
                $data = Yii::$app->cache->get($cacheKey);
                if (array_key_exists("searchresult", $data)) {
					
					// If there are next results, get them.
					if($load_more != null && (!isset($data['currentRequestReference']) || $load_more != $data['currentRequestReference'])) {
						
						// Load more hotels and combine them with the search result.
						$newHotels = Yii::$app->faregrabbr->loadNextHotels($hotelSearchForm, $load_more);
						
						if($newHotels == null || !array_key_exists("searchresult", $newHotels) || $newHotels['searchresult'] == null || $newHotels['searchresult'] == "" || count($newHotels['searchresult']) < 1) {
							$hotelSearchForm->locationType = "Airport";
							$hotelSearchForm->location = $eventAirport;
							$newHotels = Yii::$app->faregrabbr->loadNextHotels($hotelSearchForm, $load_more);
						}
						
						// Set-up our next request
						$this->currentRequestReference = $load_more;
						$this->nextRequestReference = $newHotels['nextRequestReference'];
						$data['currentRequestReference'] = $load_more;
						$data['nextRequestReference'] = $newHotels['nextRequestReference'];
						
						$newHotels['searchresult'] = getDistances($newHotels['searchresult'], $eventAddress);
						
						// Recache
						$recache = true;
						
						// Merge datas.
						$data['searchresult'] = array_merge($data['searchresult'],$newHotels['searchresult']);
						
					}
					else {
						if(isset($data['nextRequestReference'])) $this->nextRequestReference = $data['nextRequestReference'];
						if(isset($data['currentRequestReference'])) $this->currentRequestReference = $data['currentRequestReference'];
					}
					usort($data['searchresult'], array($this, "sortByMinimumRate"));
					$this->hotels = $data['searchresult'];
                }
                if (array_key_exists("currency", $data)) {
                    $this->currency = $data['currency'];
                }
                if (array_key_exists("status", $data)) {
                    $this->status = $data['status'];
                }
				
            } else {
                // query the api for a new search on hotels
                $data = Yii::$app->faregrabbr->getHotels($hotelSearchForm);
				if($data == null || !array_key_exists("searchresult", $data) || $data['searchresult'] == null || $data['searchresult'] == "" || count($data['searchresult']) < 1) {
					$hotelSearchForm->locationType = "Airport";
					$hotelSearchForm->location = $eventAirport;
					$data = Yii::$app->faregrabbr->getHotels($hotelSearchForm);
				}
				
				if($data != null) {
					if (array_key_exists("nextRequestReference", $data)) {
						$this->nextRequestReference = $data['nextRequestReference'];
					}
					if (array_key_exists("searchresult", $data)) {
						usort($data['searchresult'], array($this, "sortByMinimumRate"));
						
						$data['searchresult'] = getDistances($data['searchresult'], $eventAddress);
						
						$this->hotels = $data['searchresult'];
					}
					if (array_key_exists("currency", $data)) {
						$this->currency = $data['currency'];
					}
					if (array_key_exists("status", $data)) {
						$this->status = $data['status'];
					}
					Yii::$app->cache->set($cacheKey, $data, 360);
				}
            }
											
			// Cache again, if we loaded the next page.
			if($recache) {
				Yii::$app->cache->set($cacheKey, $data, 360);
			}
		}


    }

    // /**
    //  * @inheritdoc
    //  */
    // public function rules()
    // {
    //     return [
    //         [['id', 'admin_id', 'published'], 'integer'],
    //         [['title', 'body', 'tags', 'created_at', 'updated_at'], 'safe'],
    //     ];
    // }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->hotels,
            // 'sort' => [
            //     'attributes' => ['depart_airline_name' => SORT_ASC],
            // ],
            'pagination' => 
                [
                    'pageSize' => 10,
                ],
        ]);

        $this->load($params);

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'admin_id' => $this->admin_id,
        //     'published' => $this->published,
        //     'created_at' => $this->created_at,
        //     'updated_at' => $this->updated_at,
        // ]);

        // $query->andFilterWhere(['like', 'title', $this->title])
        //     ->andFilterWhere(['like', 'body', $this->body])
        //     ->andFilterWhere(['like', 'tags', $this->tags]);

        return $dataProvider;
    }

    private static function sortByMinimumRate($arr1, $arr2)
    {
        if (array_key_exists("minimumRate", $arr1)) {
            $valueOne = (double)str_replace("$", "", $arr1['minimumRate']);
        } else {
            return 0;
        }
        if (array_key_exists("minimumRate", $arr2)) {
            $valueTwo = (double)str_replace("$", "", $arr2['minimumRate']);
        } else {
            return 0;
        }

        if ($valueOne == $valueTwo) {
            return 0;
        }
        return ($valueOne < $valueTwo) ? -1 : 1;
    }

}
