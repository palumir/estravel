<?php

namespace common\models\faregrabbr;
use yii\base\Model;


// a flight segment is essentially a connecting flight
// It can be part of a departflightsegments in the search response 
// (or departureflightsegments in the booking request, lol) array 
// or a returnflightsegmentsarray

class FlightSegment extends Model {

	public $code;
	public $duration;
	public $flight_num;
	public $from;
	public $key;
	public $land;
	public $layover;
	public $leg;
	public $seg;
	public $takeoff;
	public $to;

	// no validation because Faregrabbr provided shit documentation

	public function __construct($flightSegmentArray)
	{
		$this->setAttributes($flightSegmentArray, false);
	}


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'flight_num' => 'Flight Number',
        ];
    }


}