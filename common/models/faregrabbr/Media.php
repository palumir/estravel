<?php

namespace common\models\faregrabbr;

use Yii;
use yii\base\Model;

class Media extends Model 
{

    public function __construct($media, $config = [])
    {
        $this->url = $media['url'];
        $this->type = $media['type'];
        $this->size = $media['size'];
        $this->caption = $media['caption'];

        parent::__construct($config);
    }

    public $url;
    public $type;
    public $size;
    public $caption;

}