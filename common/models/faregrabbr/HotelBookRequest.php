<?php

namespace common\models\faregrabbr;

use Yii;
use yii\base\Model;
use yii\web\ServerErrorHttpException;
use common\models\faregrabbr\Rate;
use common\models\faregrabbr\Payment;
use common\models\faregrabbr\Media;

class HotelBookRequest extends Model 
{

    public $HotelDetails;
    public $numrooms;
    public $checkin;
    public $checkout;
    public $rate;
    public $commission;
    public $passenger;
    public $payment;

    public function __construct(HotelDetails $hotelDetails, $numrooms, $checkin, $checkout, Rate $rate, $passengers, Payment $payment, $config = [])
    {

        $this->HotelDetails = new \stdClass();
        $this->HotelDetails->code = $hotelDetails->code;
        $this->HotelDetails->chain = $hotelDetails->chain;
        $this->HotelDetails->name = $hotelDetails->name;
        $this->HotelDetails->address = $hotelDetails->address;

        $this->numrooms = $numrooms;
        $this->checkin = $checkin;
        $this->checkout = $checkout;

        $this->rate = new \stdClass();
        $this->rate->plan = $rate->plan;
        $this->rate->category = $rate->category;
        $this->rate->per_night = $rate->per_night;
        $this->rate->total = $rate->total;

        $this->passenger = $passengers;

        $this->payment = $payment;

        parent::__construct($config);
    }

}