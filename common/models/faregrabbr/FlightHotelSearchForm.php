<?php

namespace common\models\faregrabbr;
use yii\base\Model;


class FlightHotelSearchForm extends Model
{
    
	public $roundTrip = false;
	public $origin;
	public $destination;
	public $return;
	public $departure;
	public $adults = 1;
	public $children = 0;
	public $infants = 0;
    public $class = 'economy';
	public $location;
	public $locationType;
	public $checkin;
	public $numrooms;
    public $checkout;
    public $guests;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['origin', 'destination', 'departure', 'adults'], 'required'],
            [['departure'], 'date'],
			[['return'], 'date'],
            [['class'], 'string'],
            [['adults', 'children', 'infants'], 'integer'],
            ['origin', 'filter', 'filter' => function ($id) {
                // if it's numerical, it's an id in the database
                // and we need to return the the iata_code
                if (is_numeric($id)) {
                    $result = (new \yii\db\Query())
                        ->select(['iata_code'])
                        ->from('airport')
                        ->where('id=:id', [':id' => $id])
                        ->one();
                    return $result['iata_code'];
                } else {
                    // it's probably already the iata_code
                    return $id;
                }
            }],
            ['destination', 'filter', 'filter' => function ($id) {
                // if it's numerical, it's an id in the database
                // and we need to return the the iata_code
                if (is_numeric($id)) {
                    $result = (new \yii\db\Query())
                        ->select(['iata_code'])
                        ->from('airport')
                        ->where('id=:id', [':id' => $id])
                        ->one();
                    return $result['iata_code'];
                } else {
                    // it's probably already the iata_code
                    return $id;
                }
            }],
			[['location', 'locationType', 'checkin', 'numrooms', 'checkout', 'guests'], 'required'],
            [['checkin', 'checkout'], 'date'],
            [['location', 'locationType'], 'string'],
            [['numrooms', 'guests'], 'integer'],
            [['locationType'], 'default', 'value' => 'City'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()	
    {
        return [
            'origin' => 'Origin Airport',
            'destination' => 'Destination Airport',
            'departure' => 'Departure Date',
			'return' => 'Return Date',
            'adults' => 'Adults',
            'children' => 'Children',
            'infants' => 'Infants',
            'class' => 'Class',
			'location' => 'Location',
            'locationType' => 'Location Type',
            'checkin' => 'Check-in',
            'numrooms' => 'Number of Rooms',
            'guests' => 'Number of Guests',
            'checkout' => 'Check-out',
        ];
    }

}