<?php

namespace common\models\faregrabbr;

use Yii;
use yii\base\Model;

class Rate extends Model 
{

    public $plan;
    public $category;
    public $per_night;
    public $total;
    public $base;
    public $description;
    public $rate_includes = [];
    public $cancel_deadline;
    public $cancel_penality;
    public $isSmoking;
    public $taxes_and_surcharges;
    public $deposit_amount;
    public $guarantee_type;

    public function __construct($rate, $config = [])
    {
		if(isset($rate['plan'])) {
			$this->plan = $rate['plan'];
			$this->category = $rate['category'];
			$this->total = $rate['total'];
			$this->base = $rate['base'];
			$this->description = $rate['description'];
			$this->rate_includes = $rate['rate_includes'];
			$this->cancel_deadline = $rate['cancel_deadline'];
			$this->cancel_penality = $rate['cancel_penality'];
			$this->isSmoking = $rate['isSmoking'];
			$this->taxes_and_surcharges = $rate['taxes_and_surcharges'];
			$this->deposit_amount = $rate['deposit_amount'];
			$this->guarantee_type = $rate['guarantee_type'];
		}
		else {
			$this->plan = $rate['rate_plan'];
			$this->category = $rate['rate_category'];
			$this->total = $rate['total_cost'];
			$this->base = $rate['base_cost'];
			$this->description = $rate['description'];
			$this->rate_includes = $rate['rate_includes'];
			$this->cancel_deadline = $rate['cancel_deadline'];
			$this->cancel_penality = $rate['cancel_penality'];
			$this->isSmoking = $rate['isSmoking'];
			$this->taxes_and_surcharges = $rate['taxes_and_surcharges'];
			$this->deposit_amount = $rate['deposit_amount'];
			$this->guarantee_type = $rate['guarantee_type'];
		}

        parent::__construct($config);
    }
}