<?php

namespace common\models\faregrabbr;
use yii\base\Model;


class HotelSearchForm extends Model
{
    
	public $location;
	public $locationType = "POI";
	public $checkin;
	public $numrooms;
    public $checkout;
    public $guests;
	public $event_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['location', 'locationType', 'checkin', 'numrooms', 'checkout', 'guests'], 'required'],
            [['checkin', 'checkout'], 'date'],
            [['location', 'locationType'], 'string'],
            [['numrooms', 'guests'], 'integer'],
            [['locationType'], 'default', 'value' => 'City'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'location' => 'Location',
            'locationType' => 'Location Type',
            'checkin' => 'Check-in',
            'numrooms' => 'Number of Rooms',
            'guests' => 'Number of Guests',
            'checkout' => 'Check-out',
        ];
    }

}