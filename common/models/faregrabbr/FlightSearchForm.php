<?php

namespace common\models\faregrabbr;
use yii\base\Model;


class FlightSearchForm extends Model
{
    
	public $roundTrip = false;
	public $origin;
	public $destination;
	public $return;
	public $departure;
	public $adults = 1;
	public $children = 0;
	public $infants = 0;
    public $class = 'economy';
	public $directFlights = false;
	public $event_id;
	
	// Advanced options
	public $departureFlightTimeMin = 0;
	public $departureFlightTimeMax = 24;
	public $departureFlightTime;
	public $returnFlightTimeMin = 0;
	public $returnFlightTimeMax = 24;
	public $returnFlightTime;
	public $departureJourneyDurationMin = 0;
	public $departureJourneyDurationMax = 48;
	public $departureJourneyDuration;
	public $returnJourneyDurationMin = 0;
	public $returnJourneyDurationMax = 48;
	public $returnJourneyDuration;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['origin', 'destination', 'departure', 'adults'], 'required'],
			[['roundTrip'],'boolean'],
            [['departure'], 'date'],
			['departure', 'filter', 'filter'=> function($id) {
				// Convert to correct date.
				return (new DateTime(strtotime($id)))->format("Y-m-d");
			}],
			[['return'], 'date'],
			['return', 'filter', 'filter'=> function($id) {
				// Convert to correct date.
				return (new DateTime(strtotime($id)))->format("Y-m-d");
			}],
			[['departureFlightTime'], 'number'],
			[['departureFlightTimeMax'], 'number'],
			[['departureFlightTimeMin'], 'number'],
			[['departureJourneyDuration'], 'number'],
			[['departureJourneyDurationMin'], 'number'],
			[['departureJourneyDurationMax'], 'number'],
			[['returnFlightTime'], 'number'],
			[['returnFlightTimeMax'], 'number'],
			[['returnFlightTimeMin'], 'number'],
			[['returnJourneyDuration'], 'number'],
			[['returnJourneyDurationMin'], 'number'],
			[['returnJourneyDurationMax'], 'number'],
            [['class'], 'string'],
            [['adults', 'children', 'infants'], 'integer'],
            ['origin', 'filter', 'filter' => function ($id) {
                // if it's numerical, it's an id in the database
                // and we need to return the the iata_code
                if (is_numeric($id)) {
                    $result = (new \yii\db\Query())
                        ->select(['iata_code'])
                        ->from('airport')
                        ->where('id=:id', [':id' => $id])
                        ->one();
                    return $result['iata_code'];
                } else {
                    // it's probably already the iata_code
                    return $id;
                }
            }],
            ['destination', 'filter', 'filter' => function ($id) {
                // if it's numerical, it's an id in the database
                // and we need to return the the iata_code
                if (is_numeric($id)) {
                    $result = (new \yii\db\Query())
                        ->select(['iata_code'])
                        ->from('airport')
                        ->where('id=:id', [':id' => $id])
                        ->one();
                    return $result['iata_code'];
                } else {
                    // it's probably already the iata_code
                    return $id;
                }
            }],
			[['directFlights'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'origin' => 'Origin Airport',
            'destination' => 'Destination Airport',
            'departure' => 'Departure Date',
			'return' => 'Return Date',
            'adults' => 'Adults',
            'children' => 'Children',
            'infants' => 'Infants',
            'class' => 'Class',
			'directFlights' => 'Direct Flights Only',
        ];
    }

}