<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use backend\models\Ticket;
use common\models\Airport;
use common\models\Country;
use frontend\models\BookedFlight;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 1;
    const STATUS_BANNED = 2;

    const ROLE_CLIENT = 1;
    const ROLE_ADMIN = 2;

    const NEWSLETTER_DAILY = 'daily';
    const NEWSLETTER_WEEKLY = 'weekly';
    const NEWSLETTER_MONTHLY = 'monthly';
    const NEWSLETTER_EVENT = 'events';
    const NEWSLETTER_OFF = 'off';

    const USER = 'USER';
    const ADMIN = 'ADMIN';

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $default_origin_airport_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['airport_id'], 'integer'],
            [['country_id'], 'integer'],
            [['status'], 'integer'],
            [['status'], 'default', 'value' => self::STATUS_ACTIVE],
            [['role'], 'integer'],
            [['role'], 'default', 'value' => self::ROLE_CLIENT],
            [['email'], 'email'],
            [['date_of_birth'], 'date', 'format' => 'yyyy-mm-dd'],
            [['firstname'], 'string', 'max' => 90],
            [['middlename'], 'string', 'max' => 90],
            [['lastname'], 'string', 'max' => 90],
            [['phone'], 'string', 'max' => 90],
            [['gender'], 'in', 'range' => ['M', 'F']],
            [['newsletter'], 'default', 'value' => self::NEWSLETTER_WEEKLY],
            [['newsletter'], 'in', 'range' => ['daily', 'weekly', 'monthly', 'event', 'off']],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'firstname' => 'First name',
            'middlename' => 'Middle name',
            'lastname' => 'Last name',
            'phone' => 'Phone',
            'airport_id' => 'Your Default Airport (name, city or code)',
            'country_id' => 'Country',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE, 'deleted' => self::NOT_DELETED]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function selfDelete()
    {
        $this->deleted = self::DELETED;
    }

    public function selfUnDelete()
    {
        $this->deleted = self::NOT_DELETED;
    }

    public function countTickets()
    {
        $my_tickets = (new \yii\db\Query())
                    ->select('id')
                    ->from('ticket')
                    ->where(['admin_id' => $this->id, 'status' => [Ticket::STATUS_ASSIGNED]])
                    ->count();

        return $my_tickets;
    }

    public function fullName()
    {
        return $this->firstname . " " . $this->lastname;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAirport()
    {
        return $this->hasOne(Airport::className(), ['id' => 'airport_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

}
