<?php

namespace common\models;

use Yii;
use yii\base\Model;

class CreditCardForm extends Model 
{
    public $month;
	public $year;
	public $name;
	public $number = "4547767787296090";
	public $code;
	public $addressLine1;
	public $city;
	public $state;
	public $zip;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'month',
            	'year',
            	'name',
            	'number',
            	'code',
            	'addressLine1',
            	'city',
            	'state',
            	'zip',
            ], 'required'],
            [['code'], 'number'],
            [['number'], 'number'],
            [['number'], 'string', 'max' => 16, 'min' => 15,],
            [['code'], 'string', 'max' => 3, 'min' => 3,],
            [['month'], 'string', 'max' => 2, 'min' => 2,],
            [['year'], 'string', 'max' => 4, 'min' => 4,],
            [['name', 'addressLine1', 'city', 'state', 'zip'], 'string'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number' => 'Card Number',
            'name' => 'Name on card',
            'month' => 'Month',
            'year' => 'Year',
            'code' => 'Security Code',
            'addressLine1' => 'Address',
            'city' => 'City',
            'state' => 'State',
            'zip' => 'ZIP',
        ];
    }
}
