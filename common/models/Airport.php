<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "airport".
 *
 * @property integer $id
 * @property string $name
 * @property string $city
 * @property string $country
 * @property string $iata_code
 * @property string $icao_code
 * @property string $latitude
 * @property string $longitude
 * @property string $altitude
 * @property string $timezone
 * @property string $dst
 * @property string $rank
 * @property string $type
 */
class Airport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'airport';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iata_code'], 'required'],
            [['name', 'city', 'country', 'latitude', 'longitude', 'altitude', 'timezone', 'dst', 'rank', 'type'], 'string', 'max' => 255],
            [['iata_code'], 'string', 'max' => 4],
            [['icao_code'], 'string', 'max' => 8],
            [['iata_code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'city' => 'City',
            'country' => 'Country',
            'iata_code' => 'Iata Code',
            'icao_code' => 'Icao Code',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'altitude' => 'Altitude',
            'timezone' => 'Timezone',
            'dst' => 'Dst',
            'rank' => 'Rank',
            'type' => 'Type',
        ];
    }

    public static function findByIata($iata_code)
    {
        return static::find()->where("iata_code=:iata_code", ['iata_code' => $iata_code])->one();
    }
	
	    public static function findByName($name)
    {
        return static::find()->where("name=:name", ['name' => $name])->one();
    }

}
