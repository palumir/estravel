<?php

namespace common\models;

use Yii;

// parent class for all database models on the site
class EstData extends \yii\db\ActiveRecord
{

	const NOT_DELETED = 0;
	const DELETED = 1;

	const NOT_ARCHIVED = 0;
	const ARCHIVED = 1;

}
