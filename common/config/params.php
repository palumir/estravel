<?php

$authToken = require(__DIR__ . '/authToken.php');

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'monthRange' => [
                "01" => "01",
                "02" => "02",
                "03" => "03",
                "04" => "04",
                "05" => "05",
                "06" => "06",
                "07" => "07",
                "08" => "08",
                "09" => "09",
                "10" => "10",
                "11" => "11",
                "12" => "12",
    ],
    'yearRange' => array_combine(range(2016,2036), range(2016,2036)),
    'authToken' => $authToken,
];
