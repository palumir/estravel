<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
    	// There is a need to cache search results coming from the Faregrabbr API
    	// common/adapters/
		'assetManager' => [
			'bundles' => [
				'yii\web\JqueryAsset' => [
					'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
				],
				'yii\bootstrap\BootstrapPluginAsset' => [
					'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
				],
				'yii\jui\JuiAsset' => [
					'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
				],
			],
		],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'faregrabbr' => [
            'class' => 'common\components\Faregrabbr',
            'commission' => 15,
            'baseUrl' => 'https://cde.faregrabbr.com/',
            'flightSearchUrl' => 'remoteflightsearch.php',
            'flightBookUrl' => 'remoteairbook.php',
            'hotelSearchUrl' => 'remotehotelsearch.php',
            'hotelDetailsUrl' => 'remotehoteldetails.php',
            'hotelBookUrl' => 'remotehotelbook.php',
            'tripSearchUrl' => 'remotegettrips.php',
            'remoteAutoCompletePlacesUrl' => 'remoteautocomplete_places.php',
        ],
        'faregrabbrApi' => [
            'class' => 'common\adapters\FaregrabbrApiAdapter',
        ],
    ],
];
