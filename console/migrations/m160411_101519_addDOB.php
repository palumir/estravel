<?php

use yii\db\Migration;

class m160411_101519_addDOB extends Migration
{
    public function up()
    {
        $this->addColumn("{{%user}}", "date_of_birth", "DATE NULL AFTER `gender`");
    }

    public function down()
    {
        echo "m160411_101519_addDOB cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
