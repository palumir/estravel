<?php

use yii\db\Migration;

class m160422_142139_changeDeletionFunctionality extends Migration
{
    public function up()
    {
        $this->addColumn("{{%article}}", 'deleted', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%article}}", 'archived', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%booked_flight}}", 'archived', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%booked_hotel}}", 'archived', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%booking}}", 'deleted', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%booking}}", 'archived', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%event}}", 'deleted', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%event}}", 'archived', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%game}}", 'deleted', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%game}}", 'archived', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%news}}", 'deleted', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%news}}", 'archived', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%ticket}}", 'deleted', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%ticket}}", 'archived', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%user}}", 'deleted', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
        $this->addColumn("{{%user}}", 'archived', 'TINYINT(1) NOT NULL DEFAULT 0 after updated_at');
    }

    public function down()
    {
        echo "m160422_142139_changeDeletionFunctionality cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
