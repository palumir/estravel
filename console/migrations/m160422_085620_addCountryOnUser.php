<?php

use yii\db\Migration;

class m160422_085620_addCountryOnUser extends Migration
{
    public function up()
    {
        $this->addColumn("{{%user}}", "newsletter", "ENUM('daily', 'weekly', 'monthly', 'events', 'off') NOT NULL DEFAULT 'weekly'"); // newsletter
        $this->addColumn("{{%user}}", "country", "VARCHAR(90) NULL DEFAULT NULL"); // country
    }

    public function down()
    {
        echo "m160422_085620_addCountryOnUser cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
