<?php

use yii\db\Migration;

class m160411_090646_addTypeOnPassenger extends Migration
{
    public function up()
    {
        $this->addColumn("{{%passenger}}", "type", "ENUM('adult', 'child', 'infant') NOT NULL AFTER `last_name`");
    }

    public function down()
    {
        echo "m160411_090646_addTypeOnPassenger cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
