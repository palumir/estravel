<?php

use yii\db\Schema;
use yii\db\Migration;

class m160302_123606_expandUserTable extends Migration
{
    public function up()
    {
        $this->addColumn("{{%user}}", "firstname", "VARCHAR(90) NOT NULL AFTER `username`");
        $this->addColumn("{{%user}}", "lastname", "VARCHAR(90) NOT NULL AFTER `firstname`");
        $this->addColumn("{{%user}}", "phone", "VARCHAR(90) NOT NULL AFTER `lastname`");
    }

    public function down()
    {
        echo "m160302_123606_expandUserTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
