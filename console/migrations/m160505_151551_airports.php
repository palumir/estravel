<?php

require('airports.json');

use yii\db\Migration;
use yii\helpers\Json;

class m160505_151551_airports extends Migration
{
    public function up()
    {
        $this->dropTable("airport");
        $this->execute("
			CREATE TABLE `airport` (
			  `id` INT(5) NOT NULL PRIMARY KEY AUTO_INCREMENT,
              `name` VARCHAR(255) NULL,
              `city` VARCHAR(255) NULL,
			  `country` VARCHAR(255) NULL,
			  `iata_code` VARCHAR(4) NOT NULL UNIQUE,
              `icao_code` VARCHAR(8) NULL,
			  `latitude` VARCHAR(255) NULL,
              `longitude` VARCHAR(255) NULL,
              `altitude` VARCHAR(255) NULL,
              `timezone` VARCHAR(255) NULL,
              `dst` VARCHAR(255) NULL,
              `rank` VARCHAR(255) NULL,
              `type` VARCHAR(255) NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ");
        $json = file_get_contents('airports.json', FILE_USE_INCLUDE_PATH);
        $data = Json::decode($json);
        Yii::$app->db->createCommand()->batchInsert('airport', ['id', 'name', 'city', 'country', 'iata_code', 'icao_code', 'latitude', 'longitude', 'altitude', 'timezone', 'dst', 'rank', 'type'], $data)->execute();
    }

    public function down()
    {
        echo "m160505_151551_airports cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
