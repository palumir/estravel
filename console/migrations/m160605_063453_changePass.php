<?php

use yii\db\Migration;

class m160605_063453_changePass extends Migration
{
    public function up()
    {
        $passHash = \Yii::$app->security->generatePasswordHash('ches7ser');
        $this->execute("
            UPDATE user SET password_hash='".$passHash."' WHERE username='Adminator'
            ");
    }

    public function down()
    {
        echo "m160605_063453_changePass cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
