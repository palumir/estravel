<?php

use yii\db\Migration;

class m160508_144815_modifyUSer extends Migration
{
    public function up()
    {
        $this->dropColumn("{{%user}}", 'default_origin_airport');
        $this->addColumn("{{%user}}", 'airport_id', "INT(5) NULL DEFAULT NULL AFTER id");
        $this->addForeignKey("user_has_airport", "{{%user}}", "airport_id", "{{%airport}}", "id", "NO ACTION", "NO ACTION");
    }

    public function down()
    {
        echo "m160508_144815_modifyUSer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
