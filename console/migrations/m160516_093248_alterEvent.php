<?php

use yii\db\Migration;

class m160516_093248_alterEvent extends Migration
{
    public function up()
    {
        $this->renameColumn("{{%event}}", "country", "country_id");
        $this->alterColumn("{{%event}}", "country_id" , "INT(4) NOT NULL AFTER id");
    }

    public function down()
    {
        echo "m160516_093248_alterEvent cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
