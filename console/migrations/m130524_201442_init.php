<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->execute("
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema estravel
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema estravel
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `estravel` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `estravel` ;

-- -----------------------------------------------------
-- Table `estravel`.`migration`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`migration` (
  `version` VARCHAR(180) NOT NULL,
  `apply_time` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`version`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `estravel`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`user` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `auth_key` VARCHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `password_hash` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `password_reset_token` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `status` INT(1) UNSIGNED NOT NULL DEFAULT '10',
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `estravel`.`event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`event` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `status` INT(1) NULL DEFAULT 1,
  `start_time` DATETIME NULL DEFAULT NULL,
  `end_time` DATETIME NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `updated_by` INT(11) UNSIGNED NOT NULL,
  `city` VARCHAR(255) NOT NULL,
  `address_line1` VARCHAR(255) NOT NULL,
  `address_line2` VARCHAR(255) NULL DEFAULT NULL,
  `zipcode` VARCHAR(255) NULL,
  `country` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_event_user1_idx` (`created_by` ASC),
  INDEX `fk_event_user2_idx` (`updated_by` ASC),
  CONSTRAINT `fk_event_user1`
    FOREIGN KEY (`created_by`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_user2`
    FOREIGN KEY (`updated_by`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`booking` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `event_id` INT NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_booking_user_idx` (`user_id` ASC),
  INDEX `fk_booking_event1_idx` (`event_id` ASC),
  CONSTRAINT `fk_booking_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `estravel`.`event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`news`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`news` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `admin_id` INT(11) UNSIGNED NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `body` TEXT NOT NULL,
  `tags` VARCHAR(255) NULL,
  `published` INT(1) NOT NULL DEFAULT 1,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_news_user1_idx` (`admin_id` ASC),
  CONSTRAINT `fk_news_user1`
    FOREIGN KEY (`admin_id`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`ticket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`ticket` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `admin_id` INT(11) UNSIGNED NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `body` TEXT NOT NULL,
  `status` INT(1) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` DATETIME NULL,
  `updated_at` VARCHAR(45) NULL,
  PRIMARY KEY (`id`, `user_id`),
  INDEX `fk_ticket_user1_idx` (`user_id` ASC),
  INDEX `fk_ticket_user2_idx` (`admin_id` ASC),
  CONSTRAINT `fk_ticket_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ticket_user2`
    FOREIGN KEY (`admin_id`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`article`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`article` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `admin_id` INT(11) UNSIGNED NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `body` TEXT NOT NULL,
  `tags` VARCHAR(255) NULL,
  `published` INT(1) NOT NULL DEFAULT 1,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_news_user1_idx` (`admin_id` ASC),
  CONSTRAINT `fk_news_user10`
    FOREIGN KEY (`admin_id`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`article_comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`article_comment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `article_id` INT NOT NULL,
  `title` VARCHAR(255) NULL,
  `body` VARCHAR(2000) NOT NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`, `user_id`),
  INDEX `fk_comment_user1_idx` (`user_id` ASC),
  INDEX `fk_comment_article1_idx` (`article_id` ASC),
  CONSTRAINT `fk_comment_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_article1`
    FOREIGN KEY (`article_id`)
    REFERENCES `estravel`.`article` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`party`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`party` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `booking_id` INT UNSIGNED NOT NULL,
  `size` INT(5) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`, `booking_id`),
  INDEX `fk_party_booking1_idx` (`booking_id` ASC),
  CONSTRAINT `fk_party_booking1`
    FOREIGN KEY (`booking_id`)
    REFERENCES `estravel`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`flight`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`flight` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `booking_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_flight_booking1_idx` (`booking_id` ASC),
  CONSTRAINT `fk_flight_booking1`
    FOREIGN KEY (`booking_id`)
    REFERENCES `estravel`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`hotel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`hotel` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `booking_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_hotel_booking1_idx` (`booking_id` ASC),
  CONSTRAINT `fk_hotel_booking1`
    FOREIGN KEY (`booking_id`)
    REFERENCES `estravel`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`preferences`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`preferences` (
  `user_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  INDEX `fk_preferences_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_preferences_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`airport`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`airport` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `code` CHAR(3) NOT NULL,
  `city` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`users_airports`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`users_airports` (
  `id` INT(16) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `airport_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_airports_airport1_idx` (`airport_id` ASC),
  INDEX `fk_users_airports_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_users_airports_airport1`
    FOREIGN KEY (`airport_id`)
    REFERENCES `estravel`.`airport` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_airports_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`events_airports`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`events_airports` (
  `id` INT(16) NOT NULL AUTO_INCREMENT,
  `event_id` INT NOT NULL,
  `airport_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_events_airports_event1_idx` (`event_id` ASC),
  INDEX `fk_events_airports_airport1_idx` (`airport_id` ASC),
  CONSTRAINT `fk_events_airports_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `estravel`.`event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_events_airports_airport1`
    FOREIGN KEY (`airport_id`)
    REFERENCES `estravel`.`airport` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`content`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`content` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `action` VARCHAR(45) NOT NULL,
  `menu` VARCHAR(45) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `body` TEXT NOT NULL,
  `tags` VARCHAR(255) NULL DEFAULT NULL,
  `banner` VARCHAR(255) NULL DEFAULT NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`news_comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`news_comment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `news_id` INT NOT NULL,
  `title` VARCHAR(255) NULL,
  `body` VARCHAR(2000) NOT NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`, `user_id`),
  INDEX `fk_comment_user1_idx` (`user_id` ASC),
  INDEX `fk_news_comment_news1_idx` (`news_id` ASC),
  CONSTRAINT `fk_comment_user10`
    FOREIGN KEY (`user_id`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_news_comment_news1`
    FOREIGN KEY (`news_id`)
    REFERENCES `estravel`.`news` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`ticket_comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`ticket_comment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `ticket_id` INT NOT NULL,
  `ticket_user_id` INT(11) UNSIGNED NOT NULL,
  `title` VARCHAR(255) NULL,
  `body` VARCHAR(2000) NOT NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`, `user_id`),
  INDEX `fk_comment_user1_idx` (`user_id` ASC),
  INDEX `fk_ticket_comment_ticket1_idx` (`ticket_id` ASC, `ticket_user_id` ASC),
  CONSTRAINT `fk_comment_user11`
    FOREIGN KEY (`user_id`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ticket_comment_ticket1`
    FOREIGN KEY (`ticket_id` , `ticket_user_id`)
    REFERENCES `estravel`.`ticket` (`id` , `user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`event_comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`event_comment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `event_id` INT NOT NULL,
  `title` VARCHAR(255) NULL,
  `body` VARCHAR(2000) NOT NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`, `user_id`),
  INDEX `fk_comment_user1_idx` (`user_id` ASC),
  INDEX `fk_event_comment_event1_idx` (`event_id` ASC),
  CONSTRAINT `fk_comment_user12`
    FOREIGN KEY (`user_id`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_comment_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `estravel`.`event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estravel`.`booking_comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estravel`.`booking_comment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `booking_id` INT UNSIGNED NOT NULL,
  `title` VARCHAR(255) NULL,
  `body` VARCHAR(2000) NOT NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`, `user_id`),
  INDEX `fk_comment_user1_idx` (`user_id` ASC),
  INDEX `fk_booking_comment_booking1_idx` (`booking_id` ASC),
  CONSTRAINT `fk_comment_user13`
    FOREIGN KEY (`user_id`)
    REFERENCES `estravel`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_comment_booking1`
    FOREIGN KEY (`booking_id`)
    REFERENCES `estravel`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

            ");

    }

    public function down()
    {
        
    }
}
