<?php

use yii\db\Schema;
use yii\db\Migration;

class m150530_073709_eventTimezone extends Migration
{
    public function up()
    {
        $this->addColumn("{{%event}}", "timezone", "VARCHAR(50) NOT NULL AFTER `status`");
    }

    public function down()
    {
        echo "m150530_073709_eventTimezone cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
