<?php

use yii\db\Migration;

class m160408_143836_bookingOnBookedFlight extends Migration
{
    public function up()
    {
        $this->dropTable("{{%booking_comment}}");
        $this->execute("ALTER TABLE booking DROP id");
        $this->addColumn("{{%booking}}", "id", "INT(16) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT FIRST");
        $this->addColumn("{{%booked_flight}}", "booking_id", "INT(16) UNSIGNED NOT NULL AFTER `user_id`");
        $this->addForeignKey("fk_booked_flight_booking_id", '{{%booked_flight}}', 'booking_id', 'booking', 'id');
    }

    public function down()
    {
        echo "m160408_143836_bookingOnBookedFlight cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
