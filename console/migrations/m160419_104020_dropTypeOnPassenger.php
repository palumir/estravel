<?php

use yii\db\Migration;

class m160419_104020_dropTypeOnPassenger extends Migration
{
    public function up()
    {
        $this->dropColumn("{{%passenger}}", "type");
    }

    public function down()
    {
        echo "m160419_104020_dropTypeOnPassenger cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
