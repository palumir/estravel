<?php

use yii\db\Migration;

class m160506_173858_modify extends Migration
{
    public function up()
    {
        $this->alterColumn("{{%user}}", 'default_origin_airport', "VARCHAR(4) NULL");
    }

    public function down()
    {
        echo "m160506_173858_modify cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
