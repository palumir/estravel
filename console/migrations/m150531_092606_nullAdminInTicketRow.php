<?php

use yii\db\Schema;
use yii\db\Migration;

class m150531_092606_nullAdminInTicketRow extends Migration
{
    public function up()
    {
        $this->alterColumn("{{%ticket}}", "admin_id", "INT(11) UNSIGNED NULL DEFAULT NULL");
        $this->alterColumn("{{%ticket}}", "updated_at", "DATETIME NULL DEFAULT NULL");
        $this->alterColumn("{{%ticket_comment}}", "ticket_user_id", "INT(11) UNSIGNED NULL DEFAULT NULL");
    }

    public function down()
    {
        echo "m150531_092606_nullAdminInTicketRow cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
