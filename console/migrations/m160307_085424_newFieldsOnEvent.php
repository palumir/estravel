<?php

use yii\db\Schema;
use yii\db\Migration;

class m160307_085424_newFieldsOnEvent extends Migration
{
    public function up()
    {
        $this->addColumn("{{%user}}", "default_origin_airport", "CHAR(3) NULL AFTER `phone`");

        $this->addColumn("{{%event}}", "default_destination_airport", "CHAR(3) NULL AFTER `timezone`");
        $this->addColumn("{{%event}}", "default_departure_date", "DATETIME NOT NULL AFTER `end_time`");
        $this->addColumn("{{%event}}", "default_return_date", "DATETIME NOT NULL AFTER `default_departure_date`");
    }

    public function down()
    {
        echo "m160307_085424_newFieldsOnEvent cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}