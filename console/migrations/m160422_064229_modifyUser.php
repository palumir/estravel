<?php

use yii\db\Migration;

class m160422_064229_modifyUser extends Migration
{
    public function up()
    {
        $this->alterColumn("{{%user}}", "firstname", "VARCHAR(90) NULL DEFAULT ''");
        $this->alterColumn("{{%user}}", "lastname", "VARCHAR(90) NULL DEFAULT ''");
        $this->alterColumn("{{%user}}", "gender", "ENUM('M', 'F') NULL DEFAULT NULL");
        $this->alterColumn("{{%user}}", "phone", "VARCHAR(90) NULL DEFAULT ''");
    }

    public function down()
    {
        echo "m160422_064229_modifyUser cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
