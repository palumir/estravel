<?php

use yii\db\Migration;

class m160419_100242_nullOnBookedFlight extends Migration
{
    public function up()
    {
        $this->dropForeignKey("fk_booked_flight_booking_id", '{{%booked_flight}}');
        $this->alterColumn("{{%booked_flight}}", "booking_id", "INT(16) UNSIGNED NULL AFTER `user_id`");
        $this->addForeignKey("fk_booked_flight_booking_id", '{{%booked_flight}}', 'booking_id', 'booking', 'id');

        $this->dropForeignKey("fk_booked_flight_event_id", '{{%booked_flight}}');
        $this->alterColumn("{{%booked_flight}}", "event_id", "INT NULL AFTER `user_id`");
        $this->addForeignKey("fk_booked_flight_event_id", '{{%booked_flight}}', 'event_id', 'event', 'id');

        $this->dropForeignKey("fk_booked_hotel_booking_id", '{{%booked_hotel}}');
        $this->alterColumn("{{%booked_hotel}}", "booking_id", "INT(16) UNSIGNED NULL AFTER `user_id`");
        $this->addForeignKey("fk_booked_hotel_booking_id", '{{%booked_hotel}}', 'booking_id', 'booking', 'id');

        $this->dropForeignKey("fk_booked_hotel_event_id", '{{%booked_hotel}}');
        $this->alterColumn("{{%booked_hotel}}", "event_id", "INT(11) NULL AFTER `booking_id`");
        $this->addForeignKey("fk_booked_hotel_event_id", '{{%booked_hotel}}', 'event_id', 'event', 'id');
    }

    public function down()
    {
        echo "m160419_100242_nullOnBookedFlight cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
