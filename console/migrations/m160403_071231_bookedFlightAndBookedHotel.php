<?php

use yii\db\Migration;

class m160403_071231_bookedFlightAndBookedHotel extends Migration
{
    public function up()
    {
        // A separate booked flight model is required because the Faregrabbr API
        // has variable / field names which don't match, between the flight search response and the flight booking request and response
        // for example, flight search has two fields called origin and destination
        // while flight booking has "to" and "from" ... 
        // column data types are messed up because I did not receive proper information from Faregrabbr

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%booked_flight}}', [
                'id' => 'INT(11) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
                'user_id' => 'INT(11) UNSIGNED NOT NULL',
                'order_number' => 'VARCHAR(255) NOT NULL',
                'status' => 'TINYINT(1) NOT NULL',
                'status_text' => 'VARCHAR(255) NULL',
                'to' => 'CHAR(3) NOT NULL',
                'from' => 'CHAR(3) NOT NULL',
                'total_fare' => 'DECIMAL(8,2) NOT NULL',
                'base_fare' => 'DECIMAL(8,2) NOT NULL',
                'taxes' => 'DECIMAL(8,2) NOT NULL',
                'departure_datetime' => 'VARCHAR(255) NOT NULL',
                'return_datetime' => 'VARCHAR(255) NOT NULL',
                'depart_num_connections' => 'INT(2) NULL',
                'return_num_connections' => 'INT(2) NULL',
                'depart_flight_segments' => 'TEXT NULL',
                'return_flight_segments' => 'TEXT NULL',
                'record_locators' => 'TEXT NOT NULL',
                'created_at' => 'DATETIME NOT NULL',
                'updated_at' => 'DATETIME NOT NULL',
                'deleted' => 'TINYINT(1) NOT NULL DEFAULT "0"',
            ], $tableOptions);

        $this->addForeignKey("fk_user_id", '{{%booked_flight}}', 'user_id', 'user', 'id');

        $this->createTable('{{%passenger}}', [
                'id' => 'INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT',
                'booked_flight_id' => 'INT(11) UNSIGNED NOT NULL',
                'user_id' => 'INT(11) UNSIGNED NULL',
                'first_name' => 'VARCHAR(255) NOT NULL',
                'middle_name' => 'VARCHAR(255) NULL',
                'last_name' => 'VARCHAR(255) NOT NULL',
                'date_of_birth' => 'DATE NOT NULL',
                'area_code' => 'VARCHAR(255) NULL',
                'country_code' => 'VARCHAR(255) NULL',
                'full_phone_number' => 'VARCHAR(255) NULL',
                'number' => 'VARCHAR(255) NULL',
                'email' => 'VARCHAR(255) NULL',
                'gender' => 'CHAR(1) NULL',
            ], $tableOptions);

        $this->addForeignKey("fk_booked_flight_id", '{{%passenger}}', 'booked_flight_id', 'booked_flight', 'id');
        $this->addForeignKey("fk_user_passenger_id", '{{%passenger}}', 'user_id', 'user', 'id');




    }

    public function down()
    {
        echo "m160403_071231_bookedFlightAndBookedHotel cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
