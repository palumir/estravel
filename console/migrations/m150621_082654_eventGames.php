<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_082654_eventGames extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%game}}', [
                'id' => 'INT(4) NOT NULL PRIMARY KEY AUTO_INCREMENT',
                'name' => 'VARCHAR(255) NOT NULL',
                'version' => 'VARCHAR(50) NULL DEFAULT NULL',
                'created_at' => 'DATETIME NOT NULL',
                'updated_at' => 'DATETIME NOT NULL',
            ], $tableOptions);

        $this->createTable('{{%event_game}}', [
                'id' => 'INT(16) NOT NULL PRIMARY KEY AUTO_INCREMENT',
                'event_id' => 'INT(11) NOT NULL',
                'game_id' => 'INT(4) NOT NULL',
            ], $tableOptions);

        $this->addForeignKey("fk_events_ids", '{{%event_game}}', 'event_id', 'event', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("fk_games_ids", '{{%event_game}}', 'game_id', 'game', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex("unique_event_game_ids", '{{%event_game}}', ['event_id', 'game_id'], true);
    }

    public function down()
    {
        echo "m150621_082654_eventGames cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
