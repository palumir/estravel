<?php

use yii\db\Migration;

class m160408_142336_eventRelationOnBookedFlight extends Migration
{
    public function up()
    {
        $this->addColumn("{{%booked_flight}}", "event_id", "INT NOT NULL AFTER `user_id`");
        $this->addForeignKey("fk_booked_flight_event_id", '{{%booked_flight}}', 'event_id', 'event', 'id');
    }

    public function down()
    {
        echo "m160408_142336_eventRelationOnBookedFlight cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
