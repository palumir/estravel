<?php

use yii\db\Migration;

class m160704_101649_forgotRecordLocatorOnBookedHotel extends Migration
{
    public function up()
    {
        $this->addColumn("{{%booked_hotel}}", "record_locators", "TEXT NOT NULL DEFAULT '' AFTER description");
    }

    public function down()
    {
        echo "m160704_101649_forgotRecordLocatorOnBookedHotel cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
