<?php

use yii\db\Schema;
use yii\db\Migration;

class m150622_061105_newColumnsOnEvent extends Migration
{
    public function up()
    {
        $this->addColumn("{{%event}}", 'intro', 'VARCHAR(255) NOT NULL');
        $this->addColumn("{{%event}}", 'logo', "VARCHAR(255) NULL DEFAULT 'default_logo.png'");
        $this->addColumn("{{%event}}", 'summary', 'VARCHAR(5000) NULL DEFAULT NULL');
        $this->addColumn("{{%event}}", 'description', 'VARCHAR(5000) NULL DEFAULT NULL');
    }

    public function down()
    {
        echo "m150622_061105_newColumnsOnEvent cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
