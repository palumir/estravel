<?php

use yii\db\Migration;

class m160508_163158_oops extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%country}}', 'country_name', 'name');
    }

    public function down()
    {
        echo "m160508_163158_oops cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
