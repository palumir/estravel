<?php

use yii\db\Schema;
use yii\db\Migration;

class m150530_061623_insertAdminator extends Migration
{
    public function up()
    {
        $this->addColumn("{{%user}}", "role", "INT(1) NOT NULL DEFAULT 1 AFTER `status`");
        $this->insert('user', [
            'username' => 'Adminator',
            'auth_key' => 'someauthkey',
            // what's the password?
            'password_hash' => '$2y$13$okfihMqVy0yYXqBggShy/.tMd07Got4p3y7jGqZOAWVG2/9nRb/9G',
            'password_reset_token' => NULL,
            'email' => 'doesnt@matter.com',
            'status' => 1,
            'role' => 1,
            'created_at' => '2015-04-19 09:34:03',
            'updated_at' => '2015-04-19 09:34:03',
            ]);
    }

    public function down()
    {
        echo "m150530_061623_insertAdminator cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
