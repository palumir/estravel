<?php

use yii\db\Migration;

class m160408_152405_bookedHotelTable extends Migration
{
    public function up()
    {
        // A separate booked hotel model is required because the Faregrabbr API
        // has variable / field names which don't match, between the hotel search response and the hotel booking request and response
        // column data types are messed up because I did not receive proper information from Faregrabbr
        // so really I'm not going to be able to write in proper validation
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%booked_hotel}}', [
                'id' => 'INT(11) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
                'user_id' => 'INT(11) UNSIGNED NOT NULL',
                'booking_id' => 'INT(16) UNSIGNED NOT NULL',
                'event_id' => 'INT(11) NOT NULL',

                'order_number' => 'VARCHAR(255) NOT NULL',
                'status' => 'TINYINT(1) NOT NULL',
                'status_text' => 'VARCHAR(255) NULL',
                'checkin' => 'DATE NOT NULL',
                'checkout' => 'DATE NOT NULL',
                'cancellation' => 'VARCHAR(255) NULL',

                'hotelCode' => 'VARCHAR(255) NOT NULL',
                'hotelChain' => 'VARCHAR(255) NOT NULL',
                'hotelName' => 'VARCHAR(255) NOT NULL',
                'address_line_1' => 'VARCHAR(255) NOT NULL',
                'address_line_2' => 'VARCHAR(255) NULL',
                'phone_number' => 'VARCHAR(255) NULL',
                'description' => 'TEXT NULL',

                'created_at' => 'DATETIME NOT NULL',
                'updated_at' => 'DATETIME NOT NULL',
                'deleted' => 'TINYINT(1) NOT NULL DEFAULT "0"',
            ], $tableOptions);

        $this->addForeignKey("fk_booked_hotel_user_id", '{{%booked_hotel}}', 'user_id', 'user', 'id');
        $this->addForeignKey("fk_booked_hotel_booking_id", '{{%booked_hotel}}', 'booking_id', 'booking', 'id');
        $this->addForeignKey("fk_booked_hotel_event_id", '{{%booked_hotel}}', 'event_id', 'event', 'id');

        $this->createTable('{{%guest}}', [
                'id' => 'INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT',
                'booked_hotel_id' => 'INT(11) UNSIGNED NOT NULL',
                'user_id' => 'INT(11) UNSIGNED NULL',
                'first_name' => 'VARCHAR(255) NOT NULL',
                'middle_name' => 'VARCHAR(255) NULL',
                'last_name' => 'VARCHAR(255) NOT NULL',
                'date_of_birth' => 'DATE NOT NULL',
                'area_code' => 'VARCHAR(255) NULL',
                'country_code' => 'VARCHAR(255) NULL',
                'full_phone_number' => 'VARCHAR(255) NULL',
                'number' => 'VARCHAR(255) NULL',
                'phone' => 'VARCHAR(255) NULL',
                'email' => 'VARCHAR(255) NULL',
                'gender' => 'CHAR(1) NULL',
            ], $tableOptions);

        $this->addForeignKey("fk_guest_booked_hotel_id", '{{%guest}}', 'booked_hotel_id', 'booked_hotel', 'id');
        $this->addForeignKey("fk_guest_user_id", '{{%guest}}', 'user_id', 'user', 'id');

        $this->addColumn("{{%passenger}}", "phone", "VARCHAR(255) NULL AFTER `email`");
    }

    public function down()
    {
        echo "m160408_152405_bookedHotelTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
