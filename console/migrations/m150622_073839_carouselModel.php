<?php

use yii\db\Schema;
use yii\db\Migration;

class m150622_073839_carouselModel extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable("{{%carousel}}", [
            'id' => 'INT(6) NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'rank' => 'INT(2) NULL DEFAULT NULL',
            'title' => 'VARCHAR(255) NOT NULL',
            'image' => 'VARCHAR(255) NOT NULL',
            'summary' => 'VARCHAR(900) NULL DEFAULT NULL',
            'description' => 'VARCHAR(5000) NULL DEFAULT NULL',
            ], $tableOptions);
    }

    public function down()
    {
        echo "m150622_073839_carouselModel cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
