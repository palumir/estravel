<?php

use yii\db\Migration;

class m160420_173153_addEventIdToCarousel extends Migration
{
    public function up()
    {
        $this->addColumn("{{%carousel}}", "link", "VARCHAR(2000) NULL DEFAULT NULL AFTER description");
        $this->addColumn("{{%carousel}}", "event_id", "INT(11) NULL DEFAULT NULL AFTER id");
        $this->addForeignKey("fk_carousel_event_id", '{{%carousel}}', 'event_id', 'event', 'id');
    }

    public function down()
    {
        echo "m160420_173153_addEventIdToCarousel cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
