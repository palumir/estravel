<?php

use yii\db\Migration;

class m160422_114915_venueOnEvent extends Migration
{
    public function up()
    {
        $this->addColumn("{{%event}}", "venue", "VARCHAR(255) NOT NULL AFTER default_destination_airport");
    }

    public function down()
    {
        echo "m160422_114915_venueOnEvent cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
