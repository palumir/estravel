<?php

use yii\db\Schema;
use yii\db\Migration;

class m160307_103723_cleanDatabaseAccordingToNewRequirements extends Migration
{
    public function up()
    {
        $this->dropTable("{{%hotel}}");
        $this->dropTable("{{%events_airports}}");
        $this->dropTable("{{%flight}}");
        $this->dropTable("{{%party}}");
        $this->dropTable("{{%preferences}}");
        $this->dropTable("{{%users_airports}}");
    }

    public function down()
    {
        echo "m160307_103723_cleanDatabaseAccordingToNewRequirements cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
