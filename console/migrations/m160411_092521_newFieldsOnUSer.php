<?php

use yii\db\Migration;

class m160411_092521_newFieldsOnUSer extends Migration
{
    public function up()
    {
        $this->addColumn("{{%user}}", "gender", "ENUM('M', 'F') NOT NULL AFTER `lastname`");
        $this->addColumn("{{%user}}", "middlename", "VARCHAR(90) NULL AFTER `firstname`");
    }

    public function down()
    {
        echo "m160411_092521_newFieldsOnUSer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
