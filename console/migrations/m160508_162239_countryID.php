<?php

use yii\db\Migration;

class m160508_162239_countryID extends Migration
{
    public function up()
    {
        $this->dropColumn("{{%user}}", 'country');
        $this->addColumn("{{%user}}", 'country_id', "INT(4) NULL DEFAULT NULL AFTER airport_id");
        $this->addForeignKey("user_has_country", "{{%user}}", "country_id", "{{%country}}", "id", "NO ACTION", "NO ACTION");
    }

    public function down()
    {
        echo "m160508_162239_countryID cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
