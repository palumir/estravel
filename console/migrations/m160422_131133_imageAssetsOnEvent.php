<?php

use yii\db\Migration;

class m160422_131133_imageAssetsOnEvent extends Migration
{
    public function up()
    {
        $this->addColumn("{{%event}}", 'website', 'VARCHAR(255) NULL DEFAULT NULL after logo');
        $this->addColumn("{{%event}}", 'facebook', 'VARCHAR(255) NULL DEFAULT NULL after logo');
        $this->addColumn("{{%event}}", 'background', 'VARCHAR(255) NULL DEFAULT NULL after logo');
        $this->addColumn("{{%event}}", 'image', 'VARCHAR(255) NULL DEFAULT NULL after logo');
        $this->addColumn("{{%event}}", 'thumbnail', 'VARCHAR(255) NULL DEFAULT NULL after logo');
    }

    public function down()
    {
        echo "m160422_131133_imageAssetsOnEvent cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
