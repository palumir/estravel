<?php

use yii\db\Migration;

class m160516_122430_moreOnEvent extends Migration
{
    public function up()
    {
        $this->renameColumn("{{%event}}", "default_destination_airport", "airport_id");
        $this->alterColumn("{{%event}}", "airport_id" , "INT(5) NULL AFTER country_id");
        // this doesn't run, so don't run it just use php code to establish the fk connection
        // $this->addForeignKey("event_has_airport_fk", "{{%event}}", "airport_id", "{{%airport}}", "id", "NO ACTION", "NO ACTION");
    }

    public function down()
    {
        echo "m160516_122430_moreOnEvent cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
