<?php
use yii\helpers\Url;

?>

<a class="title-link" href="<?= Url::to(['news/view', 'id' => $model->id]); ?>"><h2><?= $model->title ?></h2></a> 
<p><small>Posted On: <?php echo Yii::$app->formatter->asDate($model->created_at, "long") ?> by <?php echo $model->admin->fullName(); ?></small></p>
<div class="news-body">
	<?= $model->body ?>
</div>
<div class="tags">
	<p class="tags"><small><?= $model->tags ?></small></p>
</div>
<div class="news-info">	
	<p>Comments: <strong><?= $model->countNewsComments() ?> </strong></p>
</div>