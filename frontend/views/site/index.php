<?php

use yii\bootstrap\Carousel;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\jui\Accordion;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\EventSearch */
$this->title = 'eSports Travel';
$place_url = \yii\helpers\Url::to(['place/ajax-list']);
$airport_url = \yii\helpers\Url::to(['airport/ajax-list']);
$airportFullname_url = \yii\helpers\Url::to(['airport/ajax-fullname']);

// Event list for javaScript.
$eventList = [];
for($i = 0; $i < count($listOfEvents); $i++) {
	$singleEvent = [];
	
	$singleEvent["event_id"] = $listOfEvents[$i]->id;
	$singleEvent["event_name"] = $listOfEvents[$i]->name;
	
	$departure_array = explode(" ", $listOfEvents[$i]->default_departure_date);
	$departure_date = $departure_array[0];
	$singleEvent["default_departure_date"] = $departure_date;
	
	$return_array = explode(" ", $listOfEvents[$i]->default_return_date);
	$return_date = $return_array[0];
	$singleEvent["default_return_date"] = $return_date;

	array_push($eventList, $singleEvent);
}
?>

    <script type="text/javascript">

     function IsCheck(number){
	//here we iterate the three divs
	for(i = 1; i <= 2; i++){
		//if the current number (i) is different to the checked input (number)
		if (i != number) {
			document.getElementById('ifChecked'+i).style.display = 'none';
		}
		else {
			document.getElementById('ifChecked'+i).style.display = 'block';
		}
	}}
	
	var events = <?php echo json_encode($eventList); ?>;
	
	function eventSelected(eventName, whichForm) {
		for(i = 0; i < events.length; i++) {
			if(eventName == events[i]["event_name"]) {
				if(whichForm == "w2") {
					document.getElementById('hotelsearchform-checkin').value = events[i]["default_departure_date"];
					document.getElementById('hotelsearchform-checkout').value = events[i]["default_return_date"];
					document.getElementById('hotelsearchform-event_id').value = events[i]["event_id"];
				}
				if(whichForm == "w1") {
					document.getElementById('flightsearchform-departure').value = events[i]["default_departure_date"];
					document.getElementById('flightsearchform-return').value = events[i]["default_return_date"];
					document.getElementById('flightsearchform-event_id').value = events[i]["event_id"];
				}
			}
		}
	}
	
	// 

    </script>

<div class="site-index">
    <?php
    echo Carousel::widget([
        'controls' => ['', ''],
        'items' => $carousels,
        'showIndicators' => true,
        'options' => ['class' => '']
        ]);
        ?>
				                   
				<div class="col-xs-12 col-sm-4 content-div quicksearch">
				<input type = "radio" onclick = "IsCheck(1);" name = "plane" id = "buttonCheck1" checked> <img src="../images/planeIcon.png"> Flights
				<input type = "radio" onclick = "IsCheck(2);" name = "plane" id = "buttonCheck2"> <img src="../images/hotelIcon.png"> Hotels
						
						<div id="searches">
						<div id = "ifChecked1">
						
								 <?php 

			///////////////////////////////////
			////////// FLIGHT SEARCH START ////
			///////////////////////////////////
            $form = ActiveForm::begin([
                'method' => 'get',
                'action' => ['flight/search-round-trip'],
				'options' => [
					'class' => 'spinner-form'
				]
            ]); 

        ?>

				
		<div class="col-xs-12 col-sm-12 col-md-12 field-flightsearchform-class">
	   <div class="col-xs-12">
       <label class="control-label" for = "event_id">Event</label>
		<input list="events" class="form-control" id="flight-search-form-event_id" name="FlightSearchForm[event_id]" value="<?= $flightSearchForm->event_id ?>" placeholder = "Event Name">
		<datalist id="events">
		 <?php
			foreach($listOfEvents as $currEvent) { 
				print('<option name="event_id" value="' . $currEvent->name . '">' . $currEvent->name . '</option>');
				 
			}
		  ?>
		</datalist>
		<?php 			
		$this->registerJs("
			document.getElementById('flight-search-form-event_id').addEventListener('input', function () {
				var val = this.value;
				if($('#events').find('option').filter(function(){
					return this.value.toUpperCase() === val.toUpperCase();        
				}).length) {
					//send ajax request
					eventSelected(this.value, this.form.id);
				}

			});
		");
		?>
		<input type="hidden" value="<?php echo $flightSearchForm->event_id; ?>" name="event_id" id="flightsearchform-event_id"/>
		</div>
		<br>
		</div>	
		<div class="col-xs-12">
			   <?php if($flightSearchForm->origin == NULL || $flightSearchForm->origin=="") $flightSearchForm->origin = (!Yii::$app->user->isGuest && Yii::$app->user->identity->airport)?Yii::$app->user->identity->airport->iata_code:""; ?>
		       <?php echo $form->field($flightSearchForm, 'origin', ['options' => ['class' => 'col-xs-12 col-sm-12 col-md-12']])->widget(Select2::classname(), [
                    // 'initValueText' => "here here", // set the initial display name
                    // 'theme' => Select2::THEME_DEFAULT,
                    // 'options' => ['placeholder' => 'Your preferred airport ...'],
                    'pluginOptions' => [
                        // 'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $airport_url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression("

                            function(airport) { 
                                if (airport.name) {
                                    return airport.name;
                                } else {
                                    return airport.text;
                                }
                            }

                            "),
                        'templateSelection' => new JsExpression("

                            // this is a complicated fix to my problem but a fix nonetheless
                            function (airport) {
                                if (airport.name) {
                                    return airport.name;
                                } else {
                                    return airport.text;
                                }
                            }

                            "),
                    ],
                ]); ?>
		</div>
		<div class="col-xs-12">
        <?php echo $form->field($flightSearchForm, 'departure', ['options' => ['class' => 'col-xs-7 col-sm-12 col-md-6']])->widget(DatePicker::className()) ?>
		<?php echo $form->field($flightSearchForm, 'return', ['options' => ['class' => 'col-xs-7 col-sm-12 col-md-6']])->widget(DatePicker::className()); ?>
		</div>
		<div class="col-xs-12">
        <?php echo $form->field($flightSearchForm, 'adults', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-4']])->dropDownList([1=>1, 2=>2, 3=>3, 4=>4, 5=>5]) ?>
        <?php echo $form->field($flightSearchForm, 'children', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-4']])->dropDownList([0=>0, 1=>1, 2=>2, 3=>3, 4=>4, 5=>5]) ?>
        <?php echo $form->field($flightSearchForm, 'infants', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-4']])->dropDownList([0=>0, 1=>1, 2=>2, 3=>3, 4=>4, 5=>5]) ?>
        <?php echo $form->field($flightSearchForm, 'class', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-12']])->dropDownList(['economy' => 'Economy', 'business' => 'Business', 'first' => 'First', ]) ?>
		<input type="hidden" value="1" name="default" />
		</div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Search</button>
        </div>
		<div class="spinner"></div>
        <?php 
		ActiveForm::end(); 

		////////////////////////////////////
		////////// FLIGHT SEARCH END ///////
		////////////////////////////////////
								?>

		</div>
		
		<div id = "ifChecked2" style="display:none">

		<?php 			
								
			//////////////////////////////////////
			////////// HOTEL SEARCH BEGIN ////////
			//////////////////////////////////////
								
            $form = ActiveForm::begin([
                'method' => 'get',
                'action' => ['hotel/search'],
				'options' => [
					'class' => 'spinner-form'
				]
            ]); 
        ?>

		<div class="col-xs-12 col-sm-12 col-md-12 field-hotelsearchform-class">
		<div class="col-xs-12">
        <label class="control-label" for = "event_id"> Event</label>
		<input list="events" class="form-control" id="hotel-search-form-event_id"" name="HotelSearchForm[event_id]" value="<?= $hotelSearchForm->event_id ?>" placeholder = "Event Name">
		<datalist id="events">
		 <?php
			foreach($listOfEvents as $currEvent) { 
				print('<option name="event_id" value="' . $currEvent->name . '">' . $currEvent->name . '</option>');
				 
			}
		  ?>
		</datalist>
		<?php 			
		$this->registerJs("
			document.getElementById('hotel-search-form-event_id').addEventListener('input', function () {
				var val = this.value;
				if($('#events').find('option').filter(function(){
					return this.value.toUpperCase() === val.toUpperCase();        
				}).length) {
					//send ajax request
					eventSelected(this.value, this.form.id);
				}

			});
		");
		?>
		<input type="hidden" value="<?php echo $hotelSearchForm->event_id; ?>" name="event_id" id="hotelsearchform-event_id"/>
		</div>
		<input type="hidden" value="1" name="default" />
		<div></div>
		</div>
		<div class="col-xs-12">
        <?php echo $form->field($hotelSearchForm, 'checkin', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-6']])->widget(DatePicker::className()) ?>
        <?php echo $form->field($hotelSearchForm, 'checkout', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-6']])->widget(DatePicker::className()) ?>
		</div>
		<div class="col-xs-12">
        <?php echo $form->field($hotelSearchForm, 'numrooms', ['options' => ['class' => 'col-xs-12 col-sm-12 col-md-12']])->dropDownList([1=>1, 2=>2, 3=>3, 4=>4, 5=>5]) ?>
		<input type="hidden" value="1" name="HotelSearchForm[guests]" />
		</div>
		
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Search</button>
        </div>
		<div class="spinner"></div>
        <?php ActiveForm::end(); 
		
								////////////////////////////////////
								////////// HOTEL SEARCH END ////////
								////////////////////////////////////
		
		?>
			</div>
		<script>
		// Display the currently checked search.
		if(document.getElementById('buttonCheck1').checked) IsCheck(1);
		if(document.getElementById('buttonCheck2').checked) IsCheck(2);
		</script>
					
               </div>
			 </div>
                <div class="articles col-xs-12 col-sm-8">

						
                    <?php
                    echo ListView::widget([
                        'summary' => '',
                        'dataProvider' => $articleDataProvider,
                        'itemOptions' => ['class' => 'item article center-block'],
                        'itemView' => '_article',
                        'separator' => "<hr>",
                        // 'viewParams' => [
                        //         ],
                        ]);
                    ?>
                </div>
			</div>
