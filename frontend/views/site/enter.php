<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $signupForm \frontend\models\SignupForm */

$this->title = 'Log In';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-enter">
    <div class="row">
        <div class="col-xs-12 col-sm-5">
    <h1>Sign up</h1>
            <p>Please fill out the following fields to signup:</p>

                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                <div>
                        <?= $form->field($signupForm, 'username') ?>
                        <?= $form->field($signupForm, 'email') ?>
                        <?= $form->field($signupForm, 'password')->passwordInput() ?>
                        <?= $form->field($signupForm, 'password_repeat')->passwordInput() ?>
                </div>
                <div>
                        <?= $form->field($signupForm, 'verifyCode')->widget(Captcha::className(), [
                            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                        ]) ?>
                </div>
                <div class="col-xs-12">            
                        <div class="form-group">
                            <?= Html::submitButton('Signup', ['class' => 'btn btn-star', 'name' => 'signup-button']) ?>
                        </div>
                </div>
                    <?php ActiveForm::end(); ?>
        </div>
        <div class="col-xs-12 col-sm-5 col-sm-offset-2">
            <h1>Log In</h1>
            <p>Or fill out the following fields to login:</p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($loginForm, 'username') ?>
                <?= $form->field($loginForm, 'password')->passwordInput() ?>
                <?= $form->field($loginForm, 'rememberMe')->checkbox() ?>
                <div style="color:#999;margin:1em 0">
                    If you forgot your password you can <?= Html::a('reset it', ['user/request-password-reset']) ?>.
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-star', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>