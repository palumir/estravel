<?php
use yii\helpers\Url;

?>

<h2><a class="title-link" href="<?= Url::to(['article/view', 'id' => $model->id]); ?>"><?= $model->title ?></a></h2>
<p><small>Posted On: <?php echo Yii::$app->formatter->asDate($model->created_at, "long") ?> by <?php echo $model->admin->fullName(); ?></small></p>
<div class="article-body">
	<?= $model->body ?>
</div>
<div class="tags">
	<p class="tags"><small><?= $model->tags ?></small></p>
</div>
<div class="article-info">	
	<p>Comments: <strong><?= $model->countArticleComments() ?> </strong></p>
</div>