<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Game;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\EventSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-search col-sm-12">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'form-horizontal row']
    ]); ?>

    <?= $form->field($model, 'name', ['options' => ['class' => 'col-sm-3'], 'inputOptions' => ['class' => 'form-control', 'placeholder' => 'Event Name']])->label(false) ?>

    <?= $form->field($model, 'city', ['options' => ['class' => 'col-sm-3'], 'inputOptions' => ['class' => 'form-control', 'placeholder' => 'City']])->label(false) ?>

    <?= $form->field($model, 'game.id', ['options' => ['class' => 'col-sm-3']])->dropDownList(Game::getDropDownArray())->label(false) ?>


    <div class="col-sm-3">

        <?= Html::submitButton('Filter', ['class' => 'btn col-sm-12 btn-travel']) ?>

    </div>

    <?php ActiveForm::end(); ?>
</div>
