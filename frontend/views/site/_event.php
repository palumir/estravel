<?php
use yii\helpers\Html;
use frontend\models\Event;

?>

<div class="col-sm-4">
	<div class="event" style="background-image: url('<?= $model->logo ?>'); background-position: center;">
	    <h2 class="text-center"><?php echo $model->name ?></h2>

	    <div class="event-info">
		    <p><?php echo $model->city . ", " . Event::$countries[$model->country] ?></p>
		    <p><?php echo $model->gamesString ?></p>
		    <p><?php echo $model->intro ?></p>
		    <p><?php echo Yii::$app->formatter->asDate($model->start_time, "php:d") . "-" . Yii::$app->formatter->asDate($model->end_time, "php:dS F Y") ?></p>
	    </div>

	    <p class="book"><?php echo (!Yii::$app->user->isGuest)?Html::a("<span class='glyphicon glyphicon-off'></span> Book Now", ['booking/create', 'event_id' => $model->id], ['class' => 'btn btn-block btn-power']):""; ?></p>
	    <p class="view"><?= Html::a("<span class='glyphicon glyphicon-off'></span> View Details", ['event/view', 'id' => $model->id], ['class' => 'btn btn-block btn-game']); ?></p>
	</div>

</div>