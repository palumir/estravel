<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
        <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'password_repeat')->passwordInput() ?>
                <?= $form->field($model, 'default_origin_airport') ?>
        </div>
        <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'firstname') ?>
                <?= $form->field($model, 'lastname') ?>
                <?= $form->field($model, 'phone') ?>
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>
        </div>
        <div class="col-xs-12">            
                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-star', 'name' => 'signup-button']) ?>
                </div>
        </div>
            <?php ActiveForm::end(); ?>
    </div>
</div>
