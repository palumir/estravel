<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\BookedHotel */

$this->title = 'Create Booked Hotel';
$this->params['breadcrumbs'][] = ['label' => 'Booked Hotels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booked-hotel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
