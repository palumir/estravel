<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\BookedHotelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Booked Hotels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booked-hotel-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Booked Hotel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'booking_id',
            'event_id',
            'order_number',
            // 'status',
            // 'status_text',
            // 'checkin',
            // 'checkout',
            // 'cancellation',
            // 'hotelCode',
            // 'hotelChain',
            // 'hotelName',
            // 'address_line_1',
            // 'address_line_2',
            // 'phone_number',
            // 'description:ntext',
            // 'created_at',
            // 'updated_at',
            // 'deleted',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
