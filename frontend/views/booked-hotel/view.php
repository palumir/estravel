<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Airport;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model frontend\models\BookedFlight */

$this->title = "Hotel at " . $model->event->name;
$this->params['breadcrumbs'][] = ['label' => 'My bookings', 'url' => ['booking/index']];
$this->params['breadcrumbs'][] = ['label' => $model->event->name, 'url' => ['booking/view', 'id' => $model->booking_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booked-flight-view">

    <h1><?= Html::encode($this->title) ?></h1>

        <div class="hotel-list-item row">
            <div class="col-xs-12 col-sm-12">
                <h2><?= $model->hotelChain . " - " . $model->hotelName . " - " . $model->hotelCode ?></h2>
                <p><strong>Checkin:</strong> <?= $model->checkin ?></p>
                <p><strong>Checkout:</strong> <?= $model->checkout ?></p>
                <p><strong>Address:</strong> <?= $model->address_line_1 . " " . $model->address_line_2 ?></p>
            </div>
        </div>

</div>

<div class="row">
    <div class="col-xs-12 content-div">
        <p><strong>Cancellation:</strong> <?= $model->cancellation ?></p>
        <p><strong>Phone number:</strong> <?= $model->phone_number ?></p>
        <p><strong>Description:</strong></p>
        <?= $model->description ?>
    </div>
    &nbsp;

    <div class="col-xs-12 col-sm-6 col-md-3 content-div">
        <h3>Guests</h3>
        <?php foreach ($model->guests as $guest): ?>
            <?php 

            echo DetailView::widget([
                'options' => [
                    'class' => 'table table-condensed'
                ],
                'model' => $guest,
                'attributes' => [
                    [
                        'attribute' => 'first_name',
                        'label' => 'Name',
                        'value' => $guest->first_name . " " . $guest->last_name,
                    ],
                    [
                        'attribute' => 'date_of_birth',
                        'value' => Yii::$app->formatter->asDate($guest->date_of_birth),
                    ],
                    [
                        'attribute' => 'email',
                    ],
                    [
                        'attribute' => 'gender',
                    ],
                ],
                ]);

            ?>
        <?php endforeach ?>
        
    </div>

</div>