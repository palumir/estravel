<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Support Tickets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('New Ticket', ['create'], ['class' => 'btn btn-power']) ?>
    </p>

    <?= GridView::widget([
        'summary' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-striped'],
        'filterPosition' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
                [
                    'attribute' => 'status',
                    'format' => 'html',
                    'label' => 'Ticket Status',
                    'value' => function($model){
                        return "<p class=".$model->colorText().">".$model->getStatusString()."</p>";
                    },
                ],
            'created_at:datetime',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => false,
                    'template' => '{view}',
                        'buttons' => [
                        'view' => function($url, $model, $key) {
                            return Html::a('View Ticket', ['ticket/view', 'id' => $model->id], ['class' => 'btn btn btn-sm btn-block btn-game']);
                        },
                        ],
                ],
        ],
    ]); ?>

</div>
