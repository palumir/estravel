<?php
use yii\bootstrap\Collapse;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\i18n\Formatter;

$formatter = new Formatter(); 

?>
		<?php if (Yii::$app->user->id === $model->user_id): ?>
		 	<?= 
		 				Html::a("<span class='glyphicon glyphicon-remove'></span>", 
		 					[
		 						'ticket-comment/delete', 'id' => $model->id, 'user_id' => Yii::$app->user->id, 'ticket_id' => $ticket->id
		 					], 
		 					[
		 					'class' => 'pull-right text-danger comment-delete', 
		 					'data' => [
                				'confirm' => 'Are you sure you want to delete your comment?',
                				'method' => 'post',],
                			]); 
            ?>
		<?php endif ?>

    	
    	<?=  Html::tag('p', Html::encode($model->body)); ?>
    	<p class="text-left comment-by col-lg-6"><small> <?= Html::encode($model->user->username) ?></small></p>
    	<p class="text-right comment-at col-lg-6"><small> <?= $formatter->asDate($model->created_at, "php:d M Y") ?></small></p>
    	<br>