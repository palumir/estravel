<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view content-div">
    <div class="news">    
        <h1><?= Html::encode($this->title) ?></h1>

        <?= $model->body ?>
        
    <p>Author: <strong><?= $model->admin->fullName() ?> </strong></p>
    <p>Published: <strong><?php echo Yii::$app->formatter->asDatetime($model->created_at) ?> </strong></p>
    </div>
</div>
            <?php $form = ActiveForm::begin([
                'action' => ['news-comment/post', 'news_id' => $model->id],
                'options' => ['class' => 'news-comment-form'],
            ]); ?>

            <?= $form->field($comment, 'body')->textarea(['maxlength' => 1000])->label(false)->error(false) ?>
            <?= $form->field($comment, 'news_id')->hiddenInput(['value' => $model->id])->label(false)->error(false); ?>

            <div class="form-group">
                <?= Html::submitButton('Post Comment', ['class' => 'btn btn-star btn-block']) ?>
            </div>

            <?php ActiveForm::end(); ?>

                                <?php Pjax::begin(['id' => 'pjax-comments']); ?>

                                        <?=
                                        ListView::widget([
                                            'summary' => false,
                                            'dataProvider' => $commentDataProvider,
                                            'itemOptions' => ['class' => 'comment'],
                                            'itemView' => '_comment',
                                        ]);
                                        ?>
                                <?php Pjax::end(); ?>