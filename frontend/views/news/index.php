<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">
    <div class="row">
        <div class="col-xs-12">
            <?php
            echo ListView::widget([
                'summary' => '',
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item news center-block content-div'],
                'itemView' => '_news',
                'separator' => "<hr>"
                // 'viewParams' => [
                //         ],
            ]);
            ?>
        </div>
    </div>
</div>
