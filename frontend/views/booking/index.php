<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'My bookings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-index row">
    <div class="col-xs-12">
        <h2><?= Html::encode("Outstanding Bookings") ?></h2>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
		
		<?= GridView::widget([
            'summary' => false,
            'dataProvider' => $outstandingDataProvider,
            'tableOptions' => ['class' => 'table table-striped'],
            'columns' => [
                // ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    // 'user.username:text:User',
                     [
                     'attribute' => 'event',
                     'value' => 'event.name'
                     ],
                    'created_at:datetime:Saved On',
                    /*[
                        'header' => "Booked Flights",
                        'format' => 'html',
                        'contentOptions' => ['align' => 'center'],
                        'value' => function ($model) {
                            return ($model->hasBookedFlights())?"<span class='text-success glyphicon glyphicon-ok'></span>":"<span class='text-danger glyphicon glyphicon-remove'></span>";
                        },
                    ],
                    [
                        'header' => "Booked Hotels",
                        'format' => 'html',
                        'contentOptions' => ['align' => 'center'],
                        'value' => function ($model) {
                            return ($model->hasBookedHotels())?"<span class='text-success glyphicon glyphicon-ok'></span>":"<span class='text-danger glyphicon glyphicon-remove'></span>";
                        },
                    ],*/
					[
                        'class' => 'yii\grid\ActionColumn',
                        'header' => false,
                        'template' => '{clear}',
                            'buttons' => [
							'clear' => function($url, $model, $key) {
                                return Html::a('Clear', ['booking/delete-saved-booking', 'id' => $model->id, 'type' => 'all'], ['class' => 'btn btn btn-sm btn-block btn-power']);
                            },
                            ],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => false,
                        'template' => '{view}',
                            'buttons' => [
                            'view' => function($url, $model, $key) {
                                return Html::a('View', ['booking/view', 'id' => $model->id], ['class' => 'btn btn btn-sm btn-block btn-game']);
                            },
                            ],
                    ],
            ],
        ]); ?>

		
	<?php
	if(count($confirmedDataProvider->getModels()) > 0) :
	?>
	   <h2><?= Html::encode("Confirmed Bookings") ?></h2>
        <?= GridView::widget([
            'summary' => false,
            'dataProvider' => $confirmedDataProvider,
            'tableOptions' => ['class' => 'table table-striped'],
            'columns' => [
                // ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    // 'user.username:text:User',
                     [
                     'attribute' => 'event',
                     'value' => 'event.name'
                     ],
                    'created_at:datetime:Booked On',
                  /*  [
                        'header' => "Booked Flights",
                        'format' => 'html',
                        'contentOptions' => ['align' => 'center'],
                        'value' => function ($model) {
                            return ($model->hasBookedFlights())?"<span class='text-success glyphicon glyphicon-ok'></span>":"<span class='text-danger glyphicon glyphicon-remove'></span>";
                        },
                    ],
                    [
                        'header' => "Booked Hotels",
                        'format' => 'html',
                        'contentOptions' => ['align' => 'center'],
                        'value' => function ($model) {
                            return ($model->hasBookedHotels())?"<span class='text-success glyphicon glyphicon-ok'></span>":"<span class='text-danger glyphicon glyphicon-remove'></span>";
                        },
                    ],*/
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => false,
                        'template' => '{view}',
                            'buttons' => [
                            'view' => function($url, $model, $key) {
                                return Html::a('View', ['booking/view', 'id' => $model->id], ['class' => 'btn btn btn-sm btn-block btn-game']);
                            },
                            ],
                    ],
            ],
        ]); ?>
        
	<?php
	endif;
	?>
    </div>
</div>
