<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\widgets\DetailView;
use common\models\Airport;

// If $model->id is set then we know it's a booked hotel. 
// Otherwise, it's saved.
?>


<?php if(isset($model->id)) echo'<a class="booked-hotel-link-wrap" href="' . Url::to(['booked-hotel/view', 'id' => $model->id, ]) . '">'; ?>
		<div class="hotel-list-item row">
			<div class="col-xs-12 col-sm-12">
				<h2><?= $model->hotelChain . " - " . $model->hotelName . " - " . $model->hotelCode ?></h2>
				<p><strong>Checkin:</strong> <?= $model->checkin ?></p>
				<p><strong>Checkout:</strong> <?= $model->checkout ?></p>
				<p><strong>Address:</strong> <?php 
				
				if(isset($model->id)) echo $model->address_line_1 . " " . $model->address_line_2;
				else echo $model->address[0] . " " . $model->address[1];
				?>
				</p>
			</div>
		</div>
<?php if(isset($model->id)) echo '</a>'; ?>