<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\widgets\DetailView;
use common\models\Airport;

$departFlightSegments = array_slice($model->_departFlightSegments,-1);
$lastSegment = array_pop($departFlightSegments);

// flight type or connections
if ($model['depart_num_connections'] == 0) {
	$flightType = "Direct flight";
	$flightTextClass = "text-success";
} elseif($model['depart_num_connections'] == 1) {
	$flightType = "1" . " connecting flight";
	$flightTextClass = "text-warning";
} else {
	$flightType = $model['depart_num_connections'] . " connecting flights";
	$flightTextClass = "text-danger";
}

?>

<a class="booked-flight-link-wrap" href="<?php echo Url::to(['booked-flight/view', 'id' => $model->id, ]) ?>"
>
		<div class="flight-list-item row">

			<div class="col-xs-12 col-sm-2">
				<p class="text text-center"><u>From</u></p>
				<p class="text text-center"><strong><?= $model->fromAirport->name ?></strong></p>
				<p class="text text-center"><u>To</u></p>
				<p class="text text-center"><strong><?= $model->toAirport->name ?></strong></p>

			</div>
			<div class="col-xs-12 col-sm-3">
				<p class="text text-center"><u>Departure</u></p>
				<p class="text text-center"><?php echo Yii::$app->formatter->asDateTime($model['departure_datetime']) ?></p>
				<p class="text text-center"><span class="glyphicon glyphicon-chevron-down"></span></p>
				<p class="text text-center"><?= $model->fromAirport->name ?></p>
			</div>
			<div class="col-xs-12 col-sm-2">
				<p class="text text-center"><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span></p>
				<p class="text text-center <?= $flightTextClass ?>"><?= $flightType ?></p>
			</div>
			<div class="col-xs-12 col-sm-3">
				<p class="text text-center"><u>Arrival</u></p>
				<p class="text text-center"><?php echo Yii::$app->formatter->asDateTime($lastSegment['land']) ?></p>
				<p class="text text-center"><span class="glyphicon glyphicon-chevron-down"></span></p>
				<p class="text text-center"><?= $model->toAirport->name ?></p>
			</div>
			<div class="col-xs-12 col-sm-2">
				<p class="flight-price text text-right text-success"><strong><?= $model['total_fare'] ?></strong></p>
				<?= DetailView::widget([
	                'options' => [
	                    'class' => 'table table-condensed'
	                ],
					'model' => $model,
					'attributes' => [
						// 'status',
					],
					]) ?>
			</div> 
			<div class="col-xs-12 col-sm-6">
				<div class="html-content">
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="html-content">
				</div>
			</div>
		</div>
</a>

<div class="row">
	<div class="col-xs-12 col-md-6 content-div">
		<h3 class="text text-center">Flight segments</h3>
			<?php foreach ($model->_departFlightSegments as $depFliSeg): ?>
				<p class="text text-center"> <strong><?php echo Airport::findByIata($depFliSeg['from'])->name ?></strong> &nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span> &nbsp;&nbsp;&nbsp; <strong><?php echo Airport::findByIata($depFliSeg['to'])->name ?></strong> </p>
				<p class="text text-center"></p>
				<?= DetailView::widget([
	                'options' => [
	                    'class' => 'table table-condensed'
	                ],
					'model' => $depFliSeg,
					'attributes' => [
				        [
				            'label' => 'Flight Number',
				            'value' => $depFliSeg['flight_num'],
				        ],
				        [
				            'label' => 'Takeoff',
				            'format' => 'datetime',
				            'value' => $depFliSeg['takeoff'],
				        ],
				        [
				            'label' => 'Flight Duration',
				            'value' => $depFliSeg['duration'] . " minutes",
				        ],
				        [
				            'label' => 'Land',
				            'format' => 'datetime',
				            'value' => $depFliSeg['land'],
				        ],
						'airline',
					],
					]) ?>
			<?php endforeach ?>
	</div>
	<div class="col-xs-12 col-md-6 content-div">
		<?php if (isset($model->_returnFlightSegments)): ?>
			<h3 class="text text-center">Return flight segments</h3>
				<?php foreach ($model->_returnFlightSegments as $retFliSeg): ?>
					<p class="text text-center"> <strong><?php echo Airport::findByIata($retFliSeg['from'])->name ?></strong> &nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span> &nbsp;&nbsp;&nbsp; <strong><?php echo Airport::findByIata($retFliSeg['to'])->name ?></strong> </p>
					<p class="text text-center"></p>
					<?= DetailView::widget([
		                'options' => [
		                    'class' => 'table table-condensed'
		                ],
						'model' => $retFliSeg,
						'attributes' => [
					        [
					            'label' => 'Flight Number',
					            'value' => $retFliSeg['flight_num'],
					        ],
					        [
					            'label' => 'Takeoff',
					            'format' => 'datetime',
					            'value' => $retFliSeg['takeoff'],
					        ],
					        [
					            'label' => 'Flight Duration',
					            'value' => $retFliSeg['duration'] . " minutes",
					        ],
					        [
					            'label' => 'Land',
					            'format' => 'datetime',
					            'value' => $retFliSeg['land'],
					        ],
							'airline',
						],
						]) ?>
				<?php endforeach ?>
		<?php endif ?>
	</div>
</div>