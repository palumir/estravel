<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use yii\i18n\Formatter;
use yii\db\Query;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use common\models\CreditCardForm;
use common\models\Airport;
use common\adapters\GoogleMapsGeocoding;
use common\models\faregrabbr\HotelDetails;
use common\utilities\StringFunctions;
use dosamigos\yii2gallerywidget;
use frontend\models\Guest;
use common\adapters\ExchangeRate;


/* @var $this yii\web\View */
/* @var $model frontend\models\Booking */

$this->title = $model->event->name;
$this->params['breadcrumbs'][] = ['label' => 'My bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="booking-view row">
    <div class="col-xs-12">
        
        <h1 class="text text-center"><?= "Your travel arrangements for " .  Html::encode($this->title) ?></h1>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                // 'user.email:text:Email',
                // 'event.name:text:Event',
                // 'created_at:datetime:Booked On',
            ],
        ]) ?>


        <h2><?php 
		
		if(isset($flightModel) && $flightModel!=NULL) {
			echo "<h2><img class=\"addTooltip\" title=\"Your flight has been saved, but not yet booked.\" src=\"../images/booking_icons/circleIcon.png\"> Selected Flight</h2>"
			 . '<a class="delete-saved-booking-icon" href="'.Url::to(['booking/delete-saved-booking', 'id' => $model->id, 'type' => 'flight']) . '"><span class="glyphicon glyphicon-trash"></span></a>';
			$color = "orange";
			$showingSavedFlight = true;
		}
		else if(isset($bookedFlightProvider->getModels()[0])) {
			echo "<h2><img class=\"addTooltip\" title=\"Your flight has been booked.\" src=\"../images/booking_icons/checkIcon.png\"> Booked Flight</h2>";
			$color = "green";
		}
		else {
			echo "<h2><img class=\"addTooltip\" title=\"You have yet to save or book a flight.\" src=\"../images/booking_icons/xIcon.png\"> Flight Status</h2>";
			$color = "red";
		}
		
		?></h2>
		<div class="<?= $color ?>">
            <?php if (isset($flightModel) && $flightModel!=NULL): ?>
			<?php

$departFlightSegments = array_slice($flightModel->departureflightsegments,-1);
$lastSegment = array_pop($departFlightSegments);

// flight type or connections
if ($flightModel->depart_num_connections == 0) {
	$flightType = "Direct flight";
	$flightTextClass = "text-success";
} elseif($flightModel->depart_num_connections == 1) {
	$flightType = "1" . " connecting flight";
	$flightTextClass = "text-warning";
} else {
	$flightType = $flightModel->depart_num_connections . " connecting flights";
	$flightTextClass = "text-danger";
}

$firstFlight = $flightModel->departureflightsegments[0];
$lastFlight = $flightModel->departureflightsegments[count($flightModel->departureflightsegments)-1];

?>

		<div class="flight-list-item row">

			<div class="col-xs-12 col-sm-2">
				<p class="text text-center"><u>From</u></p>
				<p class="text text-center"><strong><?= $firstFlight->from ?></strong></p>
				<p class="text text-center"><u>To</u></p>
				<p class="text text-center"><strong><?= $lastFlight->to ?></strong></p>

			</div>
			<div class="col-xs-12 col-sm-3">
				<p class="text text-center"><u>Departure</u></p>
				<p class="text text-center"><?php echo Yii::$app->formatter->asDateTime($firstFlight->takeoff) ?></p>
				<p class="text text-center"><span class="glyphicon glyphicon-map-marker"></span></p>
				<p class="text text-center"><?= $firstFlight->from ?></p>
			</div>
			<div class="col-xs-12 col-sm-2">
				<p class="text text-center"><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span></p>
				<p class="text text-center <?= $flightTextClass ?>"><?= $flightType ?></p>
			</div>
			<div class="col-xs-12 col-sm-3">
				<p class="text text-center"><u>Arrival</u></p>
				<p class="text text-center"><?php echo Yii::$app->formatter->asDateTime($lastFlight->land) ?></p>
				<p class="text text-center"><span class="glyphicon glyphicon-map-marker"></span></p>
				<p class="text text-center"><?= $lastFlight->to ?></p>
			</div>
			<div class="col-xs-12 col-sm-2">
				<p class="flight-price text text-right text-success"><strong>$<?= $flightModel->total_fare ?></strong></p>
				<?= DetailView::widget([
	                'options' => [
	                    'class' => 'table table-condensed'
	                ],
					'model' => $flightModel,
					'attributes' => [
						// 'status',
					],
					]) ?>
			</div> 
			<div class="col-xs-12 col-sm-6">
				<div class="html-content">
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="html-content">
				</div>
			</div>
		</div>
<div class="row">
	<div class="col-xs-12 col-md-6 content-div">
		<h3 class="text text-center">Flight segments</h3>
			<?php foreach ($flightModel->departureflightsegments as $depFliSeg): ?>
			<?php
				$airportNameSplit = explode("(",$depFliSeg->from,2);
				$fromAirport = $depFliSeg->from;
				$airportNameSplit = explode("(",$depFliSeg->to,2);
				$toAirport = $depFliSeg->to;
			?>
				<p class="text text-center"> <strong><?php echo $fromAirport ?></strong> &nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span> &nbsp;&nbsp;&nbsp; <strong><?php echo $toAirport ?></strong> </p>
				<p class="text text-center"></p>
				<?= DetailView::widget([
	                'options' => [
	                    'class' => 'table table-condensed'
	                ],
					'model' => $depFliSeg,
					'attributes' => [
				        [
				            'label' => 'Flight Number',
				            'value' => $depFliSeg->flight_num,
				        ],
				        [
				            'label' => 'Takeoff',
				            'format' => 'datetime',
				            'value' => $depFliSeg->takeoff
				        ],
				        [
				            'label' => 'Flight Duration',
				            'value' => $depFliSeg->duration . " minutes",
				        ],
				        [
				            'label' => 'Land',
				            'format' => 'datetime',
				            'value' => $depFliSeg->land,
				        ],
					],
					]) ?>
			<?php endforeach ?>
	</div>
	<div class="col-xs-12 col-md-6 content-div orange">
		<?php if (isset($flightModel->returnflightsegments)): ?>
			<h3 class="text text-center">Return flight segments</h3>
				<?php foreach ($flightModel->returnflightsegments as $retFliSeg): ?>
					<?php
						$airportNameSplit = explode("(",$retFliSeg->from,2);
						$fromAirport = $retFliSeg->from;
						$airportNameSplit = explode("(",$retFliSeg->to,2);
						$toAirport = $retFliSeg->to;
					?>
					
					<p class="text text-center"> <strong><?php echo $fromAirport ?></strong> &nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span> &nbsp;&nbsp;&nbsp; <strong><?php echo $toAirport ?></strong> </p>
					<p class="text text-center"></p>
					<?= DetailView::widget([
		                'options' => [
		                    'class' => 'table table-condensed'
		                ],
						'model' => $retFliSeg,
						'attributes' => [
					        [
					            'label' => 'Flight Number',
					            'value' => $retFliSeg->flight_num,
					        ],
					        [
					            'label' => 'Takeoff',
					            'format' => 'datetime',
					            'value' => $retFliSeg->takeoff,
					        ],
					        [
					            'label' => 'Flight Duration',
					            'value' => $retFliSeg->duration . " minutes",
					        ],
					        [
					            'label' => 'Land',
					            'format' => 'datetime',
					            'value' => $retFliSeg->land,
					        ],
						],
						]) ?>
				<?php endforeach ?>
		<?php endif ?>
	</div>
</div>
				<?php else:
					echo ListView::widget([
						'summary' => false,
						'emptyText' => 'You have not yet booked a flight for '.$model->event->name.'. <a href="'.Url::to(['flight/search-round-trip', 'event_id' => $model->event_id, 'default' => true]).'"> Would you like to search flights?</a>',
						'dataProvider' => $bookedFlightProvider,
						'itemOptions' => ['class' => 'item center-block'],
						'itemView' => '_flight',
					]);
					
				?>
				<?php
				endif;
				?>
				</div>
				<?php
			
	// Is it a saved or booked hotel?
	// Booked hotel:
	if(!(isset($hotelModel) && $hotelModel!=NULL) && isset($bookedHotelProvider->getModels()[0])) {
		$hotelModel = $bookedHotelProvider->getModels()[0];
		$hotelDataProvider = $bookedHotelProvider;
		$deleteSavedHotel = "";
		$color = "green";
		$hotelTitle = "<h2><img class=\"addTooltip\" title=\"Your hotel has been booked.\" src=\"../images/booking_icons/checkIcon.png\"> Booked Hotel</h2>";
	}
	// Saved hotel:
	else if(isset($hotelModel) && $hotelModel!=NULL) {
		$hotelDataProvider =  new ActiveDataProvider(['query'=>new Query()]);
		$showingSavedHotel = true;
		$hotelDataProvider->setModels(array($hotelModel));
		$deleteSavedHotel = "";
		$color = "orange";
		$hotelTitle = "<h2><img class=\"addTooltip\" title=\"Your hotel has been saved, but not yet booked.\" src=\"../images/booking_icons/circleIcon.png\"> Selected Hotel</h2>"
		. '<a class="delete-saved-booking-icon" href="'.Url::to(['booking/delete-saved-booking', 'id' => $model->id, 'type' => 'hotel']) . '"><span class="glyphicon glyphicon-trash"></span></a>';
	}
	// No hotel saved or booked:
	else {
		$hotelDataProvider =  new ActiveDataProvider(['query'=>new Query()]);
		$hotelDataProvider->setModels(array());
		$deleteSavedHotel = "";
		$color = "red";
		$hotelTitle = "<h2><img class=\"addTooltip\" title=\"You have yet to save or book a hotel.\" src=\"../images/booking_icons/xIcon.png\"> Hotel Status</h2>";
	}
	
	// The title of the hotel section.
	echo $hotelTitle;

	?>
	
	<div class ="<?= $color ?>">
	
	<?php
	// Is it booked?
	echo ListView::widget([
		'summary' => false,
		'emptyText' => 'You have not yet booked a hotel for '.$model->event->name.'. <a href="'.Url::to(['hotel/search', 'event_id' => $model->event_id, 'default' => true]).'">Would you like to search hotels?</a>',
		'dataProvider' => $hotelDataProvider,
		'itemOptions' => ['class' => 'item center-block'],
		'itemView' => '_hotel',
	]);
	
	
	// If the model exists, display hotel details.
	if($hotelModel) { ?>
	
	<div class="hotel-rate text-success">
	<strong>
	<?php 
		$exchangeRate = new ExchangeRate();
		if(isset($rate) && is_object($rate)) print($exchangeRate->convertToUSD($rate->total)); 
	?>
	</strong>
	</div>
	
	<div class="hotel-details content-div">
	<?php
		$event = $model->event;
		$hotelInfo = $hotelModel;
		$hotelInfo = new HotelDetails($model->event_id, $hotelInfo->hotelCode, $hotelInfo->hotelChain, date("Y-m-d", strtotime('today + 330 days')), 1, 1, date("Y-m-d", strtotime('today + 331 days')));
		if(count($hotelInfo->medias) > 0) {
			echo '<div class="col-sm-7 col-xs-12" style="min-width: 300px;">';
			$carousel = array();
			for($i = 0; $i < count($hotelInfo['medias']); $i++) {
				array_push($carousel, 
				['url' => $hotelInfo->medias[$i]->url,
				 'src' => $hotelInfo->medias[$i]->url,
				 'options' => array('title' => StringFunctions::strtotitle($hotelInfo->medias[$i]->caption))
				]);
			}
		
			echo dosamigos\gallery\Carousel::widget(['items' => $carousel]); 
			echo '</div>';
		}
	?>
		<?php
		
		// Google maps integration
		if($hotelInfo->address == "" || $hotelInfo->address == NULL) {
			$geocode = json_decode(GoogleMapsGeocoding::geocode($event->address_line1 . " " . $event->city . " " . $event->zipcode),1);
			$lat = $geocode["results"][0]["geometry"]["location"]["lat"];
			$lng = $geocode["results"][0]["geometry"]["location"]["lng"];
		}
		else {
			if(!isset($hotelInfo->address[1])) {
				$geocode = json_decode(GoogleMapsGeocoding::geocode($hotelInfo->address[0]),1);
			}
			else {
				$geocode = json_decode(GoogleMapsGeocoding::geocode($hotelInfo->address[0] . " " . $hotelInfo->address[1]),1);
			}
			if(!isset($geocode["results"][0])) {
					$geocode = json_decode(GoogleMapsGeocoding::geocode($event->address_line1 . " " . $event->city . " " . $event->zipcode),1);
					$lat = $geocode["results"][0]["geometry"]["location"]["lat"];
					$lng = $geocode["results"][0]["geometry"]["location"]["lng"];
			}
			else {
				$lat = $geocode["results"][0]["geometry"]["location"]["lat"];
				$lng = $geocode["results"][0]["geometry"]["location"]["lng"];
			}
		}
		
		$geocode2 = json_decode(GoogleMapsGeocoding::geocode($event->address_line1 . " " . $event->city . " " . $event->zipcode),1);
		if(isset($geocode2["results"][0])) {
			$lat2 = $geocode2["results"][0]["geometry"]["location"]["lat"];
			$lng2 = $geocode2["results"][0]["geometry"]["location"]["lng"];
		}
		else {
			$lat2 = "";
			$lng2 = "";
		}
		
		?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfVHQuQa9h-HjAkuohObUyGmqE8iVb944&sensor=false&libraries=geometry,places,drawing&ext=.js"></script>
		<div class="col-xs-12 col-sm-5" style="position: relative;">
		<div id="map" style="<?php if(count($hotelInfo->medias) > 0) echo "float:right;"; else echo "float:left;"; ?> width: 100%; height: 0; padding-bottom: 81.5%; z-index: auto; margin: 1em auto; box-shadow: 0 0 10px #000;"></div>
		</div>
		<script>
		  function mapLocation() {
			var directionsDisplay;
			var directionsService = new google.maps.DirectionsService();
			var map;

			function initialize() {
				directionsDisplay = new google.maps.DirectionsRenderer();
				var start = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>);
				var mapOptions = {
					zoom:12,
					center: start
				};
				map = new google.maps.Map(document.getElementById('map'), mapOptions);
				directionsDisplay.setMap(map);
				calcRoute();
			}

			function calcRoute() {
				var start = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>);
				var end = new google.maps.LatLng(<?php echo $lat2; ?>, <?php echo $lng2; ?>);

				var bounds = new google.maps.LatLngBounds();
				bounds.extend(start);
				bounds.extend(end);
				map.fitBounds(bounds);
				var request = {
					origin: start,
					destination: end,
					travelMode: google.maps.TravelMode.DRIVING
				};
				directionsService.route(request, function (response, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						directionsDisplay.setDirections(response);
						directionsDisplay.setMap(map);
					} else {
					}
				});
			}

			google.maps.event.addDomListener(window, 'load', initialize);
		}
		mapLocation();

		</script>
		
	
	<div class="row">
		<div class="col-xs-12">
		<div class="hotel-info row">
		<?php
		if(count($hotelInfo->medias) > 0) {
			echo '<div class="col-xs-12 col-sm-7">';
			echo DetailView::widget([
                'options' => [
                    'class' => 'table table-condensed'
                ],
				'model' => $hotelInfo,
				'attributes' => [
					'phoneNumber',
					'checkinTime',
					'checkoutTime',
				],
				]);
			echo "</div>";
		}
		?>
		<div class="col-xs-12 col-sm-5">
			<p class="text text-center"><strong><?= $hotelInfo->address[0] ?><br><?php if(isset($hotelInfo->address[1])) echo $hotelInfo->address[1]; ?></strong></p>
		</div> 
		</div>
		</div>
		</div>
		</div>
				<?php	}
            ?>
			<?php
			// They're logged in and some hotel/flight is saved.
			if ((isset($showingSavedHotel) || isset($showingSavedFlight)) && !Yii::$app->user->isGuest) {
				Modal::begin([
					'header' => '<h2>Passenger & payment information</h2>',
					'toggleButton' => ['label' => 'Book now', 'class' => 'btn btn-lg btn-success'],
					'size' => Modal::SIZE_LARGE,
				]);
				
					$creditCardForm = new CreditCardForm();
					echo $this->render('_userInputForm', [
							'model' => $model,
							'hotelModel' => $hotelModel,
							'flightModel' => $flightModel,
							'rate' => $rate,
							'guests' => $guests,
							'creditCardForm' => $creditCardForm,
							'event_id' => $event_id,
							'id' => $id
						]);

				Modal::end();
			}
			
			// Make them login.
			else if(($hotelModel != NULL || $flightModel != NULL) && Yii::$app->user->isGuest){
				
				 $form = ActiveForm::begin([
					'id' => 'book-now',
					'class' => 'row',
					'action' => ['site/enter', 'booking_id' => $id]
				]); ?>


				<div class="form-group">
				<?= Html::submitButton("Book Now", ['class' => 'btn btn-block btn-lg btn-success']) ?>
				</div>

				<?php ActiveForm::end(); 
			}
			?>
	</div>
    </div>
	</div>
	<script>
	$(".addTooltip").tooltip();
	</script>
