<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\CreditCardForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $creditCardForm common\models\CreditCardForm */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="userInputForm-form">

    <?php
		
		// Forces $guests to contain passengers or guests (if one exists)
		$form = ActiveForm::begin([
        'id' => 'booking-form',
        'class' => 'row',
        'action' => ['book', 'event_id' => $event_id, 'id' => $id],
    ]); ?>

    <div class="col-xs-12">
        <?php
		  if(isset($guests) && $guests != NULL) {
          foreach ($guests as $index => $guest) { ?>
                
			<div class="guest-details row">

				<h3><?php
					if ($index==0) {
						echo "Your guest details";
					} else {
						$i = $index+1;
						echo "Guest $i details";
					}
				?></h3>

				<div class="col-xs-12">
					<div class="row"> 
					<?= $form->field($guest, "[".$index."]first_name", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->textInput() ?>
					<?= $form->field($guest, "[".$index."]middle_name", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->textInput() ?>
					<?= $form->field($guest, "[".$index."]last_name", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->textInput() ?>
					<?= $form->field($guest, "[".$index."]type", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->textInput(['disabled' => 'disabled']) ?>
					<?= $form->field($guest, "[".$index."]type", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->hiddenInput()->label(false)->error(false); ?>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="row">
						<?= $form->field($guest, "[".$index."]date_of_birth", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->widget(DatePicker::className(), []) ?>
						<?= $form->field($guest, "[".$index."]gender", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-2']])->dropDownList(['' => '', 'M' => 'M', 'F' => 'F']) ?>
						<?= $form->field($guest, "[".$index."]number", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->textInput(['maxlength' => true]) ?>
						<?= $form->field($guest, "[".$index."]email", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-4']])->textInput(['maxlength' => true]) ?> 
					</div>
				</div>
			</div>
			<hr>
		<?php
		   }
		  }
        ?>
    </div>

    <h2>Credit Card:</h2>
    <p class="card-security-warning"><small>Please note that your card data is not stored on our server.</small></p>
    <div class="col-xs-12">
        <div class="row">
            <?= $form->field($creditCardForm, 'number', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-5']])->textInput(['maxlength' => true]) ?>
            <?= $form->field($creditCardForm, 'code', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->textInput(['maxlength' => true]) ?>
            <?= $form->field($creditCardForm, 'month', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-2']])->dropDownList(Yii::$app->params['monthRange']) ?>
            <?= $form->field($creditCardForm, 'year', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-2']])->dropDownList(Yii::$app->params['yearRange']) ?>
        </div>
        <div class="row">
            <?= $form->field($creditCardForm, 'name', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-6']])->textInput(['maxlength' => true]) ?>

            <?= $form->field($creditCardForm, 'addressLine1', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-6']])->textInput(['maxlength' => true]) ?>
        </div>
        <div class="row">
            <?= $form->field($creditCardForm, 'city', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-4']])->textInput(['maxlength' => true]) ?>
            <?= $form->field($creditCardForm, 'state', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-4']])->textInput(['maxlength' => true]) ?>
            <?= $form->field($creditCardForm, 'zip', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-4']])->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton("Book", ['class' => 'btn btn-block btn-lg btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
