<?php
use yii\helpers\Url;

?>

<a class="title-link" href="<?= Url::to(['article/view', 'id' => $model->id]); ?>"><h2><?= $model->title ?></h2></a> 
<div class="article-body">
	<?= $model->body ?>
</div>
<div class="tags">
	<p class="tags"><small><?= $model->tags ?></small></p>
</div>
<div class="article-info">	
	<p>Comments: <strong><?= $model->countArticleComments() ?> </strong></p>
</div>