<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">
    <div class="article">    
        <h1><?= Html::encode($this->title) ?></h1>

        <?= $model->body ?>
        
    <p>Author: <strong><?= $model->admin->fullName() ?> </strong></p>
    <p>Published: <strong><?php echo Yii::$app->formatter->asDatetime($model->created_at) ?> </strong></p>
    </div>
</div>
            <?php $form = ActiveForm::begin([
                'action' => ['article-comment/post', 'article_id' => $model->id],
                'options' => ['class' => 'article-comment-form'],
            ]); ?>

            <?= $form->field($comment, 'body')->textarea(['maxlength' => 1000])->label(false)->error(false) ?>
            <?= $form->field($comment, 'article_id')->hiddenInput(['value' => $model->id])->label(false)->error(false); ?>

            <div class="form-group">
                <?= Html::submitButton('Post Comment', ['class' => 'btn btn-star btn-block']) ?>
            </div>

            <?php ActiveForm::end(); ?>

                                <?php Pjax::begin(['id' => 'pjax-comments']); ?>

                                        <?=
                                        ListView::widget([
                                            'summary' => false,
                                            'dataProvider' => $commentDataProvider,
                                            'itemOptions' => ['class' => 'comment'],
                                            'itemView' => '_comment',
                                        ]);
                                        ?>
                                <?php Pjax::end(); ?>