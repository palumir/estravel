<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">
    <div class="row">
        <div class="col-xs-12">
            <?php
            echo ListView::widget([
                'summary' => '',
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item article center-block'],
                'itemView' => '_article',
                'separator' => "<hr>"
                // 'viewParams' => [
                //         ],
            ]);
            ?>
        </div>
    </div>
</div>
