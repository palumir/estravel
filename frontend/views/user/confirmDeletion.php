<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ConfirmDeletion */

$this->title = 'Confirm account deletion';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-request-password-reset">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please confirm your action to <strong>delete</strong> your account by typing in your e-mail.</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'confirm-deletion-form']); ?>
                <?= $form->field($model, 'email') ?>
                <div class="form-group">
                    <?= Html::submitButton('Confirm', ['class' => 'btn btn-danger']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
