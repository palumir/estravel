<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\User */

$this->title = 'Update your account details';
$this->params['breadcrumbs'][] = ['label' => (Yii::$app->user->identity->firstname && Yii::$app->user->identity->lastname)?Yii::$app->user->identity->fullName():Yii::$app->user->identity->username, 'url' => ['account']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="account-update content-div">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
