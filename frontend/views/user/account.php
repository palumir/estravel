<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\i18n\Formatter;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = (Yii::$app->user->identity->firstname && Yii::$app->user->identity->lastname)?Yii::$app->user->identity->fullName():Yii::$app->user->identity->username;
$this->params['breadcrumbs'][] = $this->title;



?>
<div class="user-account">

    <div class="row">
        <div class="content-div" id="account-menu">
            <p><?= Html::a('About You', "#about-you", ['class' => 'btn btn-primary btn-block']) ?></p>
          <!----  <p><?= Html::a('Payment Methods', "#credit-cards", ['class' => 'btn btn-primary btn-block']) ?></p>---->
            <p><?= Html::a('Currency', "#currency", ['class' => 'btn btn-primary btn-block']) ?></p>
            <p><?= Html::a('Password', "#password", ['class' => 'btn btn-primary btn-block']) ?></p>
           <!----<p><?= Html::a('Social networks', "#social-networks", ['class' => 'btn btn-primary btn-block']) ?></p>---->
           <!---- <p><?= Html::a('Newsletter', "#newsletter", ['class' => 'btn btn-primary btn-block']) ?></p>--->
            <hr>
           <!---- <p><?= Html::a('My Trips', ['user/trips'], ['class' => 'btn btn-success btn-block disabled']) ?></p>--->
           <!---- <p><?= Html::a('Support Tickets', ['ticket/index'], ['class' => 'btn btn-success btn-block']) ?></p>--->
            <hr>
            <p><?= Html::a('Logout', ['site/logout'], ['class' => 'btn btn-danger btn-block', 'data-method' => 'post']) ?></p>
        </div>
        <div class="col-xs-10 col-xs-offset-2">
            <div class="content-div">
                <h2 id="about-you">You on eSports Travel</h2>
                <p>These details will auto-fill in forms and searches across the site. They are also displayed next to your publicly shared reviews, ratings, and comments. </p>
                <p class="text text-right"><?= Html::a('Update', ['update'], ['class' => 'btn btn-primary']) ?></p>

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'username',
                        'email',
                        'firstname',
                        'middlename',
                        'lastname',
                        'phone',
                        'gender',
                        [                      // the owner name of the model
                            'label' => 'Country',
                            'value' => ($model->country)?$model->country->name . " (" . $model->country->code . ")":NULL,
                        ],
                        'date_of_birth:date:Date of birth',
                        [                      // the owner name of the model
                            'label' => 'Default Airport',
                            'value' => ($model->airport)?$model->airport->name . ", " . $model->airport->city . ", " . $model->airport->country . " - " . $model->airport->iata_code:NULL,
                        ],
                        'created_at:date:Joined on',
                    ],
                ]) ?>
            </div>
           <!----  <hr>
            <div class="content-div">
                <h2 id="credit-cards">Payment Methods</h2>
                <p>We do not currently store any of your payment details on our server.</p>
                <p class="text text-right"><?= Html::a('Add a card', ['add-card'], ['class' => 'btn btn-primary disabled']) ?></p>
            </div> ---->
            <hr>
            <div class="content-div">
                <h2 id="currency">Currency</h2>
                <p>At the moment the only supported currency is USD.</p>
            </div>
            <hr>
            <div class="content-div">
                <h2 id="password">Password</h2>
                <p>Would you like to change your password? Click the button below and we'll send you an email with a link to reset it.</p>
                <p><?= Html::a('Change password', ['user/request-password-reset'], ['class' => 'btn btn-primary']) ?></p>
            </div>
            <!----<hr>
            <div class="content-div">
                <h2 id="social-networks">Social networks</h2>
                <p>Coming soon</p>
            </div>
            <hr>
            <div class="content-div">
                <h2 id="newsletter">Newsletter preferences</h2>
                <p>Don't miss out on the best eSPORTS travel deals that we have to offer! How often you'd like to receive emails containing deals, events and recommendations?</p>
            </div>
            <hr>---->
			<br>
            <p class="text text-center">
                <small><?= Html::a('Click here', ['user/delete', 'id' => Yii::$app->user->id], ['class' => '', 'data-method' => 'post', 'data-confirm' => 'Are you sure you want to delete your account?']) ?> to delete your Esports Travel account.</small>
            </p>
        </div>
    </div>
</div>
