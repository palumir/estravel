<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;
use common\models\Airport;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

$airport_url = \yii\helpers\Url::to(['airport/ajax-list']);
$country_url = \yii\helpers\Url::to(['country/ajax-list']);
if ($model->airport) {
	$airport = $model->airport->name . ", " . $model->airport->city . ", " . $model->airport->country . " - " . $model->airport->iata_code;
} else {
	$airport = "";
}

?>

<div class="user-form">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				
		    <?php $form = ActiveForm::begin(['class' => 'row']); ?>

		    <?= $form->field($model, 'firstname', ['options' => ['class' => 'col-sm-4']])->textInput(['maxlength' => true]) ?>
		    <?= $form->field($model, 'middlename', ['options' => ['class' => 'col-sm-4']])->textInput(['maxlength' => true]) ?>
		    <?= $form->field($model, 'lastname', ['options' => ['class' => 'col-sm-4']])->textInput(['maxlength' => true]) ?>
		    <?= $form->field($model, 'date_of_birth', ['options' => ['class' => 'col-sm-4']])->widget(DatePicker::className(),[
			        // inline too, not bad
			         // 'inline' => true,
			         // modify template for custom rendering
			        // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
			        'clientOptions' => [
			            // 'autoclose' => true,
			            'format' => 'yyyy-mm-dd'
			        ]
		    ])->label('Date of Birth (yyyy-mm-dd)') ?>
		    <?= $form->field($model, 'gender', ['options' => ['class' => 'col-sm-3']])->dropDownList(['M' => 'M', 'F' => 'F']) ?>
		    <?= $form->field($model, 'country_id', ['options' => ['class' => 'col-sm-5']])->widget(Select2::classname(), [
				    'pluginOptions' => [
				        'allowClear' => true,
				        'minimumInputLength' => 3,
				        'language' => [
				            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
				        ],
				        'ajax' => [
				            'url' => $country_url,
				            'dataType' => 'json',
				            'data' => new JsExpression('function(params) { return {q:params.term}; }')
				        ],
				        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
				        'templateResult' => new JsExpression('function(country) { return country.name; }'),
				        'templateSelection' => new JsExpression('function (country) { return country.name; }'),
				    ],
				]); 

			?>
		    <?= $form->field($model, 'phone', ['options' => ['class' => 'col-sm-4']])->textInput(['maxlength' => true]) ?>
		    <?= $form->field($model, 'airport_id', ['options' => ['class' => 'col-sm-8']])->widget(Select2::classname(), [
				    'initValueText' => $airport, // set the initial display name
				    // 'theme' => Select2::THEME_DEFAULT,
				    // 'options' => ['placeholder' => 'Your preferred airport ...'],
				    'pluginOptions' => [
				        'allowClear' => true,
				        'minimumInputLength' => 3,
				        'language' => [
				            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
				        ],
				        'ajax' => [
				            'url' => $airport_url,
				            'dataType' => 'json',
				            'data' => new JsExpression('function(params) { return {q:params.term}; }')
				        ],
				        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
				        'templateResult' => new JsExpression('function(airport) { return airport.name; }'),
				        'templateSelection' => new JsExpression('function (airport) { return airport.name; }'),
				    ],
				]); 

			?>


		    <div class="form-group">
		    	<p class="text text-right">
			        <?= Html::a('Cancel', ['user/account'], ['class' => 'btn btn-sm btn-danger']) ?>	
			        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>	
		    	</p>
		    </div>

		    <?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
