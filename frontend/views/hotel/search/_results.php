<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

function str_lreplace($search, $replace, $subject)
{
    $pos = strrpos($subject, $search);

    if($pos !== false)
    {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }

    return $subject;
}

?>

<div class="row">
  
  <?php
	// Echo the results.
    echo ListView::widget([
        'summary' => false,
        'dataProvider' => $hotelDataProvider,
        'itemOptions' => ['class' => 'col-xs-12 item center-block'],
        'itemView' => '__hotel',
        'viewParams' => [
        	'event_id' => $event_id,
        	'hotelSearchForm' => $hotelSearchForm,
			'hotelDataProvider' => $hotelDataProvider,
			'hotelSearchModel' => $hotelSearchModel,
			'next_page' => $next_page,
        	],
      ]);
	  
  ?>
</div>




