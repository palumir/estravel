<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

$place_url = \yii\helpers\Url::to(['place/ajax-list']);

/* @var $this yii\web\View */
/* @var $hotelSearchForm common\models\Book */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Hotels';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="hotels row">
    <div class="search-form-wrap col-xs-12 col-sm-12">
        <?php 
            $form = ActiveForm::begin([
                'options' => ['class' => 'ajax-search-form row', /*'target' => '_blank'*/],
                'method' => 'get',
                'action' => ['hotel/search', 'event_id' => $event_id],
            ]); 
        ?>
		<div class="col-sm-6 col-xs-12 field-hotel-search-form-location">
		<label class="control-label" for = "event_id">Location</label>
		<input list="location" class="form-control" id="hotel-search-form-location" name="HotelSearchForm[location]" value="<?= $hotelSearchForm->location ?>">
		<datalist id="location">
		
		 <?php
			$event_address = $event->address_line1 . ", " . $event->address_line2 . ", " . $event->city . ", " . $event->zipcode;
			echo '<option name="location" value="' . $event_address  . '">' . $event_address  . '</option>';
 
		 ?>
		</datalist>
		</div>
	    <?php echo $form->field($hotelSearchForm, 'numrooms', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-6']])->dropDownList([1=>1, 2=>2, 3=>3, 4=>4, 5=>5]) ?>
        <?php echo $form->field($hotelSearchForm, 'checkin', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-6']])->widget(DatePicker::className()) ?>
        <?php echo $form->field($hotelSearchForm, 'checkout', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-6']])->widget(DatePicker::className()) ?>
        <input type="hidden" value="1" name="HotelSearchForm[guests]" />

        <div class="form-group">
            <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary btn-block']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        
    </div>

    <div class="search-results col-xs-12 col-sm-12">
      <?php
		echo $this->render('_results', [
				'event_id' => $event_id,
				'hotelSearchForm' => $hotelSearchForm,
				'hotelSearchModel' => $hotelSearchModel,
				'hotelDataProvider' => $hotelDataProvider,
				'next_page' => $next_page
			]);
      ?>
    </div>


</div>
<div class="spinner">
    
</div>
