<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\adapters\ExchangeRate;
use common\utilities\StringFunctions;
use yii\widgets\ActiveForm;

?>

	<div class="row">	
	<?php
		// Currencycode not returned in rate object so we need to pass it to preload.
		$currencyCode = str_replace(preg_replace("/([^0-9\\.])/i", "", $model['minimumRate']), "", $model['minimumRate']);
	?>
	<a class="hotel-link-wrap" target="_blank" href="<?= Url::to(['hotel/preload', 'event_id' => $event_id, 'hotelCode' => $model['hotelCode'], 'hotelChain' => $model['hotelChain'], 'checkin' => $hotelSearchForm->checkin, 'numrooms' => $hotelSearchForm->numrooms, 'guests' => $hotelSearchForm->guests, 'checkout' => $hotelSearchForm->checkout, 'currencyCode' => $currencyCode]) ?>">
		<div class="hotel-list-item col-sm-12">
			<div class="row">
				<div class="col-xs-6">
					<h2><?= StringFunctions::strtotitle($model['name']) ?></h2>
				</div>
				<?php 
				$exchangeRate = new ExchangeRate();
				
				// Get total minimum
				$date1 = new DateTime($hotelSearchForm->checkout);
				$date2 = new DateTime($hotelSearchForm->checkin);
				$diff = $date1->diff($date2);
				$var = $model['minimumRate'];
				$minRate = (float)preg_replace("/([^0-9\\.])/i", "", $var);
				
				// Get amount.
				$totalMinimumNum = $diff->days*$minRate;
				
				
				// Get currency code.
				$var = $model['minimumRate'];
				$totalMinimum = str_replace($minRate, $totalMinimumNum, $var);
				if(strpos($totalMinimum,".") !== false) $totalMinimum = rtrim(rtrim($totalMinimum,"0"),".");
				$totalMinimum = $exchangeRate->convertToUSD($totalMinimum);
				
				// Convert other values to USD.
				$model['minimumRate'] = $exchangeRate->convertToUSD($model['minimumRate']);
				$model['maximumRate'] = $exchangeRate->convertToUSD($model['maximumRate']);
				
				?>
				
				<div class="col-xs-6">
				<?php
				
				// Sort out distance to venue.
				if(isset($model["distanceFromVenue"]))
				$distanceFromVenue = [
								'label' => 'Distance From Venue',
								'value' => $model["distanceFromVenue"]
							];
				else $distanceFromVenue = [
								'label' => 'Distance From Venue',
								'value' => "N/A"
							];
							
				// Sort out amenities.
				$supportedAmenities = [
							   [
								  "description"=>"free internet",
								  "code"=>"HWFR"
							   ],
							   [
								  "description"=>"free parking",
								  "code"=>"FPRK"
							   ],
							   [
								  "description"=>"breakfast",
								  "code"=>"BRFT"
							   ],
							   [
								  "description"=>"small pets allowed",
								  "code"=>"SPAL"
							   ],
							   [
								  "description"=>"swimming pool",
								  "code"=>"POOL"
							   ],
							   [
								  "description"=>"free airport shuttle",
								  "code"=>"SHFR"
							   ],
							   [
								  "description"=>"no smoking rooms",
								  "code"=>"NSMR"
							   ],
							   [
								  "description"=>"fitness center",
								  "code"=>"HESP"
							   ],
							   [
								  "description"=>"handicap facilities",
								  "code"=>"HAFA"
							   ],
							   [
								  "description"=>"business center",
								  "code"=>"COBU"
							   ],
							   [
								  "description"=>"safe in room",
								  "code"=>"SAIR"
							   ],
							   [
								  "description"=>"kitchen",
								  "code"=>"KTCN"
							   ]
							];
					
					$amenities = "";
					
					// Get descriptions.
					foreach($model["amenities"] as $amenity) {
						foreach($supportedAmenities as $possibleAmenity) {
							if($amenity == $possibleAmenity["code"]) {
								$amenities .= $possibleAmenity['description'] . ", ";
							}
						}
					}
					
					// Make it look pretty.
					$amenities = [
								'label' => "Amenities",
								'value' => str_lreplace(",", ", and ", ucfirst(rtrim(rtrim($amenities, ' '), ',')))
							];

				?>
					<?= DetailView::widget([
		                'options' => [
		                    'class' => 'table table-condensed'
		                ],
						'model' => $model,
						'attributes' => [
							/*[                      // the owner name of the model
								'label' => 'Address',
								'value' => StringFunctions::strtotitle($model['briefAddress']) . ", " . $model['location'],
							],*/
							[
								'label' => 'Price',
								'value' => $model['minimumRate'] . " per night",
							],
							[                      // total minimum
								'label' => 'Total',
								'value' => $totalMinimum .  " for entire stay",
							],
							$distanceFromVenue,
							$amenities
						],
						]) ?>
				</div>
			</div>
		</div>
</a>
	</div>
	
<?php
// Add a "load more hotels" button if we are at the end of the list.
if($hotelDataProvider->allModels[count($hotelDataProvider->allModels) - 1]['hotelCode'] == $model['hotelCode']): ?>

<div class="row">
	<?php
	if(!isset(Yii::$app->request->get()['page'])) $page = "";
	else $page = Yii::$app->request->get()['page'];
	$form = ActiveForm::begin([
		'options' => ['class' => 'ajax-load-more-hotels row', /*'target' => '_blank'*/],
		'method' => 'get',
		'action' => ['hotel/search', 'event_id' => $event_id, 'load_page' => $next_page, 'page' => $page],
	]); 
	?>
	<?= $form->field($hotelSearchForm, 'location', [])->hiddenInput()->label(false); ?>
	<?php echo $form->field($hotelSearchForm, 'locationType', [])->hiddenInput()->label(false) ?>
	<?php echo $form->field($hotelSearchForm, 'checkin', [])->hiddenInput()->label(false) ?>
	<?php echo $form->field($hotelSearchForm, 'checkout', [])->hiddenInput()->label(false) ?>
	<?php echo $form->field($hotelSearchForm, 'numrooms', [])->hiddenInput()->label(false) ?>
	<?php echo $form->field($hotelSearchForm, 'guests', [])->hiddenInput()->label(false) ?>
	<div class="form-group">
        <?php echo Html::submitButton('Load More Hotels', ['class' => 'btn btn-block btn-travel']) ?>
    </div>
	<?php ActiveForm::end(); ?>
</div>


<?php endif; ?>

