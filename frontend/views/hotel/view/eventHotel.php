<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\bootstrap\Carousel;
use common\adapters\GoogleMapsGeocoding;
use common\adapters\ExchangeRate;
use dosamigos\yii2gallerywidget;
use common\utilities\StringFunctions;

$this->title = StringFunctions::strtotitle($model->hotelName . " at " . $event->name);


?>

<div class="hotel-details col-xs-12 content-div">
    <h1 class="text text-center"><?= Html::encode($this->title) ?></h1>
	<?php
	
		echo '<div class="col-cs-12 col-sm-7" style="min-width: 300px;">';
		$carousel = array();
		for($i = 0; $i < count($model->medias); $i++) {
			array_push($carousel, 
			['url' => $model->medias[$i]->url,
			 'src' => $model->medias[$i]->url,
			 'options' => array('title' => StringFunctions::strtotitle($model->medias[$i]->caption))
			]);
		}
	
		echo dosamigos\gallery\Carousel::widget(['items' => $carousel]); 
		echo '</div>';
	?>
	
		
		<?php 
		// Google maps integration
		if($model->address == "" || $model->address == NULL) {
			$geocode = json_decode(GoogleMapsGeocoding::geocode($event->address_line1 . " " . $event->city . " " . $event->zipcode),1);
			if(!isset($geocode["results"][0])) {
				$lat = 0;
				$lng = 0;
			}
			else {
				$lat = $geocode["results"][0]["geometry"]["location"]["lat"];
				$lng = $geocode["results"][0]["geometry"]["location"]["lng"];
			}
		}
		else {
			$geocode = json_decode(GoogleMapsGeocoding::geocode($model->address),1);
			if(!isset($geocode["results"][0])) {
					$geocode = json_decode(GoogleMapsGeocoding::geocode($event->address_line1 . " " . $event->city . " " . $event->zipcode),1);
					if(isset($geocode["results"][0])) {
						$lat = $geocode["results"][0]["geometry"]["location"]["lat"];
						$lng = $geocode["results"][0]["geometry"]["location"]["lng"];
					}
					else {
						$lat = 0;
						$lng = 0;
					}
			}
			else {
				$lat = $geocode["results"][0]["geometry"]["location"]["lat"];
				$lng = $geocode["results"][0]["geometry"]["location"]["lng"];
			}
		}
		
		$geocode2 = json_decode(GoogleMapsGeocoding::geocode($event->address_line1 . " " . $event->city . " " . $event->zipcode),1);
		if(isset($geocode2["results"][0])) {
			$lat2 = $geocode2["results"][0]["geometry"]["location"]["lat"];
			$lng2 = $geocode2["results"][0]["geometry"]["location"]["lng"];
		}
		else {
			$lat2 = "";
			$lng2 = "";
		}
		
		?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfVHQuQa9h-HjAkuohObUyGmqE8iVb944&sensor=false&libraries=geometry,places,drawing&ext=.js"></script>
		<div class="col-sm-5 col-xs-12" style="position: relative;">
		<div id="map" style="float:right; width: 100%; height: 0; padding-bottom: 81.5%; z-index: auto; margin: 1em auto; box-shadow: 0 0 10px #000;"></div>
		</div>
		<script>
		  function mapLocation() {
			var directionsDisplay;
			var directionsService = new google.maps.DirectionsService();
			var map;

			function initialize() {
				directionsDisplay = new google.maps.DirectionsRenderer();
				var start = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>);
				var mapOptions = {
					zoom:12,
					center: start
				};
				map = new google.maps.Map(document.getElementById('map'), mapOptions);
				directionsDisplay.setMap(map);
				calcRoute();
			}

			function calcRoute() {
				var start = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>);
				var end = new google.maps.LatLng(<?php echo $lat2; ?>, <?php echo $lng2; ?>);

				var bounds = new google.maps.LatLngBounds();
				bounds.extend(start);
				bounds.extend(end);
				map.fitBounds(bounds);
				var request = {
					origin: start,
					destination: end,
					travelMode: google.maps.TravelMode.DRIVING
				};
				directionsService.route(request, function (response, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						directionsDisplay.setDirections(response);
						directionsDisplay.setMap(map);
					} else {
						alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
					}
				});
			}

			google.maps.event.addDomListener(window, 'load', initialize);
		}
		mapLocation();

		</script>
		
	
	<div class="row">
		<div class="col-xs-12">
		<div class="hotel-info row">
		<div class="col-xs-7" style="padding-left:30px;">
			<?= DetailView::widget([
                'options' => [
                    'class' => 'table table-condensed'
                ],
				'model' => $model,
				'attributes' => [
					'phoneNumber',
					'checkinTime',
					'checkoutTime',
				],
				]) ?>
		</div> 
		<div class="col-xs-5">
			<p class="text text-center"><strong><?= $model->address[0] ?><br><?= $model->address[1] ?></strong></p>
		</div> 
	
			</div>
    
    <?php foreach ($model->rates as $rate): ?>
		<div class="hotel-rate-item col-sm-12">
			<div class="row">
				<div class="col-xs-9">
					<h2><?= StringFunctions::strtotitle($rate['description']) ?></h2>
				</div>
				<div class="col-xs-3">
				</div>
			</div>
			<div class="rate-info row">
				<div class="col-xs-12 col-sm-9">
					<div class="html-content">
						<?php
						
							// Convert currency for display to USD if it's not USD.
							if($model->currencyCode!="$") {
								$exchangeRate = new ExchangeRate();
								if(isset($rate->taxes_and_surcharges) && $rate->taxes_and_surcharges != 0) $rate->taxes_and_surcharges = $exchangeRate->convertToUSD($model->currencyCode . $rate->taxes_and_surcharges);
								if(isset($rate->total)) $rate->total = $exchangeRate->convertToUSD($model->currencyCode . $rate->total);
								if(isset($rate->base)) $rate->base = $exchangeRate->convertToUSD($model->currencyCode . $rate->base);
								if(isset($rate->cancel_penality) && $rate->cancel_penality != "") $rate->cancel_penality = $exchangeRate->convertToUSD($model->currencyCode . $rate->cancel_penality);
								if(isset($rate->deposit_amount)) $rate->deposit_amount = $exchangeRate->convertToUSD($model->currencyCode . $rate->deposit_amount);
							}
						?>
						<?= DetailView::widget([
			                'options' => [
			                    'class' => 'table table-condensed'
			                ],
							'model' => $rate,
							'attributes' => [
								//'plan',
								'category',
								'total',
								'base',
								'taxes_and_surcharges',
								// 'rate_includes',
								'cancel_deadline',
								'cancel_penality',
								'isSmoking',
								'deposit_amount',
								//'guarantee_type',
							],
							'template' => function($attribute, $index, $widget){
								if($attribute['value'])
								{
									return "<tr><th>{$attribute['label']}</th><td>{$attribute['value']}</td></tr>";
								}
							},
							]) ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3">
				    <p>
						<?php 
			
						?>
						
						<?php $form = ActiveForm::begin([
							'id' => 'save-form',
							'class' => 'row',
							'action' => ['booking/view', 'hK' => $hK, 'rate' => json_encode($rate), 'guests' => json_encode($guests), 'event_id' => $event->id]
						]); ?>


						<div class="form-group">
						<?= Html::submitButton("Select Hotel", ['class' => 'btn btn-block btn-lg btn-success']) ?>
						</div>

						<?php ActiveForm::end(); ?>
				    </p>
				</div> 
			</div>
		</div>
    <?php endforeach ?>
		</div>
		<div class="col-xs-3">
		</div>
	</div>
</div>
