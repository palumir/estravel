<?php foreach ($medias as $media): ?>
	<figure>
	  <img src="<?= $media->url ?>" alt="<?= $media->caption ?>">
	  <figcaption><?= $media->caption ?></figcaption>
	</figure>
<?php endforeach ?>