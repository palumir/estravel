<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use common\models\faregrabbr\HotelDetails;


$this->title = $model->hotelName;
?>

<div class="hotel-details col-xs-12">
    <h1><?= Html::encode($this->title) ?></h1>
    <p class="text text-right"><?= Html::a('Cancel', ['hotel/search'], [
        'class' => 'btn btn-danger',
    ]) ?></p>
    <h2><?= $model->hotelName ?></h2>

	<div class="hotel-info row">
		<div class="col-xs-9">
			<div class="html-content">
				<?php echo $model->description; ?>
			</div>
		</div>
		<div class="col-xs-3">
			<?= DetailView::widget([
                'options' => [
                    'class' => 'table table-condensed'
                ],
				'model' => $model,
				'attributes' => [
					'phoneNumber',
					'checkinTime',
					'checkoutTime',
				],
				]) ?>
			<p class="text text-center"><strong><?= $model->address[0] ?></strong></p>
			<p class="text text-center"><strong><?= $model->address[1] ?></strong></p>

	        <?php
	            Modal::begin([
	                // 'header' => '<h2>Fa</h2>',
	                'toggleButton' => ['label' => 'View facilities', 'class' => 'btn btn-lg btn-block btn-info'],
	                'size' => Modal::SIZE_LARGE,
	            ]);

	                echo $this->render('_medias', [
	                        'medias' => $model->medias,
	                    ]);

	            Modal::end();
	        ?>
		</div> 
	</div>
	<div class="row">
		<div class="col-xs-9">
    
    <?php foreach ($model->rates as $rate): ?>
		<div class="hotel-rate-item col-sm-12">
			<div class="row">
				<div class="col-xs-9">
					<h2><?= $rate['description'] ?></h2>
					<?php 
					print("<pre>");
					print_r($rate); 
					print("</pre>");
					?>
				</div>
				<div class="col-xs-3">
				</div>
			</div>
			<div class="rate-info row">
				<div class="col-xs-9">
					<div class="html-content">
						<?= DetailView::widget([
			                'options' => [
			                    'class' => 'table table-condensed'
			                ],
							'model' => $rate,
							'attributes' => [
								'plan',
								'category',
								'total',
								'base',
								'taxes_and_surcharges',
								// 'rate_includes',
								'cancel_deadline',
								'cancel_penality',
								'isSmoking',
								'deposit_amount',
								'guarantee_type',
							],
							'template' => function($attribute, $index, $widget){
								if($attribute['value'])
								{
									return "<tr><th>{$attribute['label']}</th><td>{$attribute['value']}</td></tr>";
								}
							},
							]) ?>
					</div>
				</div>
				<div class="col-xs-3">
				    <p>
				        <?php // echo Html::a('Book this Flight', ['book', 'hK' => $model->cacheKey], ['class' => 'btn btn-primary']) ?>
				        <?php
				        	if (!Yii::$app->user->isGuest) {
					            Modal::begin([
					                'header' => '<h2>Passenger & payment information</h2>',
					                'toggleButton' => ['label' => 'Book now', 'class' => 'btn btn-lg btn-success'],
					                'size' => Modal::SIZE_LARGE,
					            ]);

					                echo $this->render('_userInputForm', [
					                        'hK' => $hK,
					                        'rate' => $rate,
					                        'guests' => $guests,
					                        'creditCardForm' => $creditCardForm,
					                    ]);

					            Modal::end();
				        	}
				        ?>
				    </p>
				</div> 
			</div>
		</div>
    <?php endforeach ?>
		</div>
		<div class="col-xs-3">
		</div>
	</div>
</div>
