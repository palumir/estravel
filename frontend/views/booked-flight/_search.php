<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\BookedFlightSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="booked-flight-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'order_number') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'status_text') ?>

    <?php // echo $form->field($model, 'to') ?>

    <?php // echo $form->field($model, 'from') ?>

    <?php // echo $form->field($model, 'total_fare') ?>

    <?php // echo $form->field($model, 'base_fare') ?>

    <?php // echo $form->field($model, 'taxes') ?>

    <?php // echo $form->field($model, 'departure_datetime') ?>

    <?php // echo $form->field($model, 'return_datetime') ?>

    <?php // echo $form->field($model, 'depart_num_connections') ?>

    <?php // echo $form->field($model, 'return_num_connections') ?>

    <?php // echo $form->field($model, 'depart_flight_segments') ?>

    <?php // echo $form->field($model, 'return_flight_segments') ?>

    <?php // echo $form->field($model, 'record_locators') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'deleted') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
