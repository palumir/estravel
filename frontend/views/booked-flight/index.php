<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\BookedFlightSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Booked Flights';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booked-flight-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Booked Flight', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'order_number',
            'status',
            'status_text',
            // 'to',
            // 'from',
            // 'total_fare',
            // 'base_fare',
            // 'taxes',
            // 'departure_datetime',
            // 'return_datetime',
            // 'depart_num_connections',
            // 'return_num_connections',
            // 'depart_flight_segments:ntext',
            // 'return_flight_segments:ntext',
            // 'record_locators:ntext',
            // 'created_at',
            // 'updated_at',
            // 'deleted',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
