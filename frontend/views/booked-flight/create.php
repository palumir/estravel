<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\BookedFlight */

$this->title = 'Create Booked Flight';
$this->params['breadcrumbs'][] = ['label' => 'Booked Flights', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booked-flight-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
