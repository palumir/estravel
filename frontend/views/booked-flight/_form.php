<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\BookedFlight */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="booked-flight-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'order_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'status_text')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_fare')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'base_fare')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'taxes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'departure_datetime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'return_datetime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'depart_num_connections')->textInput() ?>

    <?= $form->field($model, 'return_num_connections')->textInput() ?>

    <?= $form->field($model, 'depart_flight_segments')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'return_flight_segments')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'record_locators')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'deleted')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
