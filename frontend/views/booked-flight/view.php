<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Airport;
use yii\widgets\DetailView;

$departSegs = array_slice($model->_departFlightSegments, -1);
$lastSegment = array_pop($departSegs);


// flight type or connections
if ($model['depart_num_connections'] == 0) {
    $flightType = "Direct flight";
    $flightTextClass = "text-success";
} elseif($model['depart_num_connections'] == 1) {
    $flightType = "1" . " connecting flight";
    $flightTextClass = "text-warning";
} else {
    $flightType = $model['depart_num_connections'] . " connecting flights";
    $flightTextClass = "text-danger";
}




/* @var $this yii\web\View */
/* @var $model frontend\models\BookedFlight */

$this->title = "Flight from " . $model->origin->name . " to " . $model->destination->name;
$this->params['breadcrumbs'][] = ['label' => 'My bookings', 'url' => ['booking/index']];
$this->params['breadcrumbs'][] = ['label' => $model->event->name, 'url' => ['booking/view', 'id' => $model->booking_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booked-flight-view">

    <h1><?= Html::encode($this->title) ?></h1>

<div class="flight-list-item row">

    <div class="col-xs-12 col-sm-2">
        <p class="text text-center"><u>From</u></p>
        <p class="text text-center"><strong><?= $model->fromAirport->name ?></strong></p>
        <p class="text text-center"><u>To</u></p>
        <p class="text text-center"><strong><?= $model->toAirport->name ?></strong></p>

    </div>
    <div class="col-xs-12 col-sm-3">
        <p class="text text-center"><u>Departure</u></p>
        <p class="text text-center"><?php echo Yii::$app->formatter->asDateTime($model['departure_datetime']) ?></p>
        <p class="text text-center"><span class="glyphicon glyphicon-chevron-down"></span></p>
        <p class="text text-center"><?= $model->fromAirport->name ?></p>
    </div>
    <div class="col-xs-12 col-sm-2">
        <p class="text text-center"><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span></p>
        <p class="text text-center <?= $flightTextClass ?>"><?= $flightType ?></p>
    </div>
    <div class="col-xs-12 col-sm-3">
        <p class="text text-center"><u>Arrival</u></p>
        <p class="text text-center"><?php echo Yii::$app->formatter->asDateTime($lastSegment['land']) ?></p>
        <p class="text text-center"><span class="glyphicon glyphicon-chevron-down"></span></p>
        <p class="text text-center"><?= $model->toAirport->name ?></p>
    </div>
    <div class="col-xs-12 col-sm-2">
        <p class="flight-price text text-right text-success"><strong><?= $model['total_fare'] ?></strong></p>
        <?= DetailView::widget([
            'options' => [
                'class' => 'table table-condensed'
            ],
            'model' => $model,
            'attributes' => [
                // 'status',
            ],
            ]) ?>
    </div> 
    <div class="col-xs-12 col-sm-6">
        <div class="html-content">
        </div>
    </div>
    <div class="col-xs-12 col-sm-6">
        <div class="html-content">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-6 content-div">
        <h3 class="text text-center">Flight segments</h3>
            <?php foreach ($model->_departFlightSegments as $depFliSeg): ?>
                <p class="text text-center"> <strong><?php echo Airport::findByIata($depFliSeg['from'])->name ?></strong> &nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span> &nbsp;&nbsp;&nbsp; <strong><?php echo Airport::findByIata($depFliSeg['to'])->name ?></strong> </p>
                <p class="text text-center"></p>
                <?= DetailView::widget([
                    'options' => [
                        'class' => 'table table-condensed'
                    ],
                    'model' => $depFliSeg,
                    'attributes' => [
                        [
                            'label' => 'Flight Number',
                            'value' => $depFliSeg['flight_num'],
                        ],
                        [
                            'label' => 'Takeoff',
                            'format' => 'datetime',
                            'value' => $depFliSeg['takeoff'],
                        ],
                        [
                            'label' => 'Flight Duration',
                            'value' => $depFliSeg['duration'] . " minutes",
                        ],
                        [
                            'label' => 'Land',
                            'format' => 'datetime',
                            'value' => $depFliSeg['land'],
                        ],
                        'airline',
                    ],
                    ]) ?>
            <?php endforeach ?>
    </div>
    <div class="col-xs-12 col-md-6 content-div">
        <?php if (isset($model->_returnFlightSegments)): ?>
            <h3 class="text text-center">Return flight segments</h3>
                <?php foreach ($model->_returnFlightSegments as $retFliSeg): ?>
                    <p class="text text-center"> <strong><?php echo Airport::findByIata($retFliSeg['from'])->name ?></strong> &nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span> &nbsp;&nbsp;&nbsp; <strong><?php echo Airport::findByIata($retFliSeg['to'])->name ?></strong> </p>
                    <p class="text text-center"></p>
                    <?= DetailView::widget([
                        'options' => [
                            'class' => 'table table-condensed'
                        ],
                        'model' => $retFliSeg,
                        'attributes' => [
                            [
                                'label' => 'Flight Number',
                                'value' => $retFliSeg['flight_num'],
                            ],
                            [
                                'label' => 'Takeoff',
                                'format' => 'datetime',
                                'value' => $retFliSeg['takeoff'],
                            ],
                            [
                                'label' => 'Flight Duration',
                                'value' => $retFliSeg['duration'] . " minutes",
                            ],
                            [
                                'label' => 'Land',
                                'format' => 'datetime',
                                'value' => $retFliSeg['land'],
                            ],
                            'airline',
                        ],
                        ]) ?>
                <?php endforeach ?>
        <?php endif ?>
    </div>

    <div class="col-xs-12 col-md-6 col-md-offset-3 content-div">    
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                // 'user_id',
                'order_number',
                // 'status',
                // 'status_text',
                'from',
                'to',
                'total_fare',
                'base_fare',
                'taxes',
                [
                    'label' => 'Departure',
                    'format' => 'datetime',
                    'value' => $model['departure_datetime'],
                ],
                [
                    'label' => 'Arrival',
                    'format' => 'datetime',
                    'value' => $lastSegment['land'],
                ],
            ],
        ]) ?>
    </div>

</div>


</div>
