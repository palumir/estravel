<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use frontend\assets\FontAsset;
use frontend\models\Event;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
FontAsset::register($this);
$this->title = "Esports Travel";

// Get post.
$request = Yii::$app->request;
$get = $request->get();

// Get URL
$url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
$escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );

// If we are in an event, check the id.
if(strpos($escaped_url, "event") !== false && isset($get['id']) && strpos($escaped_url, "booking") !== false) {
	$event = Event::findOne($get['id']);
	if(is_object($event)) {
		$this->title .= " - " . $event->name;
	}
}

// If there's an event id, append it to title, etc.
if(isset($get['event_id'])) {
	$event = Event::findOne($get['event_id']);
	$this->title .= " - " . $event->name;
	
	// Flights
	if(strpos($escaped_url, "flight") !== false) {
		$this->title .= " Flights";
	}
	
	// Hotels
	if(strpos($escaped_url, "hotel") !== false) {
		$this->title .= " Hotels";
	}
}

// Session
$session = Yii::$app->session;

// Set session for bookings
if(!isset($session['booking_session_id'])) 
	$session['booking_session_id'] = [
    'value' => Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->session->getId()),
	'lifetime' => 0
];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => '<div class="hidden-xs logo-div"><img class="logo img pull-left center-block" src="../images/logo.png"> <div class="hidden-sm logo-text">Esports Travel</div></div>
							     <div class="visible-xs logo-div"><img class="center-block" src="../images/logo-xs.png"><div class="logo-text-xs">Esports Travel</div></div>',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                    'id' => 'top-nav',
                ],
            ]);
            $menuItems = [
                ['label' => 'Home', 'url' => ['/site/index']],
                // ['label' => 'Flights', 'url' => ['/flight/search'], 'options' => [/*'class' => 'game'*/]],
                // ['label' => 'Hotels', 'url' => ['/hotel/search'], 'options' => [/*'class' => 'game'*/]],
                // ['label' => 'Articles', 'url' => ['/article/index'], 'options' => [/*'class' => 'game'*/]],
                ['label' => 'Events', 'url' => ['/event/index'], 'options' => [/*'class' => 'game'*/]],
                // ['label' => 'Support', 'url' => ['/ticket/index'], 'options' => [/*'class' => 'star'*/]],
            ];
            if (Yii::$app->user->isGuest) {
                 $menuItems[] = [
                    'label' => 'My Bookings',
                    'url' => ['/booking/index'],
                    'options' => [/*'class' => 'travel'*/]
                ];
                $menuItems[] = ['label' => 'Log In', 'url' => ['/site/enter']];
                // $menuItems[] = ['label' => '<span class="glyphicon glyphicon-log-in"></span>', 'url' => ['/site/login'], 'encode' => false];
            } else {
                $menuItems[] = [
                    'label' => 'My Bookings',
                    'url' => ['/booking/index'],
                    'options' => [/*'class' => 'travel'*/]
                ];
                $menuItems[] = [
                    'label' => (Yii::$app->user->identity->firstname && Yii::$app->user->identity->lastname)?Yii::$app->user->identity->fullName():Yii::$app->user->identity->username,
                    'url' => ['/user/account'],
                    'options' => [/*'class' => 'travel'*/]
                ];
                // $menuItems[] = [
                //     'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                //     'url' => ['/site/logout'],
                //     'linkOptions' => ['data-method' => 'post']
                // ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>
		<div class="hidden-xs navbar-spacing-large"></div>
		<div class="visible-xs navbar-spacing-small"></div>
        <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <p><?= date('Y') ?> &copy; Esports Travel Agency. All rights Reserved.</p>
                    <p>All trademarks, service marks, trade names, trade dress, product names and logos appearing on the site are property of their respective owners, including in some instances Esports Travel Agency. Any rights not expressly granted are reserved.</p>
                </div>
				<div class="pull-left col-xs-12 col-sm-2"> 
					<div id="help-text">24/7 Help Line:</div>
					<div id="help-number"><a href="tel:1-800-628-6668">1-800-628-6668</a></div>
					<div id="help-asterisk">* fees may apply</div>
				</div>
                <div class="pull-right col-xs-12 col-sm-4">
					<p class="pull-right"> <?= Html::a("Articles", ['article/index']); ?> &nbsp; &nbsp; &nbsp; <?= Html::a("Corporate", 'https://cde.faregrabbr.com'); ?>  &nbsp; &nbsp; &nbsp; <?= Html::a("Contact", ['site/contact']); ?></p>                    
                </div>
            </div>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
