<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

?>


<div class="flight-index row">

  <?php	
  	function getAirportCodeFromStr($str) {
		$start  = strpos($str, '(');
		$end    = strpos($str, ')', $start + 1);
		$length = $end - $start;
		$result = substr($str, $start + 1, $length - 1);
		return $result;
	}
	
	 echo ListView::widget([
		'summary' => false,
		'dataProvider' => $flightDataProvider,
		'itemOptions' => ['class' => 'item center-block'],
		'itemView' => '__flight',
		'viewParams' => [
			'event_id' => $event_id,
			'flightSearchForm' => $flightSearchForm,
		],
	  ]);
  ?>
</div>


