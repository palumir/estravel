<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\jui\SliderInput;
use yii\widgets\ListView;
use kartik\select2\Select2;
use yii\web\JsExpression;
use common\models\Airport;
/* @var $this yii\web\View */
/* @var $flightSearchForm common\models\Book */
/* @var $form yii\widgets\ActiveForm */

$airport_url = \yii\helpers\Url::to(['airport/ajax-list']);
$airportFullname_url = \yii\helpers\Url::to(['airport/ajax-fullname']);

// var_dump($flightSearchForm->origin); exit;

// if ($model->airport) {
//     $airport = $model->airport->name . ", " . $model->airport->city . ", " . $model->airport->country . " - " . $model->airport->iata_code;
// } else {
//     $airport = "";
// }

$this->title = 'Flights';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="flights row">
    <div class="search-form-wrap col-xs-12 col-sm-12">
        <?php 

            $form = ActiveForm::begin([
                'options' => ['class' => 'ajax-search-form row', /*'target' => '_blank'*/],
                'method' => 'get',
                'action' => ['flight/search', 'event_id' => $event_id],
            ]); 

        ?>

		<div class="col-xs-12 origin-and-departure">
        <?php echo $form->field($flightSearchForm, 'origin', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-6']])->widget(Select2::classname(), [
                    // 'initValueText' => "here here", // set the initial display name
                    // 'theme' => Select2::THEME_DEFAULT,
                    // 'options' => ['placeholder' => 'Your preferred airport ...'],
                    'pluginOptions' => [
                        // 'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $airport_url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression("

                            function(airport) { 
                                if (airport.name) {
                                    return airport.name;
                                } else {
                                    return airport.text;
                                }
                            }

                            "),
                        'templateSelection' => new JsExpression("

                            // this is a complicated fix to my problem but a fix nonetheless
                            function (airport) {
                                if (airport.name) {
                                    return airport.name;
                                } else {
                                    return airport.text;
                                }
                            }

                            "),
                    ],
                ]); ?>
        <?php echo $form->field($flightSearchForm, 'destination', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-6']])->widget(Select2::classname(), [
                    // 'initValueText' => $airport, // set the initial display name
                    // 'theme' => Select2::THEME_DEFAULT,
                    // 'options' => ['placeholder' => 'Your preferred airport ...'],
                    'pluginOptions' => [
                        // 'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $airport_url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('

                            function(airport) { 
                                if (airport.name) {
                                    return airport.name;
                                } else {
                                    return airport.text;
                                }
                            }

                            '),
                        'templateSelection' => new JsExpression('

                            function (airport) {
                                if (airport.name) {
                                    return airport.name;
                                } else {
                                    return airport.text;
                                }
                            }

                            '),
                    ],
                ]); ?>
		</div>
		<div class="col-xs-12">
        <?php echo $form->field($flightSearchForm, 'departure', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-6']])->widget(DatePicker::className()) ?>
		<?php echo $form->field($flightSearchForm, 'return', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-6', 'style'=>""]])->widget(DatePicker::className()); ?>
		
		<?php
		
		if(!(isset($flightSearchForm->roundTrip) && $flightSearchForm->roundTrip == true)) 
			// Hide the return flight if it's not a round trip.
			$this->registerJs('function hideReturn() {
							// Hide and set return flight to be null
							$(".field-flightsearchform-return").hide();
							}
						  hideReturn();');
		?>
		</div>
		<div class="col-xs-12">
        <?php echo $form->field($flightSearchForm, 'adults', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->dropDownList([1=>1, 2=>2, 3=>3, 4=>4, 5=>5]) ?>
        <?php echo $form->field($flightSearchForm, 'children', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->dropDownList([0=>0, 1=>1, 2=>2, 3=>3, 4=>4, 5=>5]) ?>
        <?php echo $form->field($flightSearchForm, 'infants', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->dropDownList([0=>0, 1=>1, 2=>2, 3=>3, 4=>4, 5=>5]) ?>
        <?php echo $form->field($flightSearchForm, 'class', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->dropDownList(['economy' => 'Economy', 'business' => 'Business', 'first' => 'First', ]) ?>
		</div>
		<div class="col-xs-12">
		<?php $this->registerJs('$(document).ready(function(){
				$("#flightsearchform-roundtrip").change(function(){
					if($(this).is(":checked")){
						// Unhide the return flight
						$(".field-flightsearchform-return").show();
					   }else{
						// Hide and set return flight to be null
						$(".field-flightsearchform-return").hide();
					   }
					});

			});');
			?>
		<?php echo $form->field($flightSearchForm, 'roundTrip', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-2']])->checkBox() ?>
		<?php echo $form->field($flightSearchForm, 'directFlights', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-2']])->checkBox() ?>
		</div>
		<?php 
			// Accordion js
		$this->registerJs("$( function() {
			$( \"#accordion\" ).accordion({
				header: \"#advanced-search\",
				collapsible: true,
				autoHeight: false,
				active: false
			});
		 });");
		?>
		<div class="col-xs-12" id='accordion-container'>
		<div id="accordion">
		<div id="advanced-search">Advanced Search</div>
		<div id="accordion-content">
		<?php 
		
		/////////////////////////////////////////
		///////      DEPART FLIGHT SLIDER ///////
		/////////////////////////////////////////
		?>
		<div id="departureFlightTimeSlider" class="advanced-search-slider col-xs-12">
		<?php
		echo $form->field($flightSearchForm, "departureFlightTimeMin")->hiddenInput()->label(false);
		echo $form->field($flightSearchForm, "departureFlightTimeMax")->hiddenInput()->label(false);
		?>
		<?php
		echo $form->field($flightSearchForm, "departureFlightTime", ['options' => ['class' => 'col-xs-12 col-sm-12 col-md-12']])->widget(\yii\jui\SliderInput::classname(), [
			'clientOptions' => [
				'animate' => true,
				'values' => [$flightSearchForm->departureFlightTimeMin, $flightSearchForm->departureFlightTimeMax],
				'range' => true,
				'step' => 0.25,
				'min' => 0,
				'max' => 24,
			]]);
		?>
		<span id="departureFlightTimeMin" class="col-xs-1 slider-range-time-min">
		12:00am
		</span>
		<span id="departureFlightTimeMax" class="col-xs-1 slider-range-time-max">
		11:59pm
		</span>
		<?php 
		
		// JS that responds to the the departureTimeMin/Max being changed.
		$this->registerJs('
		
		function decimalToTime(decimal){
			 var hour = Math.floor(Math.abs(decimal));
			 var min = Math.floor((Math.abs(decimal) * 60) % 60);
			 
			 // Set am or pm.
			 var amOrPm = "am";
			 if(hour >= 12) {
				 amOrPm = "pm";
			 }
			 
			 // Minute 0 case.
			 if(min==0) {
				 min = "00";
			 }
			 
			 // Hours
			 if(hour==0) {
				 hour = 12;
			 }
			 else if(hour==24) {
				 hour = 11;
				 min = 59;
			 }
			 else if(hour>=13) {
				 hour = hour - 12;
			 }
			
			 return hour + ":" + min + amOrPm;
		}
		
		var minSlide = 0, maxSlide = 24;
		$("#flightsearchform-departureflighttime-container").slider({
		
			slide: function( event, ui ) {
			   if(minSlide != parseFloat(ui.values[0], 10)){
				  // Do what in minimum handle case
				  document.getElementsByName("FlightSearchForm[departureFlightTimeMin]")[0].value = ui.values[0];
				  document.getElementById("departureFlightTimeMin").innerHTML = decimalToTime(ui.values[0]);
			   }
			   else if(maxSlide != parseFloat(ui.values[1], 10)) {
				  // Do what in maximum handle case
				  document.getElementsByName("FlightSearchForm[departureFlightTimeMax]")[0].value = ui.values[1];
				  document.getElementById("departureFlightTimeMax").innerHTML = decimalToTime(ui.values[1]);
			   }
			},
			
			stop: function(event, ui){
				minSlide = parseFloat(ui.values[0], 10);
				maxSlide = parseFloat(ui.values[1], 10);               
		}});
		
		$("#departureFlightTimeSlider #departureFlightTimeMax").appendTo($("#flightsearchform-departureflighttime-container span").get(1));
		$("#departureFlightTimeSlider #departureFlightTimeMin").appendTo($("#flightsearchform-departureflighttime-container span").get(0));
					');
		
		?>
		</div>
		<?php 
		/////////////////////////////////////////
		///////      RETURN FLIGHT SLIDER ///////
		/////////////////////////////////////////
		?>
		<div id="returnFlightTimeSlider" class="advanced-search-slider col-xs-12">
		<?php
		echo $form->field($flightSearchForm, "returnFlightTimeMin")->hiddenInput()->label(false);
		echo $form->field($flightSearchForm, "returnFlightTimeMax")->hiddenInput()->label(false);
		?>
		<?php
		echo $form->field($flightSearchForm, "returnFlightTime", ['options' => ['class' => 'col-xs-12 col-sm-12 col-md-12']])->widget(\yii\jui\SliderInput::classname(), [
			'clientOptions' => [
				'animate' => true,
				'values' => [$flightSearchForm->returnFlightTimeMin, $flightSearchForm->returnFlightTimeMax],
				'range' => true,
				'step' => 0.25,
				'min' => 0,
				'max' => 24,
			]]);
		?>
		<span id="returnFlightTimeMin" class="col-xs-1 slider-range-time-min">
		12:00am
		</span>
		<span id="returnFlightTimeMax" class="col-xs-1 slider-range-time-max">
		11:59pm
		</span>
		<?php 
		
		// JS that responds to the the departureTimeMin/Max being changed.
		$this->registerJs('
		
		var minSlide = 0, maxSlide = 24;
		$("#flightsearchform-returnflighttime-container").slider({
		
			slide: function( event, ui ) {
			   if(minSlide != parseFloat(ui.values[0], 10)){
				  // Do what in minimum handle case
				  document.getElementsByName("FlightSearchForm[returnFlightTimeMin]")[0].value = ui.values[0];
				  document.getElementById("returnFlightTimeMin").innerHTML = decimalToTime(ui.values[0]);
			   }
			   else if(maxSlide != parseFloat(ui.values[1], 10)) {
				  // Do what in maximum handle case
				  document.getElementsByName("FlightSearchForm[returnFlightTimeMax]")[0].value = ui.values[1];
				  document.getElementById("returnFlightTimeMax").innerHTML = decimalToTime(ui.values[1]);
			   }
			},
			
			stop: function(event, ui){
				minSlide = parseFloat(ui.values[0], 10);
				maxSlide = parseFloat(ui.values[1], 10);               
		}});
		
		$("#returnFlightTimeSlider #returnFlightTimeMax").appendTo($("#flightsearchform-returnflighttime-container span").get(1));
		$("#returnFlightTimeSlider #returnFlightTimeMin").appendTo($("#flightsearchform-returnflighttime-container span").get(0));
					');
		
		?>
		</div>
		<?php 
		/////////////////////////////////////////////////
		///////      DEPARTURE JOURNEY LENGTH SLIDER ///////
		/////////////////////////////////////////////////
		?>
		<div id="departureJourneyDurationSlider" class="advanced-search-slider col-xs-12">
		<?php
		echo $form->field($flightSearchForm, "departureJourneyDurationMin")->hiddenInput()->label(false);
		echo $form->field($flightSearchForm, "departureJourneyDurationMax")->hiddenInput()->label(false);
		?>
		<?php
		echo $form->field($flightSearchForm, "departureJourneyDuration", ['options' => ['class' => 'col-xs-12 col-sm-12 col-md-12']])->widget(\yii\jui\SliderInput::classname(), [
			'clientOptions' => [
				'animate' => true,
				'values' => [$flightSearchForm->departureJourneyDurationMin, $flightSearchForm->departureJourneyDurationMax],
				'range' => true,
				'step' => 0.5,
				'min' => 0,
				'max' => 48,
			]]);
		?>
		<span id="departureJourneyDurationMin" class="col-xs-1 slider-range-time-min">
		0 hours
		</span>
		<span id="departureJourneyDurationMax" class="col-xs-1 slider-range-time-max">
		48 hours
		</span>
		<?php 
		
		// JS that responds to the the departureTimeMin/Max being changed.
		$this->registerJs('
		
		function decimalToHours(decimal){
			 return decimal + " hours";
		}
		
		var minSlide = 0, maxSlide = 24;
		$("#flightsearchform-departurejourneyduration-container").slider({
		
			slide: function( event, ui ) {
			   if(minSlide != parseFloat(ui.values[0], 10)){
				  // Do what in minimum handle case
				  document.getElementsByName("FlightSearchForm[departureJourneyDurationMin]")[0].value = ui.values[0];
				  document.getElementById("departureJourneyDurationMin").innerHTML = decimalToHours(ui.values[0]);
			   }
			   else if(maxSlide != parseFloat(ui.values[1], 10)) {
				  // Do what in maximum handle case
				  document.getElementsByName("FlightSearchForm[departureJourneyDurationMax]")[0].value = ui.values[1];
				  document.getElementById("departureJourneyDurationMax").innerHTML = decimalToHours(ui.values[1]);
			   }
			},
			
			stop: function(event, ui){
				minSlide = parseFloat(ui.values[0], 10);
				maxSlide = parseFloat(ui.values[1], 10);               
		}});
		
		$("#departureJourneyDurationSlider #departureJourneyDurationMax").appendTo($("#flightsearchform-departurejourneyduration-container span").get(1));
		$("#departureJourneyDurationSlider #departureJourneyDurationMin").appendTo($("#flightsearchform-departurejourneyduration-container span").get(0));
					');
		
		?>
		</div>
		<?php 
		/////////////////////////////////////////////////
		///////      RETURN JOURNEY LENGTH SLIDER ///////
		/////////////////////////////////////////////////
		?>
		<div id="returnJourneyDurationSlider" class="advanced-search-slider col-xs-12">
		<?php
		echo $form->field($flightSearchForm, "returnJourneyDurationMin")->hiddenInput()->label(false);
		echo $form->field($flightSearchForm, "returnJourneyDurationMax")->hiddenInput()->label(false);
		?>
		<?php
		echo $form->field($flightSearchForm, "returnJourneyDuration", ['options' => ['class' => 'col-xs-12 col-sm-12 col-md-12']])->widget(\yii\jui\SliderInput::classname(), [
			'clientOptions' => [
				'animate' => true,
				'values' => [$flightSearchForm->returnJourneyDurationMin, $flightSearchForm->returnJourneyDurationMax],
				'range' => true,
				'step' => 0.5,
				'min' => 0,
				'max' => 48,
			]]);
		?>
		<span id="returnJourneyDurationMin" class="col-xs-3 slider-range-time-min">
		0 hours
		</span>
		<span id="returnJourneyDurationMax" class="col-xs-3 slider-range-time-max">
		48 hours
		</span>
		<?php 
		
		// JS that responds to the the returnTimeMin/Max being changed.
		$this->registerJs('
		
		var minSlide = 0, maxSlide = 24;
		$("#flightsearchform-returnjourneyduration-container").slider({
		
			slide: function( event, ui ) {
			   if(minSlide != parseFloat(ui.values[0], 10)){
				  // Do what in minimum handle case
				  document.getElementsByName("FlightSearchForm[returnJourneyDurationMin]")[0].value = ui.values[0];
				  document.getElementById("returnJourneyDurationMin").innerHTML = decimalToHours(ui.values[0]);
			   }
			   else if(maxSlide != parseFloat(ui.values[1], 10)) {
				  // Do what in maximum handle case
				  document.getElementsByName("FlightSearchForm[returnJourneyDurationMax]")[0].value = ui.values[1];
				  document.getElementById("returnJourneyDurationMax").innerHTML = decimalToHours(ui.values[1]);
			   }
			},
			
			stop: function(event, ui){
				minSlide = parseFloat(ui.values[0], 10);
				maxSlide = parseFloat(ui.values[1], 10);               
		}});
		
		$("#returnJourneyDurationSlider #returnJourneyDurationMax").appendTo($("#flightsearchform-returnjourneyduration-container span").get(1));
		$("#returnJourneyDurationSlider #returnJourneyDurationMin").appendTo($("#flightsearchform-returnjourneyduration-container span").get(0));
					');
		
		?>
		</div>
		</div>
		</div>
		</div>
        <div class="form-group">
            <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary btn-block']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        
    </div>

    <div class="search-results col-xs-12">

        <?php
            echo $this->render('_results', [
                    'event_id' => $event_id,
                    'flightSearchModel' => $flightSearchModel,
                    'flightDataProvider' => $flightDataProvider,
                    'flightSearchForm' => $flightSearchForm,
                ]);
        ?>

    </div>


</div>

<div class="spinner">
    
</div>


