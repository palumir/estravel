<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\widgets\DetailView;

// flight json
$flightJson = Json::encode($model);

// flight type or connections
if ($model['depart_num_connections'] == 0) {
	$flightType = "Direct flight";
	$flightTextClass = "text-success";
} elseif($model['depart_num_connections'] == 1) {
	$flightType = "1" . " connecting flight";
	$flightTextClass = "text-warning";
} else {
	$flightType = $model['depart_num_connections'] . " connecting flights";
	$flightTextClass = "text-danger";
}

?>
<div class="col-xs-12">
	<a class="flight-link-wrap" 
		target="_blank"
		href="<?= Url::to([
			'flight/preload', 
			'event_id' => $event_id,
			'origin' => $flightSearchForm->origin,
			'destination' => $flightSearchForm->destination,
			'departure_date' => $flightSearchForm->departure,
			'adults' => $flightSearchForm->adults,
			'children' => $flightSearchForm->children,
			'infants' => $flightSearchForm->infants,
			'model' => $model
			]) ?>"

		data-flight='<?= $flightJson ?>' 
	>
	<?php
		// Gather the list of connections.
		$connections = array();
		foreach($model['departflightsegments'] as $segment) {
			$connections[] = "<div class='text-center'>" . getAirportCodeFromStr($segment["from"]) . ' <span class="glyphicon glyphicon-chevron-right"></span> ' . getAirportCodeFromStr($segment["to"]) . "</div>";
		}
	
	?>
		<div class="flight-list-item row">

			<div class="col-xs-12 col-sm-2">
				<h2 class="text text-center"><?= $model['depart_airline_name'] ?></h2>
			</div>
			<div class="col-xs-12 col-sm-2">
				<p class="text text-center"><?php echo date('g:i a', strtotime($model['depart_start_time'])) ?></p>
				<p class="text text-center"><span class="glyphicon glyphicon-map-marker"></span></p>
				<p class="text text-center"><?= $model['departflightsegments'][0]['from'] ?></p>
			</div>
			<div class="col-xs-12 col-sm-2">
				<p class="text text-center"><?php echo date('H\h:i\m', mktime(0,$model['depart_duration'])); ?></p>
				<p class="text text-center"><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span></p>
				<span class="hasTooltip text-center" style="width:100%">
				<p class="text text-center <?= $flightTextClass ?>"><?= $flightType ?></p>
				<div class="tooltipText">
				<?php
					foreach($connections as $connection) echo $connection;
				?>
				</div>
				</span>
			</div>
			<div class="col-xs-12 col-sm-2">
				<p class="text text-center"><?php echo date('g:i a', strtotime($model['depart_end_time'])) ?></p>
				<p class="text text-center"><span class="glyphicon glyphicon-map-marker"></span></p>
				<p class="text text-center"><?= $model['departflightsegments'][count($model['departflightsegments']) - 1]['to'] ?></p>
			</div>
			<div class="col-xs-12 col-sm-4">
				<p class="flight-price text text-right text-success"><strong><?= $model['fare'] ?></strong></p>
				<?= DetailView::widget([
	                'options' => [
	                    'class' => 'table table-condensed'
	                ],
					'model' => $model,
					'attributes' => [
						'class',
					],
					]) ?>
			</div> 
			<div class="col-xs-12 col-sm-6">
				<div class="html-content">
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="html-content">
				</div>
			</div>
		</div>
	</a>
</div>



