<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\data\ArrayDataProvider;

$this->title = "Book flight to " . $event->name;
$this->params['breadcrumbs'][] = ['label' => 'Flights to ' . $event->name, 'url' => ['flight/search', 'event_id' => $event->id, 'default' => true]];
$this->params['breadcrumbs'][] = ['label' => 'Flight to ' . $event->name, 'url' => ['flight/view', 'fK' => $model->cacheKey]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flight-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Cancel', ['event/view', 'id' => $event->id], [
            'class' => 'btn btn-danger',
        ]) ?>
    </p>



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'origin',
            'destination',
            'departure_date',
            'return_date',
            'adults',
            'children',
            'infants',
            'class',
            'base_fare',
            'taxes_and_fees',
            'total_fare',
        ],
    ]) ?>

        <?php
            echo $this->render('_creditCardForm', [
                    'creditCardForm' => $creditCardForm,
                ]);
        ?>

</div>
