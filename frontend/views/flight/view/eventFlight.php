<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\data\ArrayDataProvider;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use common\models\Airport;

$departureDataProvider = new ArrayDataProvider();
$departureDataProvider->allModels = $model['departureflightsegments'];
$returnDataProvider = new ArrayDataProvider();
$returnDataProvider->allModels = $model['returnflightsegments'];


$this->title = "Flight to " . $event->name;
$this->params['breadcrumbs'][] = ['label' => 'Flights to ' . $event->name, 'url' => ['flight/search', 'event_id' => $event->id, 'default' => true]];
$this->params['breadcrumbs'][] = $this->title;

// flight type or connections
if ($model['depart_num_connections'] == 0) {
    $flightType = "Direct flight";
    $flightTextClass = "text-success";
} elseif($model['depart_num_connections'] == 1) {
    $flightType = "1" . " connecting flight";
    $flightTextClass = "text-warning";
} else {
    $flightType = $model['depart_num_connections'] . " connecting flights";
    $flightTextClass = "text-danger";
}

?>
<div class="flight-view row">

    <h1 class="text text-center"><?= Html::encode($this->title) ?></h1>

	
<?php

$flightModel = $model;

$departFlightSegments = array_slice($flightModel->departureflightsegments,-1);
$lastSegment = array_pop($departFlightSegments);

// flight type or connections
if ($flightModel['depart_num_connections'] == 0) {
	$flightType = "Direct flight";
	$flightTextClass = "text-success";
} elseif($flightModel['depart_num_connections'] == 1) {
	$flightType = "1" . " connecting flight";
	$flightTextClass = "text-warning";
} else {
	$flightType = $flightModel['depart_num_connections'] . " connecting flights";
	$flightTextClass = "text-danger";
}

$firstFlight = $flightModel->departureflightsegments[0];
$lastFlight = $flightModel->departureflightsegments[count($flightModel->departureflightsegments)-1];

?>

		<div class="flight-list-item row">
			<div class="col-xs-12 col-sm-2">
				<p class="text text-center"><u>From</u></p>
				<p class="text text-center"><strong><?= $firstFlight->from ?></strong></p>
				<p class="text text-center"><u>To</u></p>
				<p class="text text-center"><strong><?= $lastFlight->to ?></strong></p>
			</div>
			<div class="col-xs-12 col-sm-3">
				<p class="text text-center"><u>Departure</u></p>
				<p class="text text-center"><?php echo Yii::$app->formatter->asDateTime($firstFlight->takeoff) ?></p>
				<p class="text text-center"><span class="glyphicon glyphicon-map-marker"></span></p>
				<p class="text text-center"><?= $firstFlight->from ?></p>
			</div>
			<div class="col-xs-12 col-sm-2">
				<p class="text text-center"><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span></p>
				<p class="text text-center <?= $flightTextClass ?>"><?= $flightType ?></p>
			</div>
			<div class="col-xs-12 col-sm-3">
				<p class="text text-center"><u>Arrival</u></p>
				<p class="text text-center"><?php echo Yii::$app->formatter->asDateTime($lastFlight->land) ?></p>
				<p class="text text-center"><span class="glyphicon glyphicon-map-marker"></span></p>
				<p class="text text-center"><?= $lastFlight->to ?></p>
			</div>
			<div class="col-xs-12 col-sm-2">
				<p class="flight-price text text-right text-success"><strong><?= $flightModel['total_fare'] ?></strong></p>
				<?= DetailView::widget([
	                'options' => [
	                    'class' => 'table table-condensed'
	                ],
					'model' => $flightModel,
					'attributes' => [
						// 'status',
					],
					]) ?>
			</div> 
			<div class="col-xs-12 col-sm-6">
				<div class="html-content">
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="html-content">
				</div>
			</div>
		</div>
<div class="row">
	<div class="col-xs-12 col-md-6">
		<h3 class="text text-center">Flight segments</h3>
			<?php foreach ($flightModel->departureflightsegments as $depFliSeg): ?>
			<?php
				$airportNameSplit = explode("(",$depFliSeg->from,2);
				$fromAirport = $depFliSeg->from;
				$airportNameSplit = explode("(",$depFliSeg->to,2);
				$toAirport = $depFliSeg->to;
			?>
				<p class="text text-center"> <strong><?php echo $fromAirport ?></strong> &nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span> &nbsp;&nbsp;&nbsp; <strong>
				<?php 
				echo $toAirport ?></strong> </p>
				<p class="text text-center"></p>
				<?= DetailView::widget([
	                'options' => [
	                    'class' => 'table table-condensed'
	                ],
					'model' => $depFliSeg,
					'attributes' => [
				        [
				            'label' => 'Flight Number',
				            'value' => $depFliSeg['flight_num'],
				        ],
				        [
				            'label' => 'Takeoff',
				            'format' => 'datetime',
				            'value' => $depFliSeg['takeoff'],
				        ],
				        [
				            'label' => 'Flight Duration',
				            'value' => $depFliSeg['duration'] . " minutes",
				        ],
				        [
				            'label' => 'Land',
				            'format' => 'datetime',
				            'value' => $depFliSeg['land'],
				        ],
					],
					]) ?>
			<?php endforeach ?>
	</div>
	<div class="col-xs-12 col-md-6">
		<?php if (isset($flightModel->returnflightsegments)): ?>
			<h3 class="text text-center">Return flight segments</h3>
				<?php foreach ($flightModel->returnflightsegments as $retFliSeg): ?>
					<?php
						$airportNameSplit = explode("(",$retFliSeg->from,2);
						$fromAirport = $retFliSeg->from;
						$airportNameSplit = explode("(",$retFliSeg->to,2);
						$toAirport = $retFliSeg->to;
					?>
					
					<p class="text text-center"> <strong><?php echo $fromAirport; ?></strong> &nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-chevron-right"></span>
					<span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span> &nbsp;&nbsp;&nbsp; <strong><?php echo $toAirport; ?></strong> </p>
					<p class="text text-center"></p>
					<?= DetailView::widget([
		                'options' => [
		                    'class' => 'table table-condensed'
		                ],
						'model' => $retFliSeg,
						'attributes' => [
					        [
					            'label' => 'Flight Number',
					            'value' => $retFliSeg['flight_num'],
					        ],
					        [
					            'label' => 'Takeoff',
					            'format' => 'datetime',
					            'value' => $retFliSeg['takeoff'],
					        ],
					        [
					            'label' => 'Flight Duration',
					            'value' => $retFliSeg['duration'] . " minutes",
					        ],
					        [
					            'label' => 'Land',
					            'format' => 'datetime',
					            'value' => $retFliSeg['land'],
					        ],
						],
						]) ?>
				<?php endforeach ?>
		<?php endif ?>
	</div>
</div>
</div>

    <div class="col-xs-12 col-sm-12 ">
		<div>
        <p class="text">
            <?php // echo Html::a('Book this Flight', ['book', 'fK' => $model->cacheKey], ['class' => 'btn btn-primary']) ?>
			<?php 
			$form = ActiveForm::begin([
				'id' => 'save-form',
				'class' => 'row',
				'action' => ['booking/view', 'fK' => $fK, 'passengers' => json_encode($passengers), 'event_id' => $event->id]
			]); ?>


			<div class="form-group col-xs-12 col-md-6" style="float:right;">
			<?= Html::submitButton("Select Flight", ['class' => 'btn btn-block btn-lg btn-success']) ?>
			</div>

			<?php ActiveForm::end(); ?>
        </p>
		</div>
    </div>
