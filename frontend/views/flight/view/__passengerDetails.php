<?php

use yii\helpers\Html;
use dosamigos\datepicker\DatePicker;


?>

<div class="passenger-details row">

    <h3><?php
        if ($index==0) {
            echo "Your passenger details";
        } else {
            $i = $index+1;
            echo "Passenger $i details";
        }
    ?></h3>

    <div class="col-xs-12">
        <div class="row">
        <?= $form->field($passenger, "[".$index."]first_name", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($passenger, "[".$index."]middle_name", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($passenger, "[".$index."]last_name", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($passenger, "[".$index."]type", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->textInput(['disabled' => 'disabled']) ?>
        <?= $form->field($passenger, "[".$index."]type", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->hiddenInput()->label(false)->error(false); ?>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <?= $form->field($passenger, "[".$index."]date_of_birth", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->widget(DatePicker::className(), []) ?>
            <?= $form->field($passenger, "[".$index."]gender", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-2']])->dropDownList(['' => '', 'M' => 'M', 'F' => 'F']) ?>
            <?= $form->field($passenger, "[".$index."]number", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->textInput(['maxlength' => true]) ?>
            <?= $form->field($passenger, "[".$index."]email", ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-4']])->textInput(['maxlength' => true]) ?>
        </div>
    </div>
</div>
<hr>
