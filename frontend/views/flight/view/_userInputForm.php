<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\CreditCardForm;

/* @var $this yii\web\View */
/* @var $creditCardForm common\models\CreditCardForm */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="userInputForm-form">

    <?php $form = ActiveForm::begin([
        'id' => 'booking-form',
        'class' => 'row',
        'action' => ['book', 'fK' => $fK],
    ]); ?>

    <div class="col-xs-12">
        <?php
            foreach ($passengers as $index => $passenger) {
                echo $this->render('__passengerDetails', [
                    'form' => $form,
                    'index' => $index,
                    'passenger' => $passenger,
                    ]);
            }
        ?>
    </div>

    <h2>Credit Card:</h2>
    <p class="card-security-warning"><small>Please note that your card data is not stored on our server and we have taken precautions against <em>Cross-site request forgery</em></small></p>
    <div class="col-xs-12">
        <div class="row">
            <?= $form->field($creditCardForm, 'number', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-5']])->textInput(['maxlength' => true]) ?>
            <?= $form->field($creditCardForm, 'code', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-3']])->textInput(['maxlength' => true]) ?>
            <?= $form->field($creditCardForm, 'month', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-2']])->dropDownList(Yii::$app->params['monthRange']) ?>
            <?= $form->field($creditCardForm, 'year', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-2']])->dropDownList(Yii::$app->params['yearRange']) ?>
        </div>
        <div class="row">
            <?= $form->field($creditCardForm, 'name', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-6']])->textInput(['maxlength' => true]) ?>

            <?= $form->field($creditCardForm, 'addressLine1', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-6']])->textInput(['maxlength' => true]) ?>
        </div>
        <div class="row">
            <?= $form->field($creditCardForm, 'city', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-4']])->textInput(['maxlength' => true]) ?>
            <?= $form->field($creditCardForm, 'state', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-4']])->textInput(['maxlength' => true]) ?>
            <?= $form->field($creditCardForm, 'zip', ['options' => ['class' => 'col-xs-12 col-sm-6 col-md-4']])->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton("Book Flight", ['class' => 'btn btn-block btn-lg btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
