<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\data\ArrayDataProvider;
use yii\bootstrap\Modal;


$departureDataProvider = new ArrayDataProvider();
$departureDataProvider->allModels = $model['departureflightsegments'];
$returnDataProvider = new ArrayDataProvider();
$returnDataProvider->allModels = $model['returnflightsegments'];


$this->title = "Flight to " . $model->destination;
$this->params['breadcrumbs'][] = ['label' => 'Search Flights', 'url' => ['flight/search']];
$this->params['breadcrumbs'][] = $this->title;

// flight type or connections
if ($model['depart_num_connections'] == 0) {
    $flightType = "Direct flight";
    $flightTextClass = "text-success";
} elseif($model['depart_num_connections'] == 1) {
    $flightType = "1" . " connecting flight";
    $flightTextClass = "text-warning";
} else {
    $flightType = $model['depart_num_connections'] . " connecting flights";
    $flightTextClass = "text-danger";
}


?>
<div class="flight-view row content-div">

    <h1 class="text text-center"><?= Html::encode($this->title) ?></h1>

    <div class="col-xs-12 col-sm-9">
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <h2 class="text text-center"><?= $model['depart_airline_name'] ?></h2>
            </div>
            <div class="col-xs-12 col-sm-1">
                <p class="text text-center"><?php echo Yii::$app->formatter->asTime($model['depart_start_time']) ?></p>
                <p class="text text-center"><span class="glyphicon glyphicon-chevron-down"></span></p>
                <p class="text text-center"><?= $model->origin ?></p>
            </div>
            <div class="col-xs-12 col-sm-3">
                <p class="text text-center"><?php echo date('H:i', mktime(0,$model['depart_duration'])); ?></p>
                <p class="text text-center"><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span></p>
                <p class="text text-center <?= $flightTextClass ?>"><?= $flightType ?></p>
            </div>
            <div class="col-xs-12 col-sm-1">
                <p class="text text-center"><?php echo Yii::$app->formatter->asTime($model['depart_end_time']) ?></p>
                <p class="text text-center"><span class="glyphicon glyphicon-chevron-down"></span></p>
                <p class="text text-center"><?= $model->destination ?></p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <p class="flight-price text text-right text-success"><strong><?= $model['fare'] ?></strong></p>
                <?= DetailView::widget([
                    'options' => [
                        'class' => 'table table-condensed'
                    ],
                    'model' => $model,
                    'attributes' => [
                        'class',
                    ],
                    ]) ?>
            </div> 
        </div>
        
    </div>
    <div class="col-xs-12 col-sm-3">
        <p class="text text-right">
            <?php // echo Html::a('Book this Flight', ['book', 'fK' => $model->cacheKey], ['class' => 'btn btn-primary']) ?>
            <?php

                if (!Yii::$app->user->isGuest) {
                    Modal::begin([
                        'header' => '<h2>Passenger & payment information</h2>',
                        'toggleButton' => ['label' => 'Book this flight', 'class' => 'btn btn-lg btn-success'],
                        'size' => Modal::SIZE_LARGE,
                    ]);

                        echo $this->render('_userInputForm', [
                                'fK' => $fK,
                                'passengers' => $passengers,
                                'creditCardForm' => $creditCardForm,
                            ]);

                    Modal::end();
                }

            ?>
        </p>
        <p class="text text-right">        
            <?= Html::a('Cancel', ['flight/search'], [
                'class' => 'btn btn-danger',
            ]) ?>
        </p>
    </div>



    <?= DetailView::widget([
        'options' => [
            'class' => 'table table-condensed'
        ],
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Origin Airport',
                'value' => $model->originAirport->name . ", " . $model->originAirport->city . ", " . $model->originAirport->country . " - " . $model->originAirport->iata_code,
            ],
            [
                'label' => 'Destination Airport',
                'value' => $model->destinationAirport->name . ", " . $model->destinationAirport->city . ", " . $model->destinationAirport->country . " - " . $model->destinationAirport->iata_code,
            ],
            [
                'label' => 'Departure',
                'value' => Yii::$app->formatter->asDatetime($model->depart_start_time),
            ],
            [
                'label' => 'Return',
                'value' => Yii::$app->formatter->asDatetime($model->return_start_time),
            ],
            'adults',
            'children',
            'infants',
            'class',
            // 'base_fare',
            // 'taxes_and_fees',
            'total_fare',
        ],
    ]) ?>
    <h2>Flight Segments</h2>
    <h3>Departure</h3>
        <?php
            echo ListView::widget([
                'summary' => false,
                'dataProvider' => $departureDataProvider,
                'itemOptions' => ['class' => 'item center-block'],
                'itemView' => '_flightSegment',
            ]);
        ?>
    <h3>Return</h3>
        <?php
            echo ListView::widget([
                'summary' => false,
                'dataProvider' => $returnDataProvider,
                'itemOptions' => ['class' => 'item center-block'],
                'itemView' => '_flightSegment',
            ]);
        ?>
</div>
