<?php

use yii\widgets\DetailView;

?>

    <?= DetailView::widget([
        'options' => [
            'class' => 'table table-condensed'
        ],
        'model' => $model,
        'attributes' => [
            'flight_num',
            'from',
            'to',
            'takeoff:datetime',
            'land:datetime',
            // 'duration',
            // 'code',
            // 'key',
            // 'layover',
            // 'leg',
            // 'seg',
        ],
    ]) ?>