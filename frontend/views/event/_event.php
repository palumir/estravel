<?php
use yii\helpers\Html;
use frontend\models\Event;

?>

<div class="row event" style="background-image: url('<?= $model->background ?>'); background-position: center; background-repeat: no-repeat; background-size: 100% auto;">

	<div class="col-xs-12 event-title">
	    <h2 class="text text-center"><?php echo $model->name ?></h2>	
	</div>

	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-sm-offset-8 event-row-inside">
			    <div class="event-info">
				    <p class="text text-right"><?php echo $model->city . ", " . $model->country->name . " (" . $model->country->code . ")" ?></p>
				    <p class="text text-right"><?php echo rtrim($model->gamesString," ,") ?></p>
				    <p class="text text-right"><?php echo Yii::$app->formatter->asDate($model->start_time, "php:dS F") . " - " . Yii::$app->formatter->asDate($model->end_time, "php:dS F Y") ?></p>
				    <p class="text text-right"><?php echo $model->intro ?></p>
			    </div>
			    <div class="row">
					<div class="col-xs-12 event-view">
					    <p class="text text-left"><?= Html::a("<span class='glyphicon glyphicon-eye-open'></span> View Details", ['event/view', 'id' => $model->id], ['class' => 'btn btn-travel btn-block']); ?></p>
					</div>
			    </div>
			</div>
		</div>
	</div>

</div>