<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>

  <?php Pjax::begin(); ?>
          <div class="row search-form content-div">
            <?php echo $this->render('_eventSearch', ['model' => $searchModel, 'dataProvider' => $dataProvider]); ?>            
          </div>
          <div class="row event-list">
            <?php
			
			   // Sort by date.
			   $dataProvider->sort->defaultOrder = ['start_time'=>SORT_ASC];
               echo ListView::widget([
                  'summary' => '',
                  'dataProvider' => $dataProvider,
                  'itemOptions' => ['class' => 'item center-block col-xs-12'],
                  'itemView' => '_event',
                  'separator' => '<hr><hr>',
                  // 'viewParams' => [
                  //         ],
                ]);
            ?>
          </div>
  <?php Pjax::end(); ?>
</div>
