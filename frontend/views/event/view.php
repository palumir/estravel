<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Event */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row event-view content-div">
    <div class="col-xs-12 event-image">
        <?= Html::img($model->background, ['class' => 'img img-responsive img-thumbnail', 'alt' => $model->name]) ?>
    </div>
    <div class="col-xs-12 col-sm-6">
        
        <div class="row">
            
            <div class="col-xs-12">
                <h2 class="text text-center"><?= Html::encode($this->title) ?></h1>
                <p><?php echo $model->city . ", " . $model->country->name . " (" . $model->country->code . ")" ?></p>
                <p><?php echo $model->gamesString ?></p>
                <p><?php echo Yii::$app->formatter->asDate($model->start_time, "php:dS F") . " - " . Yii::$app->formatter->asDate($model->end_time, "php:dS F Y") ?></p>
            </div>

            <div class="col-xs-12 event-intro">
                <p><?php echo $model->intro ?></p>
            </div>

            <div class="col-xs-12 event-summary">
                <p><?php echo $model->summary ?></p>
            </div>

            <div class="col-xs-12 event-description">
                <p><?php echo $model->description ?></p>
            </div>
        </div>

    </div>

    <div class="col-xs-12 col-sm-6 event-details">
        <h2>Details</h2>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'venue',
                'city',
                'country.name',
                [
                    'label' => 'Games',
                    'value' => $model->lineUpGames(),
                ],
            ],
        ]) ?>

        <p class="text text-right">
					    <div class="row">
					<div class="col-xs-6 event-view">
					    <p class="text text-left"><?= Html::a("<span class='glyphicon glyphicon-plane'></span> Search Flights", ['flight/search-round-trip', 'event_id' => $model->id, 'default' => true,], ['class' => 'btn btn-game btn-block']); ?></p>
					</div>
					<div class="col-xs-6 event-book">
					    <p class="text text-right"><?= Html::a("<span class='glyphicon glyphicon-home'></span> Search Hotels", ['hotel/search', 'event_id' => $model->id, 'default' => true,], ['class' => 'btn btn-travel btn-block']); ?></p>
					</div>
			    </div>
		</p>
    </div>


</div>
