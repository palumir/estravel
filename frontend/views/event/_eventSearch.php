<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Game;
use kartik\select2\Select2;
use yii\web\JsExpression;
use frontend\models\Event;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\EventSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-search col-sm-12">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'form-horizontal row']
    ]); ?>
	
	<?php 
	//print("<pre>"); print_r($dataProvider->getModels()); print("</pre>"); ?>

	<div class = "col-sm-3 col-m-3">
	<input list="events" class="form-control" name="EventSearch[name]" value="<?= $model->name ?>" placeholder = "Event Name">
	<datalist id="events">
	  <?php
		foreach($dataProvider->getModels() as  $currItem) {
			echo '<option value="' . $currItem->name . '">';
		}
	  ?>
	</datalist>
	</div>
	
	<div class = "col-sm-3 col-m-3">
	<input list="city" class="form-control" name="EventSearch[city]" value="<?= $model->city ?>" placeholder = "City">
	<datalist id="city">
	  <?php
		foreach($dataProvider->getModels() as  $currItem) {
			echo '<option value="' . $currItem->city . '">';
		}
	  ?>
	</datalist>
	</div>

    <?= $form->field($model, 'game.id', ['options' => ['class' => 'col-sm-3']])->dropDownList(Game::getDropDownArray())->label(false) ?>


    <div class="col-sm-3">

        <?= Html::submitButton('Search', ['class' => 'btn col-sm-12 btn-travel']) ?>

    </div>

    <?php ActiveForm::end(); ?>
</div>
