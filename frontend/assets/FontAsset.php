<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class FontAsset extends AssetBundle
{
    public $css = [
        'https://fonts.googleapis.com/css?family=Raleway',
    ];
    public $cssOptions = [
        'type' => 'text/css',
    ];
}