$(function(){
    $('#top-nav').data('status','no-scroll');
});

$(window).scroll(function(){
    if($(document).scrollTop() > 0)
    {
        if($('#top-nav').data('status') == 'no-scroll')
        {
            $('#top-nav').data('status','scroll');
            $('#top-nav').stop().animate({
                height:'50px'
            },300);
            $('.navbar-inverse .navbar-nav > li > a').stop().animate({
            	'line-height':'10px'
            }, 300);
			$('#top-nav img.logo').stop().animate({
                height:'37px',
				width:'34px'
            },300);
			$('.navbar-brand .logo-text').stop().animate({
                'font-size': '19px',
				'padding-top': '0px',
				'margin-top': '-20px'
            },300);
        }
    }
    else
    {
        if($('#top-nav').data('status') == 'scroll')
        {
			if($('#top-nav div.hidden-xs.logo-div').is(':visible')) {
				$('#top-nav').data('status','no-scroll');
				$('#top-nav').stop().animate({
					height:'119px'
				},300);
				$('.navbar-inverse .navbar-nav > li > a').stop().animate({
					'line-height':'80px'
				}, 300);
				$('#top-nav img.logo').stop().animate({
					height:'100px',
					width:'110px'
				},300);
				$('.navbar-brand .logo-text').stop().animate({
					'font-size': '28px',
					'padding-top': '15px',
					'margin-top': '0px'
				},300);
			}
        }  
    }
});