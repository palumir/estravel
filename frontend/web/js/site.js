jQuery(document).ready(function () {


	 $(document).on('submit', '.article-comment-form', function(e) {
	    $('.comments').addClass('loading');
	 	e.preventDefault();
	 	var action = $(this).attr('action');
	 		$.ajax({
				  type: "POST",
				  url: action,
				  data: $(this).serialize()
				})
				  .done(function( response ) {
					$("#articlecomment-body").val('');
				    $.pjax.reload({container:'#pjax-comments'});
		            $('.comments').removeClass('loading');
				    }
				);
	 });


	 $(document).on('submit', '.ticket-comment-form', function(e) {
	    $('.comments').addClass('loading');
	 	e.preventDefault();
	 	var action = $(this).attr('action');
	 		$.ajax({
				  type: "POST",
				  url: action,
				  data: $(this).serialize()
				})
				  .done(function( response ) {
					$("#ticketcomment-body").val('');
				    $.pjax.reload({container:'#pjax-comments'});
		            $('.comments').removeClass('loading');
				    }
				);
	 });


	 $(document).on('submit', '.news-comment-form', function(e) {
	    $('.comments').addClass('loading');
	 	e.preventDefault();
	 	var action = $(this).attr('action');
	 		$.ajax({
				  type: "POST",
				  url: action,
				  data: $(this).serialize()
				})
				  .done(function( response ) {
					$("#newscomment-body").val('');
				    $.pjax.reload({container:'#pjax-comments'});
		            $('.comments').removeClass('loading');
				    }
				);
	 });
	 
	 $(document).on('submit', '.spinner-form', function(e) {
		$('.spinner').addClass('spin');
	 });

	 $(document).on('submit', '.ajax-search-form', function(e) {
	 	e.preventDefault();
	    $('.spinner').addClass('spin');
	 	var action = $(this).attr('action');
	 		$.ajax({
				type: "GET",
				url: action,
				data: $(this).serialize(),
                success: function(result, textStatus, xhr) {
                    $('.search-results').html(result);
	    			$('.spinner').removeClass('spin');

                },
              	error: function(xhr, textStatus, errorThrown) {
              		console.log(errorThrown);
                    $('.search-results').html(xhr.status);
	    			$('.spinner').removeClass('spin');

                  }
			}).done(function( response ) {
	    			$('.spinner').removeClass('spin');
				    }
				);
	 });
	 
	 
	 $(document).on('submit', '.ajax-load-more-hotels', function(e) {
	 	e.preventDefault();
	    $('.spinner').addClass('spin');
	 	var action = $(this).attr('action');
	 		$.ajax({
				type: "GET",
				url: action,
				data: $(this).serialize(),
                success: function(result, textStatus, xhr) {
                    $('.search-results').html(result);
	    			$('.spinner').removeClass('spin');

                },
              	error: function(xhr, textStatus, errorThrown) {
              		console.log(errorThrown);
                    $('.search-results').html(errorThrown);
	    			$('.spinner').removeClass('spin');

                  }
			}).done(function( response ) {
	    			$('.spinner').removeClass('spin');
				    }
				);
	 });

	 $(document).on('click', '#clear-return-date', function(e) {
	    $('#flightsearchform-return').datepicker( "setDate", null );
	 });

});

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41429372-5', 'auto');
  ga('send', 'pageview');
