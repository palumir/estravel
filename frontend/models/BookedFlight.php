<?php

namespace frontend\models;

use Yii;
use yii\helpers\Json;
use yii\behaviors\BlameableBehavior;
use frontend\models\Booking;
use frontend\models\Event;
use common\models\Airport;
use common\models\User;
use frontend\models\Passenger;

/**
 * This is the model class for table "booked_flight".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $order_number
 * @property integer $status
 * @property string $status_text
 * @property string $to
 * @property string $from
 * @property string $total_fare
 * @property string $base_fare
 * @property string $taxes
 * @property string $departure_datetime
 * @property string $return_datetime
 * @property integer $depart_num_connections
 * @property integer $return_num_connections
 * @property string $depart_flight_segments
 * @property string $return_flight_segments
 * @property string $record_locators
 * @property string $created_at
 * @property string $updated_at
 * @property integer $deleted
 *
 * @property User $user
 * @property Passenger[] $passengers
 */
class BookedFlight extends \common\models\EstData
{

    public $_departFlightSegments;
    public $_returnFlightSegments;

    public function __construct($data = null, $config = [])
    {
        // if we pass data that means we're building the flight model from API data
        // as opposed to retrieving it from the database or something
        if ($data !== null) {
            // $this->user_id = Yii::$app->user->id;
            $this->order_number = (string)$data['ordernumber'];
            $this->status = $data['status'];
            $this->status_text = $data['statustext'];
            $this->to = $data['flight']['to'];
            $this->from = $data['flight']['from'];
            $this->total_fare = (double)str_replace("$", "", $data['flight']['total_fare']);
            $this->base_fare = (double)str_replace("$", "", $data['flight']['base_fare']);
            $this->taxes = (double)str_replace("$", "", $data['flight']['taxes']);
            $this->departure_datetime = $data['flight']['departure_datetime'];
            $this->return_datetime = $data['flight']['return_datetime'];
            $this->depart_num_connections = $data['flight']['depart_num_connections'];
            $this->return_num_connections = $data['flight']['return_num_connections'];
            $this->depart_flight_segments = Json::encode($data['flight']['depart_flight_segments']);
            $this->return_flight_segments = Json::encode($data['flight']['return_flight_segments']);
            $this->record_locators = Json::encode($data['RecordLocators']);
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booked_flight';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \common\models\EstData::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \common\models\EstData::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blame' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_number', 'status', 'to', 'from', 'departure_datetime', 'record_locators'], 'required'],
            [['user_id', 'status', 'depart_num_connections', 'return_num_connections', 'deleted'], 'integer'],
            [['total_fare', 'base_fare', 'taxes'], 'number'],
            [['depart_flight_segments', 'return_flight_segments', 'record_locators'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['order_number', 'status_text', 'departure_datetime', 'return_datetime'], 'string', 'max' => 255],
            [['to', 'from'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'order_number' => 'Order Number',
            'status' => 'Status',
            'status_text' => 'Status Text',
            'to' => 'To',
            'from' => 'From',
            'total_fare' => 'Total Fare',
            'base_fare' => 'Base Fare',
            'taxes' => 'Taxes',
            'departure_datetime' => 'Departure Datetime',
            'return_datetime' => 'Return Datetime',
            'depart_num_connections' => 'Depart Num Connections',
            'return_num_connections' => 'Return Num Connections',
            'depart_flight_segments' => 'Depart Flight Segments',
            'return_flight_segments' => 'Return Flight Segments',
            'record_locators' => 'Record Locators',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted' => 'Deleted',
        ];
    }

    public function getFromAirport()
    {
        return $this->hasOne(Airport::className(), ['iata_code' => 'from']);
    }

    public function getToAirport()
    {
        return $this->hasOne(Airport::className(), ['iata_code' => 'to']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooking()
    {
        return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrigin()
    {
        return $this->hasOne(Airport::className(), ['iata_code' => 'from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Airport::className(), ['iata_code' => 'to']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassengers()
    {
        return $this->hasMany(Passenger::className(), ['booked_flight_id' => 'id']);
    }

    public function afterFind()
    {
            $this->_departFlightSegments = Json::decode($this->depart_flight_segments);
            $this->_returnFlightSegments = Json::decode($this->return_flight_segments);
        if (parent::afterFind()) {
            return true;
        } else {
            return false;
        }
    }

}
