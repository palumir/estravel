<?php

namespace frontend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use common\models\User;

/**
 * This is the model class for table "ticket_comment".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $ticket_id
 * @property string $title
 * @property string $body
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Ticket $ticket
 */
class TicketComment extends \common\models\EstData
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_comment';
    }


    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \common\models\EstData::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \common\models\EstData::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blame' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body'], 'required'],
            [['id', 'user_id', 'ticket_id'], 'integer'],
            ['ticket_user_id', 'default', 'value' => NULL],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['body'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'ticket_id' => 'Ticket ID',
            'ticket_user_id' => 'Ticket User ID',
            'title' => 'Title',
            'body' => 'Body',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicket()
    {
        return $this->hasOne(Ticket::className(), ['id' => 'ticket_id', 'user_id' => 'ticket_user_id']);
    }
}
