<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Event;
use yii\data\Sort;

/**
 * EventSearch represents the model behind the search form about `frontend\models\Event`.
 */
class EventSearch extends Event
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['name', 'start_time', 'end_time', 'created_at', 'updated_at', 'city', 'address_line1', 'address_line2', 'zipcode', 'country', 'game.name', 'game.id'], 'safe'],
        ];
    }

    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['game.name', 'game.id']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Event::find()->where("start_time > NOW()");

        $query->joinWith(['games' => function($query) { $query->from('game'); }]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            // 'id' => $this->id,
            'game.id' => $this->getAttribute('game.id'),
            // 'start_time' => $this->start_time,
            // 'end_time' => $this->end_time,
            // 'created_at' => $this->created_at,
            // 'updated_at' => $this->updated_at,
            // 'created_by' => $this->created_by,
            'event.deleted' => $this::NOT_DELETED,
            'event.archived' => $this::NOT_ARCHIVED,
        ]);

        $query->andFilterCompare('status', '< 3');

        $query->andFilterWhere(['like', 'event.name', $this->name])
            ->andFilterWhere(['like', 'event.city', $this->city]);
			
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['start_time'=>SORT_ASC]],
        ]);

        return $dataProvider;
    }
}
