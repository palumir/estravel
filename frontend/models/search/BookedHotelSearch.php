<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\BookedHotel;

/**
 * BookedHotelSearch represents the model behind the search form about `frontend\models\BookedHotel`.
 */
class BookedHotelSearch extends BookedHotel
{

    public $booking_id;

    public function __construct($booking_id)
    {
        $this->booking_id = $booking_id;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'booking_id', 'event_id', 'status', 'deleted'], 'integer'],
            [['order_number', 'status_text', 'checkin', 'checkout', 'cancellation', 'hotelCode', 'hotelChain', 'hotelName', 'address_line_1', 'address_line_2', 'phone_number', 'description', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BookedHotel::find()->where(['user_id' => Yii::$app->user->id, 'booking_id' => $this->booking_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'user_id' => $this->user_id,
        //     'booking_id' => $this->booking_id,
        //     'event_id' => $this->event_id,
        //     'status' => $this->status,
        //     'checkin' => $this->checkin,
        //     'checkout' => $this->checkout,
        // ]);

        // $query->andFilterWhere(['like', 'order_number', $this->order_number])
        //     ->andFilterWhere(['like', 'status_text', $this->status_text])
        //     ->andFilterWhere(['like', 'cancellation', $this->cancellation])
        //     ->andFilterWhere(['like', 'hotelCode', $this->hotelCode])
        //     ->andFilterWhere(['like', 'hotelChain', $this->hotelChain])
        //     ->andFilterWhere(['like', 'hotelName', $this->hotelName])
        //     ->andFilterWhere(['like', 'address_line_1', $this->address_line_1])
        //     ->andFilterWhere(['like', 'address_line_2', $this->address_line_2])
        //     ->andFilterWhere(['like', 'phone_number', $this->phone_number])
        //     ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
