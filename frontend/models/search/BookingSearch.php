<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use frontend\models\Booking;

/**
 * BookingSearch represents the model behind the search form about `frontend\models\Booking`.
 */
class BookingSearch extends Booking
{

    public $event;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'event_id'], 'integer'],
            [['event', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *

     * @return ActiveDataProvider
     */
    public function search($params, $confirmed = false)
    {
		// Set confirmed to be an number for SQL
		if($confirmed) $confirmed = 1;
		else $confirmed = 0;
		
		if(!Yii::$app->user->isGuest) {
			$query = Booking::find()->where(['user_id' => Yii::$app->user->id])->orWhere(['session_id' => Yii::$app->session['booking_session_id']['value']])->andFilterWhere(['confirmed' => $confirmed]);
		}
		else {
			$query = Booking::find()->where(['session_id' => Yii::$app->session['booking_session_id']['value']])->andFilterWhere(['confirmed' => $confirmed]);
		}
		
        $query->joinWith(['event']);

        $sort = new Sort([
            'defaultOrder' => [
                'created_at' => SORT_DESC
            ],
            'attributes' => [
                // 'name' => [
                //     'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
                // ],
            ],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => $sort,

        ]);

        $dataProvider->sort->attributes['event'] = [
                'asc' => ['event.name' => SORT_ASC],
                'desc' => ['event.name' => SORT_DESC],
            ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'event_id' => $this->event_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ])->andFilterWhere(['like', 'event.name', $this->event]);

        return $dataProvider;
    }
}
