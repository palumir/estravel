<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Event;
use yii\db\Query;
use frontend\models\Carousel;
use yii\helpers\Url;

/**
 * CarouselSearch represents the model behind the search form about `frontend\models\Carousel`.
 */
class CarouselSearch extends Carousel
{

    public static $carousels = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rank'], 'integer'],
            [['title', 'image', 'summary', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
	
	public function getEventCarousels() {
		// Search for carousel images ... where rank is not null? Whatever that is.
        $carouselQuery = Carousel::find()->where("EXISTS (SELECT 1 FROM event WHERE (event.id = carousel.event_id AND event.start_time > NOW()))");
        $carouselDataProvider = new ActiveDataProvider([
            'query' => $carouselQuery,
            'sort' => [
                'defaultOrder' => [
                    'rank' => SORT_DESC,
                ]
            ],
        ]);
		
		// Search for the events that haven't ended.
		$eventQuery = Event::find()->where('event.start_time > NOW()')->where("EXISTS (SELECT 1 FROM carousel WHERE (event.id = carousel.event_id))");
        $eventDataProvider = new ActiveDataProvider([
            'query' => $eventQuery,
            'sort' => [
                'defaultOrder' => [
                    'start_time' => SORT_ASC,
                ]
            ],
        ]);
		
		// Sort the carousel query to be the same order as the event query, then return.
		$dataProvider =  new ActiveDataProvider(['query'=>new Query()]);
		$newArray = array();
		foreach($eventDataProvider->getModels() as $event) {
			
			// Find it within the carousel array.
			foreach($carouselDataProvider->getModels() as $carousel) {
				if($event->id == $carousel->event_id) {
					$newArray[] = $carousel;
					break;
				}
			}
		}
		$dataProvider->setModels($newArray);
		return $dataProvider;
	}

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
		$dataProvider = $this->getEventCarousels();
        return $dataProvider;
    }

    public function returnCarousels()
    {
        if (is_null(self::$carousels)) {
			
			$dataProvider = $this->getEventCarousels();

            foreach ($dataProvider->getModels() as $model) {
                self::$carousels[] = 
                    [
                        // required, slide content (HTML), such as an image tag
                        'content' => ($model->event_id)?'<a href="'.Url::to(['event/view', 'id' => $model->event_id]).'"><img src="'.$model->image.'"/></a>':'<img src="'.$model->image.'"/>',
                        // optional, the caption (HTML) of the slide
                        'caption' => ($model->event_id)?'<h4>'.$model->title." on ".Yii::$app->formatter->asDate($model->event->start_time, "long").'</h4><p>'.$model->summary.'</p>':'<h4>'.$model->title.'</h4><p>'.$model->summary.'</p>',
                        // optional the HTML attributes of the slide container
                        'options' => [],
                    ];
            }

        }

        return self::$carousels;
    }
}
