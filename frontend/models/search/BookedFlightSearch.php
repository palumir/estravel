<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\BookedFlight;

/**
 * BookedFlightSearch represents the model behind the search form about `frontend\models\BookedFlight`.
 */
class BookedFlightSearch extends BookedFlight
{

    public $booking_id;

    public function __construct($booking_id)
    {
        $this->booking_id = $booking_id;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'status', 'depart_num_connections', 'return_num_connections', 'deleted'], 'integer'],
            [['order_number', 'status_text', 'to', 'from', 'departure_datetime', 'return_datetime', 'depart_flight_segments', 'return_flight_segments', 'record_locators', 'created_at', 'updated_at'], 'safe'],
            [['total_fare', 'base_fare', 'taxes'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BookedFlight::find()->where(['user_id' => Yii::$app->user->id, 'booking_id' => $this->booking_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // $query->andFilterWhere([
        //     'status' => $this->status,
        //     'total_fare' => $this->total_fare,
        //     'base_fare' => $this->base_fare,
        //     'taxes' => $this->taxes,
        //     'depart_num_connections' => $this->depart_num_connections,
        //     'return_num_connections' => $this->return_num_connections,
        //     'created_at' => $this->created_at,
        //     'updated_at' => $this->updated_at,
        //     'deleted' => $this->deleted,
        // ]);

        // $query->andFilterWhere(['like', 'order_number', $this->order_number])
        //     ->andFilterWhere(['like', 'status_text', $this->status_text])
        //     ->andFilterWhere(['like', 'to', $this->to])
        //     ->andFilterWhere(['like', 'from', $this->from])
        //     ->andFilterWhere(['like', 'departure_datetime', $this->departure_datetime])
        //     ->andFilterWhere(['like', 'return_datetime', $this->return_datetime]);

        return $dataProvider;
    }
}
