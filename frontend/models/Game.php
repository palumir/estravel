<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "game".
 *
 * @property integer $id
 * @property string $name
 * @property string $version
 * @property string $created_at
 * @property string $updated_at
 *
 * @property EventGame[] $eventGames
 */
class Game extends \common\models\EstData
{

    public static $_drop_items;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['version'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'version' => 'Version',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventGames()
    {
        return $this->hasMany(EventGame::className(), ['game_id' => 'id']);
    }

    public static function getDropDownArray()
    {
        if (!isset(self::$_drop_items)) {
            self::$_drop_items[""] = "All Games";
            $items = self::find()->select('id, name')->asArray()->all();
            foreach ($items as $key => $value) {
                self::$_drop_items[$value['id']] = $value['name'];
            }
        }
        return self::$_drop_items;
    } 

}
