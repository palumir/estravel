<?php

namespace frontend\models;

use Yii;
use common\models\Country;
use common\models\Airport;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property string $start_time
 * @property string $end_time
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $city
 * @property string $address_line1
 * @property string $address_line2
 * @property string $zipcode
 * @property string $country
 *
 * @property Booking[] $bookings
 * @property User $createdBy
 * @property User $updatedBy
 * @property EventComment[] $eventComments
 * @property EventsAirports[] $eventsAirports
 */
class Event extends \common\models\EstData
{

    const STATUS_SAVED = 1;
    const STATUS_UPCOMING = 2;
    const STATUS_ONGOING = 3;
    const STATUS_FINISHED = 4;

    const SAVED = "Saved";
    const UPCOMING = "Upcoming";
    const ONGOING = "Ongoing";
    const FINISHED = "Finished";

    public $gamesString = '';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'created_by', 'updated_by', 'city', 'address_line1', 'airport_id'], 'required'],
            [['id', 'status', 'created_by', 'updated_by', 'deleted', 'archived', 'airport_id'], 'integer'],
            [['start_time', 'end_time', 'created_at', 'updated_at', 'default_departure_date', 'default_return_date', ], 'safe'],
            [['name', 'city', 'address_line1', 'address_line2', 'zipcode', 'country', 'venue'], 'string', 'max' => 255],
            [['logo', 'website', 'facebook', 'background', 'image', 'thumbnail'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'intro' => 'Intro',
            'logo' => 'Logo',
            'website' => 'Website',
            'facebook' => 'Facebook',
            'background' => 'Background',
            'image' => 'Image',
            'thumbnail' => 'Thumbnail',
            'summary' => 'Summary',
            'description' => 'Description',
            'status' => 'Status',
            'timezone' => 'Timezone',
            'airport_id' => 'Default Destination Airport',
            'venue' => 'Venue',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'default_departure_date' => 'Default Departure Date',
            'default_return_date' => 'Default Return Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'city' => 'City',
            'address_line1' => 'Address Line1',
            'address_line2' => 'Address Line2',
            'zipcode' => 'Zipcode',
            'country' => 'Country',
            'deleted' => 'Deleted',
            'archived' => 'Archived',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventComments()
    {
        return $this->hasMany(EventComment::className(), ['event_id' => 'id']);
    }


    public function getEventGames()
    {
        return $this->hasMany(EventGame::className(), ['event_id' => 'id']);
    }

    public function getGames()
    {
        return $this->hasMany(Game::className(), ['id' => 'game_id'])
            ->via('eventGames');
    }


    public function lineUpGames()
    {
        $string = "";
        $games = $this->hasMany(Game::className(), ['id' => 'game_id'])
            ->via('eventGames')->all();
        foreach ($games as $game) {
            $string .= $game->name . ", ";
        }
        return $string;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAirport()
    {
        return $this->hasOne(Airport::className(), ['id' => 'airport_id']);
    }

    public function getStatusString()
    {
        switch ($this->status) {
            case self::STATUS_SAVED;
                return self::SAVED;
                break;
            case self::STATUS_UPCOMING;
                return self::UPCOMING;
                break;
            case self::STATUS_ONGOING;
                return self::ONGOING;
                break;
            case self::STATUS_FINISHED;
                return self::FINISHED;
                break;
            default:
                return false;
                break;
        }
    }

    public function afterFind()
    {
        foreach ($this->games as $game) {
            $this->gamesString .= $game->name . ", ";
        }

        if (parent::afterFind()) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }

    public function afterValidate()
    {
        if (parent::afterValidate()) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (parent::afterSave($insert, $changedAttributes)) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }

    public function afterDelete()
    {
        if (parent::afterDelete()) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }


}
