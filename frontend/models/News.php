<?php

namespace frontend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $admin_id
 * @property string $title
 * @property string $body
 * @property string $tags
 * @property integer $published
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $admin
 * @property NewsComment[] $newsComments
 */
class News extends \common\models\EstData
{

    const STATUS_SAVED = 1;
    const STATUS_PUBLISHED = 2;

    const SAVED = "Saved";
    const PUBLISHED = "Published";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'admin_id', 'title', 'body'], 'required'],
            [['id', 'admin_id', 'published'], 'integer'],
            [['body'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'tags'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'admin_id' => 'Admin ID',
            'title' => 'Title',
            'body' => 'Body',
            'tags' => 'Tags',
            'published' => 'Published',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(User::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsComments()
    {
        return $this->hasMany(NewsComment::className(), ['news_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function countNewsComments()
    {
        return $this->hasMany(NewsComment::className(), ['news_id' => 'id'])->count();
    }
}
