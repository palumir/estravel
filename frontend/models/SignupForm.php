<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $firstname;
    public $lastname;
    public $phone;
    public $verifyCode;
    public $default_origin_airport;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['firstname', 'string', 'max' => 90],

            ['lastname', 'string', 'max' => 90],

            ['phone', 'string', 'max' => 90],

            ['default_origin_airport', 'string', 'max' => 3],

            [['password', 'password_repeat'], 'required'],
            ['password', 'compare'],
            [['password', 'password_repeat'], 'string', 'min' => 6],

            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
            'password_repeat' => 'Repeat Password',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
		
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->firstname = $this->firstname;
            $user->lastname = $this->lastname;
            $user->phone = $this->phone;
            // $user->default_origin_airport = $this->default_origin_airport;
            $user->setPassword($this->password);
            $user->generateAuthKey();
			
            if ($user->save()) {
                return $user;
            }
        }
		

        return null;
    }
}
