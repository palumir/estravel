<?php

namespace frontend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "passenger".
 *
 * @property integer $id
 * @property integer $booked_flight_id
 * @property integer $user_id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $date_of_birth
 * @property string $area_code
 * @property string $country_code
 * @property string $full_phone_number
 * @property string $number
 * @property string $email
 * @property string $gender
 *
 * @property BookedFlight $bookedFlight
 * @property User $user
 */
class Passenger extends \common\models\EstData
{

    const TYPE_ADULT = "adult";
    const TYPE_CHILD = "child";
    const TYPE_INFANT = "infant";

    const SCENARIO_BOOK = 'book';
    const SCENARIO_SAVE = 'save';

    public $index;
    public $type;

    public function __construct($data = false, $config = [])
    {
        // if we pass data that means we're building the passenger model from API data
        // as opposed to retrieving it from the database or something
        if ($data !== false) {
			
			// If our data is from the (new and improved) flight/hotel dual booking.
			if(isset($data["first_name"])) {
				$this->first_name = $data['first_name'];
				$this->middle_name = $data['middle_name'];
				$this->last_name = $data['last_name'];
				$this->date_of_birth = $data['date_of_birth'];
				//$this->area_code = $data['AreaCode'];
				//$this->country_code = $data['CountryCode'];
				//$this->full_phone_number = $data['FullPhoneNumber'];
				$this->number = $data['number'];
				$this->email = $data['email']; // API returns 2 emails, email and Email... ?!
				$this->gender = $data['gender'];
			}
			
			else {
				$this->first_name = $data['FirstName'];
				$this->middle_name = $data['MiddleName'];
				$this->last_name = $data['LastName'];
				$this->date_of_birth = $data['DOB'];
				$this->area_code = $data['AreaCode'];
				$this->country_code = $data['CountryCode'];
				$this->full_phone_number = $data['FullPhoneNumber'];
				$this->number = $data['Number'];
				$this->email = $data['Email']; // API returns 2 emails, email and Email... ?!
				$this->gender = $data['Gender'];
			}
            if ($user = User::findByEmail($this->email)) {
                $this->user_id = $user->id;
            }
        }
        parent::__construct($config);
    }

    public function scenarios()
    {

        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_BOOK] = [
            'first_name', 
            'middle_name', 
            'last_name', 
            'date_of_birth', 
            'gender', 
            'type', 
            'email',
            'number',
            ];
        $scenarios[self::SCENARIO_SAVE] = [
            'booked_flight_id', 
            'user_id', 
            'first_name', 
            'middle_name', 
            'last_name', 
            'date_of_birth', 
            'gender', 
            'email',
            'number',
            'full_phone_number',
            ];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'passenger';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'date_of_birth', 'gender', 'email', 'number'], 'required', 'on' => 'book'],
            [['first_name', 'last_name', 'date_of_birth'], 'required', 'on' => 'save'],
            [['booked_flight_id', 'user_id', 'index'], 'integer'],
            [['date_of_birth'], 'safe'],
            [['email'], 'email'],
            [['gender'], 'in', 'range' => ['M', 'F']],
            [['first_name', 'middle_name', 'last_name', 'area_code', 'country_code', 'full_phone_number', 'number', 'email'], 'string', 'max' => 255],
            [['gender'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'booked_flight_id' => 'Booked Flight ID',
            'user_id' => 'User ID',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'type' => 'Passenger Type',
            'date_of_birth' => 'Date Of Birth',
            'area_code' => 'Area Code',
            'country_code' => 'Country Code',
            'full_phone_number' => 'Full Phone Number',
            'number' => 'Number',
            'email' => 'Email',
            'gender' => 'Gender',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookedFlight()
    {
        return $this->hasOne(BookedFlight::className(), ['id' => 'booked_flight_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
