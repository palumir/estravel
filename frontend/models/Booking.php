<?php

namespace frontend\models;

use Yii;
use common\models\User;
use frontend\models\BookedFlight;
use frontend\models\BookedHotel;

/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $event_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $session_id
 * @property string $saved_hotel_rate
 * @property string $saved_hotel_model
 * @property string $saved_flight_model
 * @property string $saved_guest
 * @property boolean $confirmed
 *
 * @property User $user
 * @property Event $event
 * @property BookingComment[] $bookingComments
 * @property Flight[] $flights
 * @property Hotel[] $hotels
 * @property Party[] $parties
 */

// Booking is a model in itself because eventually the site might want to charge users per booked event
// which would make it a separate payment as opposed to booking flights or hotels for an event
// There can be only 1 booking per 1 event for each user.
// This makes it convenient to get things like booked flights and booked hotels as well
class Booking extends \common\models\EstData
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \common\models\EstData::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \common\models\EstData::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['session_id'], 'string'],
            [['user_id', 'event_id'], 'integer'],
            [['id', 'user_id', 'event_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id', 'event_id', 'session_id'], 'unique', 'targetAttribute' => ['user_id', 'event_id', 'session_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Your unique booking ID',
			'session_id' => 'Session ID',
            'user_id' => 'User ID',
            'event_id' => 'Event ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    public function hasBookedFlights()
    {
        return ($this->bookedFlights)?true:false;
    }

    public function hasBookedHotels()
    {
        return ($this->bookedHotels)?true:false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookedFlights()
    {
        return $this->hasMany(BookedFlight::className(), ['booking_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookedHotels()
    {
        return $this->hasMany(BookedHotel::className(), ['booking_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassengers()
    {
        // relation though a 3rd table, will add later
        // return $this->hasMany(Party::className(), ['booking_id' => 'id']);
    }
}
