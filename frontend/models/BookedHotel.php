<?php

namespace frontend\models;

use Yii;
use yii\helpers\Json;
use yii\behaviors\BlameableBehavior;
use frontend\models\Booking;
use frontend\models\Event;
use common\models\User;
use frontend\models\Guest;

/**
 * This is the model class for table "booked_hotel".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $booking_id
 * @property integer $event_id
 * @property string $order_number
 * @property integer $status
 * @property string $status_text
 * @property string $checkin
 * @property string $checkout
 * @property string $cancellation
 * @property string $hotelCode
 * @property string $hotelChain
 * @property string $hotelName
 * @property string $address_line_1
 * @property string $address_line_2
 * @property string $phone_number
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property integer $deleted
 *
 * @property Booking $booking
 * @property Event $event
 * @property User $user
 * @property Guest[] $guests
 */
class BookedHotel extends \common\models\EstData
{
    public function __construct($data = null, $config = [])
    {
        // if we pass data that means we're building the flight model from API data
        // as opposed to retrieving it from the database or something
        if ($data !== null) {
            // $this->user_id = Yii::$app->user->id;
            $this->order_number = (string)$data['ordernumber'];
            $this->status = $data['status'];
            $this->status_text = $data['statustext'];
            $this->checkin = $data['checkin'];
            $this->checkout = $data['checkout'];

            $this->cancellation = $data['cancellation'];
            $this->record_locators = Json::encode($data['RecordLocators']);

            $this->hotelCode = $data['HotelDetails']['hotelCode'];
            $this->hotelChain = $data['HotelDetails']['hotelChain'];
            $this->hotelName = $data['HotelDetails']['hotelName'];

            $this->address_line_1 = $data['HotelDetails']['address'][0];
            $this->address_line_2 = $data['HotelDetails']['address'][1];

            $this->phone_number = $data['HotelDetails']['phoneNumber'];
            $this->description = $data['HotelDetails']['description'];

        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booked_hotel';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \common\models\EstData::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \common\models\EstData::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blame' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_number', 'status', 'checkin', 'checkout', 'hotelCode', 'hotelChain', 'hotelName', 'address_line_1', 'record_locators'], 'required'],
            [['user_id', 'booking_id', 'event_id', 'status', 'deleted'], 'integer'],
            [['checkin', 'checkout', 'created_at', 'updated_at'], 'safe'],
            [['description', 'record_locators'], 'string'],
            [['order_number', 'status_text', 'cancellation', 'hotelCode', 'hotelChain', 'hotelName', 'address_line_1', 'address_line_2', 'phone_number'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'booking_id' => 'Booking ID',
            'event_id' => 'Event ID',
            'order_number' => 'Order Number',
            'status' => 'Status',
            'status_text' => 'Status Text',
            'checkin' => 'Checkin',
            'checkout' => 'Checkout',
            'cancellation' => 'Cancellation',
            'hotelCode' => 'Hotel Code',
            'hotelChain' => 'Hotel Chain',
            'hotelName' => 'Hotel Name',
            'address_line_1' => 'Address Line 1',
            'address_line_2' => 'Address Line 2',
            'phone_number' => 'Phone Number',
            'description' => 'Description',
            'record_locators' => 'Record Locators',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooking()
    {
        return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuests()
    {
        return $this->hasMany(Guest::className(), ['booked_hotel_id' => 'id']);
    }
}
