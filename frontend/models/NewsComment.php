<?php

namespace frontend\models;

use Yii;
use common\models\User;
use frontend\models\News;

/**
 * This is the model class for table "news_comment".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $news_id
 * @property string $title
 * @property string $body
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property News $news
 */
class NewsComment extends \common\models\EstData
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_comment';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \common\models\EstData::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \common\models\EstData::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'news_id', 'body'], 'required'],
            [['id', 'user_id', 'news_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['body'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'news_id' => 'News ID',
            'title' => 'Title',
            'body' => 'Body',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }
}
