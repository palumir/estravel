<?php
namespace frontend\models;

use Yii;
use common\models\User;
use yii\base\Model;

/**
 * Confirm deletion request form
 */
class ConfirmDeletionForm extends Model
{
    public $email;

    /**
     * @var \common\models\User
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'Sorry we could not find a user registered with this e-mail address.'
            ],
        ];
    }

    public function deleteUser($id)
    {
        $this->_user = User::findOne($id);

        if ($this->_user->email === Yii::$app->user->identity->email) {
            $this->_user->selfDelete();
            if ($this->_user->save(false)) {
                return true;
            } else {
                return false;
            }
        } else {
            throw new InvalidParamException('Logged in User and User e-mail do not match. Please contact an admin if you think this is a system error. Thank you.');
        }
    }

    // For when we'll want to confirm all deletions by email
    // /**
    //  * Sends an email with a link, for resetting the password.
    //  *
    //  * @return boolean whether the email was send
    //  */
    // public function sendEmail()
    // {
    //     /* @var $user User */
    //     $user = User::findOne([
    //         'status' => User::STATUS_ACTIVE,
    //         'email' => $this->email,
    //     ]);

    //     if ($user) {
    //         if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
    //             $user->generatePasswordResetToken();
    //         }

    //         if ($user->save()) {
    //             return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user])
    //                 ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
    //                 ->setTo($this->email)
    //                 ->setSubject('esTravel password reset for ' . \Yii::$app->name)
    //                 ->send();
    //         }
    //     }

    //     return false;
    // }
}
