<?php

namespace frontend\models;

use Yii;
use frontend\models\Event;

/**
 * This is the model class for table "carousel".
 *
 * @property integer $id
 * @property integer $rank
 * @property string $title
 * @property string $image
 * @property string $summary
 * @property string $description
 */
class Carousel extends \common\models\EstData
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carousel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rank'], 'integer'],
            [['title', 'image'], 'required'],
            [['title', 'image'], 'string', 'max' => 255],
            [['summary'], 'string', 'max' => 900],
            [['description'], 'string', 'max' => 5000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rank' => 'Rank',
            'title' => 'Title',
            'image' => 'Image',
            'summary' => 'Summary',
            'description' => 'Description',
        ];
    }

    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }
}
