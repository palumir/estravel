<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "event_game".
 *
 * @property integer $id
 * @property integer $event_id
 * @property integer $game_id
 *
 * @property Game $game
 * @property Event $event
 */
class EventGame extends \common\models\EstData
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_game';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'game_id'], 'required'],
            [['event_id', 'game_id'], 'integer'],
            [['event_id', 'game_id'], 'unique', 'targetAttribute' => ['event_id', 'game_id'], 'message' => 'The combination of Event ID and Game ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'game_id' => 'Game ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Game::className(), ['id' => 'game_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }
}
