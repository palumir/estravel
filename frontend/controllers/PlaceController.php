<?php

namespace frontend\controllers;

use Yii;
use common\models\Place;
use frontend\models\search\PlaceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class PlaceController extends Controller
{
    public function actionAjaxList($q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $results = $this->queryApi($q);
        foreach ($results as $key => $result) {
            $out['results'][] = ['id' => $result['description'], 'text' => $result['description']];
        }
        return $out;
    }

    protected function queryApi($text)
    {
        return Yii::$app->faregrabbr->autoCompletePlaces($text);
    }
}
