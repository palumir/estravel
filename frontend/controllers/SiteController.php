<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Event;
use frontend\models\FaregrabbrFlightForm;
use common\models\faregrabbr\HotelSearchForm;
use common\models\faregrabbr\FlightHotelSearchForm;
use frontend\models\search\CarouselSearch;
use frontend\models\search\EventSearch;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\adapters\FaregrabbrFlight;
use yii\helpers\Json;
use frontend\models\Article;
use frontend\models\News;
use frontend\models\search\ArticleSearch;
use frontend\models\search\NewsSearch;
use frontend\models\ArticleComment;
use frontend\models\search\ArticleCommentSearch;
use common\adapters\FaregrabbrHotelSearch;
use common\models\faregrabbr\HotelDetails;
use common\models\faregrabbr\HotelBookRequest;
use common\models\faregrabbr\Payment;
use common\models\faregrabbr\search\HotelSearch;
use common\models\faregrabbr\Passenger as FaregrabberPassenger;
use common\models\CreditCardForm;
use frontend\models\Guest;
use frontend\models\BookedHotel;
use frontend\models\Booking;
use yii\web\NotFoundHttpException;
use yii\data\Sort;
use yii\data\ArrayDataProvider;
use yii\web\GoneHttpException;
use common\adapters\FaregrabbrFlightSearch;
use common\models\faregrabbr\Flight;
use common\models\faregrabbr\FlightSearchForm;
use common\models\faregrabbr\search\FlightSearch;


/**
 * Site controller
 */
class SiteController extends Controller
{

    public static $carousels = null;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $articleSearchModel = new ArticleSearch();
        $newsSearchModel = new NewsSearch();
        $articleDataProvider = $articleSearchModel->search(Yii::$app->request->queryParams);
        $newsDataProvider = $newsSearchModel->search(Yii::$app->request->queryParams);
		$listOfEvents = Event::find()->where(['deleted' => Event::NOT_DELETED])->andWhere("start_time > NOW()")->orderBy('start_time')->all();

        $carouselSearchModel = new CarouselSearch();
        if (is_null(self::$carousels)) {
            self::$carousels = $carouselSearchModel->returnCarousels();
        }

        $nextEvent = $this->nextEvent();
		$hotelSearchForm = new HotelSearchForm();
		$flightSearchForm = new FlightSearchForm();
		$flightHotelSearchForm = new FlightHotelSearchForm();

        return $this->render('index', [
            'carousels' => self::$carousels,
            'articleSearchModel' => $articleSearchModel,
            'articleDataProvider' => $articleDataProvider,
            'newsSearchModel' => $newsSearchModel,
            'newsDataProvider' => $newsDataProvider,
			'listOfEvents' => $listOfEvents,
            'nextEvent' => $nextEvent,
			'hotelSearchForm' => $hotelSearchForm,
			'flightSearchForm' => $flightSearchForm,
			'flightHotelSearchForm' => $flightHotelSearchForm,
            ]);
    }

    public function actionEnter()
    {
        $signupForm = new SignupForm();
        $loginForm = new LoginForm();


        if ($signupForm->load(Yii::$app->request->post())) {
            if ($user = $signupForm->signup()) {
                if (Yii::$app->getUser()->login($user)) {
					$request = Yii::$app->request;
					$id = $request->get('booking_id');
					if($id != NULL) {
						return $this->redirect(['booking/view', 
						'id' => $id]);
					}
                    return $this->goHome();
                }
            }
        }

        if ($loginForm->load(Yii::$app->request->post()) && $loginForm->login()) {
            return $this->goBack();
        }

        return $this->render('enter', [
                'signupForm' => $signupForm,
                'loginForm' => $loginForm,
            ]);

    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
	
	    public function actionCorporate()
    {
          return $this->render('corporate');
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function nextEvent()
    {
        $event = Event::find()->where(['deleted' => Event::NOT_DELETED])->andWhere("start_time > NOW()")->orderBy('start_time')->one();
        return $event;
    }
}
