<?php

namespace frontend\controllers;

use Yii;
use common\adapters\FaregrabbrFlightSearch;
use common\models\faregrabbr\Flight;
use common\models\faregrabbr\Payment;
use common\models\faregrabbr\Passenger as FaregrabberPassenger;
use common\models\faregrabbr\FlightSearchForm;
use common\models\faregrabbr\search\FlightSearch;
use common\models\CreditCardForm;
use common\models\Airport;
use frontend\models\Passenger;
use frontend\models\Event;
use frontend\models\BookedFlight;
use frontend\models\BookedHotel;
use frontend\models\Booking;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\GoneHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\base\InvalidParamException;
use yii\helpers\Json;
use yii\data\Sort;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;

/**
 * FlightController implements the CRUD actions for Search model.
 */
class FlightController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['search', 'view', 'book'],
                'rules' => [
                    [
                        'actions' => ['search', 'view'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['search', 'view', 'book'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'action' => ['post'],
            //     ],
            // ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            // 'captcha' => [
            //     'class' => 'yii\captcha\CaptchaAction',
            //     'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            // ],
        ];
    }

    public function actionSearch($event_id = null, $default = null)
    {
		
        $flightSearchForm = new FlightSearchForm();
					

		// If we're doing both flight/hotel booking, we need to populate the fields
		// a little differently (FlightHotel instead of Flight array)
		$get = Yii::$app->request->queryParams;
		if(isset($get["FlightHotelSearchForm"])) {
			$getFlightHotel = $get["FlightHotelSearchForm"];
			$getFlight['departure'] = $getFlightHotel['departure'];
			$getFlight['return'] = $getFlightHotel['return'];
			$getFlight['class'] = $getFlightHotel['class'];
			$getFlight['adults'] = $getFlightHotel['adults'];
			$getFlight['children'] = $getFlightHotel['children'];
			$getFlight['infants'] = $getFlightHotel['infants'];
			$getFlight['origin'] = $getFlightHotel['origin'];
			$event_id = $get["event_id"];
		}
		
		// Always default to round-trip.
		$flightSearchForm->roundTrip = true;
		
		// Event?
		if(!is_null($event_id)) {
			$event = $this->findEvent($event_id);
            $flightSearchForm->origin = (!Yii::$app->user->isGuest && Yii::$app->user->identity->airport)?Yii::$app->user->identity->airport->iata_code:"";
            $flightSearchForm->destination = $event->airport->iata_code;
			$flightSearchForm->return = $event->default_return_date;
            $flightSearchForm->departure = $event->default_departure_date;		
		}

        // preload search data into the search form
        // if an event is selected and the user got to the page via a get request
        if (!is_null($event_id) && !is_null($default) && !$flightSearchForm->load(Yii::$app->request->get())) {
			
            // instantiate the search model and the provider with the prepopulated form
            $flightSearchModel = new FlightSearch($flightSearchForm);
			if(isset($getFlightHotel)) $flightDataProvider = $flightSearchModel->search($getFlightHotel);
            else $flightDataProvider = $flightSearchModel->search(Yii::$app->request->queryParams);
        } 

        // if the user posted his own new search parameters, load them and replace
        // both the search model and the provider
        else if ($flightSearchForm->load(Yii::$app->request->get()) && !is_null($default)) {
			
			// if it's numerical, it's an id in the database
			// and we need to return the the iata_code
			if (is_numeric($flightSearchForm->origin)) {
				$result = (new \yii\db\Query())
					->select(['iata_code'])
					->from('airport')
					->where('id=:id', [':id' => $flightSearchForm->origin])
					->one();
				$flightSearchForm->origin = $result['iata_code'];
			} 
			if (is_numeric($flightSearchForm->destination)) {
				$result = (new \yii\db\Query())
					->select(['iata_code'])
					->from('airport')
					->where('id=:id', [':id' => $flightSearchForm->destination])
					->one();
				$flightSearchForm->destination = $result['iata_code'];
			} 
            $flightSearchModel = new FlightSearch($flightSearchForm);
            $flightDataProvider = $flightSearchModel->search(Yii::$app->request->queryParams);
			
        }
		
		// If we are quicksearching
		else if(isset($getFlightHotel) && $getFlightHotel!=NULL) {
			$flightSearchForm = new FlightSearchForm($getFlight);
			$event = $this->findEvent($event_id);
            $flightSearchForm->destination = $event->airport->iata_code;
			$flightSearchForm->roundTrip = true;
			$flightSearchForm->validate();
			// if it's numerical, it's an id in the database
			// and we need to return the the iata_code
			if (is_numeric($flightSearchForm->origin)) {
				$result = (new \yii\db\Query())
					->select(['iata_code'])
					->from('airport')
					->where('id=:id', [':id' => $flightSearchForm->origin])
					->one();
				$flightSearchForm->origin = $result['iata_code'];
			} 
			if (is_numeric($flightSearchForm->destination)) {
				$result = (new \yii\db\Query())
					->select(['iata_code'])
					->from('airport')
					->where('id=:id', [':id' => $flightSearchForm->destination])
					->one();
				$flightSearchForm->destination = $result['iata_code'];
			} 
			$flightSearchModel = new FlightSearch($flightSearchForm);
			$flightDataProvider = $flightSearchModel->search(Yii::$app->request->queryParams);
					
			// Set hotel details.
			$cookies = Yii::$app->response->cookies;

			// add a new cookie to the response to be sent
			$cookies->add(new \yii\web\Cookie([
				'name' => 'getFlightHotel' + $event_id,
				'value' => json_encode($getFlightHotel),
			]));
			
		}
		
		else {
			// if it's numerical, it's an id in the database
			// and we need to return the the iata_code
			if (is_numeric($flightSearchForm->origin)) {
				$result = (new \yii\db\Query())
					->select(['iata_code'])
					->from('airport')
					->where('id=:id', [':id' => $flightSearchForm->origin])
					->one();
				$flightSearchForm->origin = $result['iata_code'];
			} 
			if (is_numeric($flightSearchForm->destination)) {
				$result = (new \yii\db\Query())
					->select(['iata_code'])
					->from('airport')
					->where('id=:id', [':id' => $flightSearchForm->destination])
					->one();
				$flightSearchForm->destination = $result['iata_code'];
			} 
            $flightSearchModel = new FlightSearch($flightSearchForm);
            $flightDataProvider = $flightSearchModel->search(Yii::$app->request->queryParams);
        }

        if (Yii::$app->request->isAjax) {
			return $this->renderPartial('search/_results', [
				'event_id' => $event_id,
				'flightSearchForm' => $flightSearchForm,
				'flightSearchModel' => $flightSearchModel,
				'flightDataProvider' => $flightDataProvider,
				'summary' => $flightSearchModel->summary,
				'airlines' => $flightSearchModel->airlines,
				]);
        }

        return $this->render('search/index', [
                'event_id' => $event_id,
                'flightSearchForm' => $flightSearchForm,
                'flightSearchModel' => $flightSearchModel,
                'flightDataProvider' => $flightDataProvider,
                'summary' => $flightSearchModel->summary,
                'airlines' => $flightSearchModel->airlines,
            ]);
    }

    // f-ing origin and destination, adults, children and infants need to be taken from the form, AGAIN, and passed in the query string
    // because the Faregrabber API doesn't return them with the searched flights, idiots
    // ridiculous!!
    // [later-edit] => eventually the search params were returned and added but after I already had to write this code
    // now i'm going to leave it as it is
    public function actionPreload($event_id = null, $origin, $destination, $departure_date, $adults = 1, $children = 0, $infants = 0, $getFlightHotel = NULL)
    {
        // this will take the array posted using Jquery from the html data-flight
        // to construct the flight model before being able to view it
        // to uniquely identify it the flight is constructed with a cache key
        // and then cached; the cache key is then used to retrieve the flight on the view page
        // with the idea of keeping controllers thin, this is all done in __construct()

        $flight = new Flight($event_id, $origin, $destination, $departure_date, $adults, $children, $infants, Yii::$app->request->get()['model']);
        $this->redirect(['flight/view', 'fK' => $flight->cacheKey]);
    }

    public function actionView($fK)
    {
        // get the flight model from cache
        $model = Yii::$app->cache->get($fK);
        if (!$model) {
            throw new GoneHttpException("The flight is no longer in cache. Please try another search.", 410);
        }

        $creditCardForm = new CreditCardForm();
        $passengers = [];

        $passenger = new Passenger(false, ['scenario' => Passenger::SCENARIO_BOOK]);

        if (!Yii::$app->user->isGuest) {
            // load the default params from the logged in user
            $passenger->first_name = Yii::$app->user->identity->firstname;
            $passenger->middle_name = Yii::$app->user->identity->middlename;
            $passenger->last_name = Yii::$app->user->identity->lastname;
            $passenger->type = Passenger::TYPE_ADULT;
            $passenger->date_of_birth = Yii::$app->user->identity->date_of_birth;
            $passenger->number = Yii::$app->user->identity->phone;
            $passenger->email = Yii::$app->user->identity->email;
            $passenger->gender = Yii::$app->user->identity->gender;            
        }

        $passengers[] = $passenger;

        // 1 because the first passenger is populated manually, above
        for ($i=1; $i < $model->adults; $i++) {
            $passenger = new Passenger(false, ['scenario' => Passenger::SCENARIO_BOOK]);
            $passenger->type = Passenger::TYPE_ADULT;
            $passengers[] = $passenger;
        }

        for ($i=0; $i < $model->children; $i++) {
            $passenger = new Passenger(false, ['scenario' => Passenger::SCENARIO_BOOK]);
            $passenger->type = Passenger::TYPE_CHILD;
            $passengers[] = $passenger;
        }

        for ($i=0; $i < $model->infants; $i++) {
            $passenger = new Passenger(false, ['scenario' => Passenger::SCENARIO_BOOK]);
            $passenger->type = Passenger::TYPE_INFANT;
            $passengers[] = $passenger;
        }
        
        if ($model->event_id) {
            $event = $this->findEvent($model->event_id);            
            return $this->render('view/eventFlight', [
                    'fK' => $fK,
                    'model' => $model,
                    'creditCardForm' => $creditCardForm,
                    'passengers' => $passengers,
                    'event' => $event,
                ]);
        } else {
            return $this->render('view/nonEventFlight', [
                    'fK' => $fK,
                    'model' => $model,
                    'creditCardForm' => $creditCardForm,
                    'passengers' => $passengers,
                ]);
        }
    }

    public function actionBook($fK)
    {
        // load the post params into models
        // here we take the user input and attept to book using the crappy Faregrabbr API
        // first, get the same flight from cache
        $model = Yii::$app->cache->get($fK);
        // instantiate an equal amount of empty passenger models
        $count = count(Yii::$app->request->post('Passenger', []));
        $passengers = [new Passenger(false, ['scenario' => Passenger::SCENARIO_BOOK])];
        for ($i=1; $i < $count; $i++) { 
            $passengers[] = new Passenger(false, ['scenario' => Passenger::SCENARIO_BOOK]);
        }
        // load multiple post passengers into site passenger models 
        // there are 2 types of passenger models, one for the website and the database, 
        // one for the Faregrabbr API because god knows what the API is going to look like in the future
        if (Passenger::loadMultiple($passengers, Yii::$app->request->post()) && Passenger::validateMultiple($passengers)) {
            // for each site passenger now create a faregrabbr passenger
            foreach ($passengers as $index => $passenger) {
                $faregrabberPassenger = new FaregrabberPassenger($index, $passenger);
                $faregrabberPassengers[] = $faregrabberPassenger;
            }
        }
        // load credit card details into the payment model
        $creditCardForm = new CreditCardForm();
        if ($creditCardForm->load(Yii::$app->request->post()) && $creditCardForm->validate()) {
            // load the CreditCardForm into a payment
            $payment = new Payment($creditCardForm);
        }
        // check if flight is attached to an event
        if ($model && $model->event_id) {
            $event = $this->findEvent($model->event_id);
        }

        // book the flight; if not false we got a response from the server
        if ($data = Yii::$app->faregrabbr->bookFlight($model, $payment, $faregrabberPassengers)) {
            // if status is true the booking was successful so we save the booking details to the database
            if ($data['status'] === true) {
                $bookedFlight = new BookedFlight($data); // pass the data to construct the model

                // connect the BookedFlight with the event and the booking if flight is to an event
                if ($event) {
                    $booking = Booking::find()->where(['user_id' => Yii::$app->user->id, 'event_id' => (int)$event->id ])->one();
                    $bookedFlight->event_id = $event->id;
                    if ($booking) {
                        $bookedFlight->booking_id = $booking->id;
                    }
                }

                if ($bookedFlight->validate() && $bookedFlight->save(false)) {
                    // use a foreach because we can't use loadMultiple
                    foreach ($data['passenger'] as $passengerData) {
                        $passenger = new Passenger($passengerData, ['scenario' => Passenger::SCENARIO_SAVE]);
                        if ($passenger->validate()) {
                            $passenger->link('bookedFlight', $bookedFlight);
                        }                  
                    }

                    // this will actually redirect the user to his booking on this event
                    Yii::$app->session->setFlash('success', 'Your flight was booked successfully. Please check your e-mail for further details.');
                    $this->redirect(['booking/create', 'event_id' => $event->id]);

                } else {
                    return $this->render('../site/error', [
                            'name' => "Local Server Error",
                            'message' => "Your flight was booked successfully but the flight data could not be saved locally. Please contact a website administrator. Your order number is: " . $data['ordernumber'],
                        ]);
                }
            } elseif ($data['status'] === false) {
                return $this->render('../site/error', [
                        'name' => "Booking Error",
                        'message' => $data['statustext'],
                    ]);
            } else {
            // some unforseen shit like the server didn't send back valid JSON as it happened several times in the past
                return $this->render('../site/error', [
                        'name' => "Unforseen Error",
                        'message' => "Could not determine the nature of the error, please contact a website administrator.",
                    ]);        
            }
        // else we did not get a response from the server
        } else {
            return $this->render('../site/error', [
                    'name' => "Booking Failed",
                    'message' => "Failed to reach the flight booking API server.",
                ]);        
        }
    }

    protected function findEvent($id)
    {
        if (($model = Event::findOne(['id' => $id, 'deleted' => \common\models\EstData::NOT_DELETED, 'archived' => \common\models\EstData::NOT_ARCHIVED])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionSearchRoundTrip($event_id = null, $default = null) {
		return $this->actionSearch($event_id,$default);
	}


}
