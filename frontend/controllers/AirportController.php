<?php

namespace frontend\controllers;

use Yii;
use common\models\Airport;
use frontend\models\search\AirportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AirportController implements the CRUD actions for Airport model.
 */
class AirportController extends Controller
{
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['post'],
    //             ],
    //         ],
    //     ];
    // }

        public function actionAjaxList($q = null, $iata_code = null)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $out = ['results' => ['id' => '', 'name' => '']];

            if (!is_null($q)) {
                $query = new \yii\db\Query();
                $query->select(['id, CONCAT(iata_code, " - ", name, ", ", city, ", ", country) AS name'])
                    ->from('airport')
                    ->where(['like', 'iata_code', $q])
                    ->orWhere(['like', 'name', $q])
                    ->orWhere(['like', 'city', $q])
					->orderBy(['INSTR(CONCAT(iata_code, \'' . $q . '\'), \'' . $q . '\')' => SORT_ASC, 
							   'INSTR(CONCAT(city, \'' . $q . '\'), \'' . $q . '\')' => SORT_ASC,
							   'INSTR(CONCAT(name, \'' . $q . '\'), \'' . $q . '\')' => SORT_ASC])
                    ->limit(20);
                $command = $query->createCommand();
                $data = $command->queryAll();
                $out['results'] = array_values($data);
            }
            elseif ($iata_code) {
                $out['results'] = ['iata_code' => $iata_code, 'name' => $this->findModel($iata_code)->name];
            }
            return $out;
        }

        // ended up not using this
        // public function actionAjaxFullname()
        // {
        //     if (Yii::$app->request->isAjax) {
        //         $iata_code = Yii::$app->request->post('iata_code');
        //         $airport = Airport::find()
        //                             // ->select('CONCAT(name, ", ", city, ", ", country, " - ", iata_code) AS name')
        //                             ->where("iata_code=:iata_code", ['iata_code' => $iata_code])
        //                             ->one();

        //         return $airport->name . ", " . $airport->city . ", " . $airport->country . " - " . $airport->iata_code;               
        //     }
        // }

    /**
     * Finds the Airport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $code
     * @return Airport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($code)
    {
        if ($model = Airport::findOne(['code' => $code])) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
