<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ArticleComment;
use frontend\models\search\ArticleCommentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArticleCommentController implements the CRUD actions for ArticleComment model.
 */
class ArticleCommentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionPost($article_id)
    {

        if (Yii::$app->request->isAjax) {
            if (!Yii::$app->user->isGuest) {
                $model = new ArticleComment();
                $model->load(Yii::$app->request->post());
                $model->user_id = Yii::$app->user->id;
                if ($model->save()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                Yii::$app->session->setFlash('error', 'Please log in before posting a comment. Thank you.');
                $this->goHome();
            }
        }

    }

    public function actionDelete($id, $user_id)
    {
        if (Yii::$app->request->isPjax) {
            if (Yii::$app->user->id !== (int)$user_id) {
                throw new UnauthorizedHttpException('You are not authorized to delete this comment.');
            } else {
                $comment = $this->findModel($id, $user_id);
                if (Yii::$app->user->id !== $comment->user_id) {
                    throw new UnauthorizedHttpException('You are not authorized to delete this comment.');
                } else {
                    $comment->delete();
                    return $this->renderAjax(['article/view', 'id' => $comment->article_id]);
                }
            }
        } else {
            if (Yii::$app->user->id !== (int)$user_id) {
                throw new UnauthorizedHttpException('You are not authorized to delete this comment.');
            } else {
                $comment = $this->findModel($id, $user_id);
                if (Yii::$app->user->id !== $comment->user_id) {
                    throw new UnauthorizedHttpException('You are not authorized to delete this comment.');
                } else {
                    if ($comment->delete()) {
                        return $this->redirect(['article/view', 'id' => $comment->article_id]);
                    } else {
                        return $this->redirect(['article/view', 'id' => $comment->article_id]);
                    }
                }
            }
        }
    }

    /**
     * Finds the ArticleComment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $user_id
     * @return ArticleComment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $user_id)
    {
        if (($model = ArticleComment::findOne(['id' => $id, 'user_id' => $user_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
