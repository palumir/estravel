<?php

namespace frontend\controllers;

use Yii;
use frontend\models\TicketComment;
use frontend\models\search\TicketCommentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TicketCommentController implements the CRUD actions for TicketComment model.
 */
class TicketCommentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Creates a new TicketComment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionPost($ticket_id)
    {

        if (Yii::$app->request->isAjax) {
            if (!Yii::$app->user->isGuest) {
                $model = new TicketComment();
                $model->load(Yii::$app->request->post());
                $model->user_id = Yii::$app->user->id;
                if ($model->save()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                Yii::$app->session->setFlash('error', 'Please log in before posting a comment. Thank you.');
                $this->goHome();
            }
        }

    }


    /**
     * Deletes an existing TicketComment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $user_id
     * @return mixed
     */
    public function actionDelete($id, $user_id)
    {
        if (Yii::$app->request->isPjax) {
            if (Yii::$app->user->id !== (int)$user_id) {
                throw new UnauthorizedHttpException('You are not authorized to delete this comment.');
            } else {
                $comment = $this->findModel($id, $user_id);
                if (Yii::$app->user->id !== $comment->user_id) {
                    throw new UnauthorizedHttpException('You are not authorized to delete this comment.');
                } else {
                    $comment->delete();
                    return $this->renderAjax(['ticket/view', 'id' => $comment->ticket_id]);
                }
            }
        } else {
            if (Yii::$app->user->id !== (int)$user_id) {
                throw new UnauthorizedHttpException('You are not authorized to delete this comment.');
            } else {
                $comment = $this->findModel($id, $user_id);
                if (Yii::$app->user->id !== $comment->user_id) {
                    throw new UnauthorizedHttpException('You are not authorized to delete this comment.');
                } else {
                    if ($comment->delete()) {
                        return $this->redirect(['ticket/view', 'id' => $comment->ticket_id]);
                    } else {
                        return $this->redirect(['ticket/view', 'id' => $comment->ticket_id]);
                    }
                }
            }
        }
    }


    /**
     * Finds the TicketComment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $user_id
     * @return TicketComment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $user_id)
    {
        if (($model = TicketComment::findOne(['id' => $id, 'user_id' => $user_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
