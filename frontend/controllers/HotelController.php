<?php

namespace frontend\controllers;

use Yii;
use common\adapters\FaregrabbrHotelSearch;
use common\models\faregrabbr\HotelSearchForm;
use common\models\faregrabbr\HotelDetails;
use common\models\faregrabbr\HotelBookRequest;
use common\models\faregrabbr\Payment;
use common\models\faregrabbr\search\HotelSearch;
use common\models\faregrabbr\Passenger as FaregrabberPassenger;
use common\models\CreditCardForm;
use frontend\models\Guest;
use frontend\models\Event;
use frontend\models\BookedHotel;
use frontend\models\Booking;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\web\UnauthorizedHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\InvalidParamException;
use yii\helpers\Json;
use yii\data\Sort;
use yii\data\ArrayDataProvider;
use yii\web\GoneHttpException;

/**
 * HotelController implements the CRUD actions for Search model.
 */
class HotelController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['search', 'details', 'book'],
                'rules' => [
                    [
                        'actions' => ['search'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['search', 'details', 'book'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'action' => ['post'],
            //     ],
            // ],
        ];
    }

    public function actionSearch($event_id = null, $default = null, $load_page = null)
    {
        // if (Yii::$app->user->isGuest) {
        //     $this->redirect(['site/login']);
        //     throw new UnauthorizedHttpException("You are required to log in before searching and booking hotels.");
        // }
		if($event_id != null) {
			$event = $this->findEvent($event_id);
			$eventAddress = $event->address_line1 . ", " . $event->address_line2 . ", " . $event->city . ", " . $event->zipcode;
			$eventAirport = $event->airport->iata_code;
		}
        $hotelSearchForm = new HotelSearchForm();
        // preload search data into the search form
        // if an event is selected and the user got to the page via a get request
        if (!is_null($event_id) && !is_null($default)) {
            $event = $this->findEvent($event_id);
            $hotelSearchForm->location = $eventAddress;
            $hotelSearchForm->checkin = $event->default_departure_date;
            $hotelSearchForm->numrooms = 1;
            $hotelSearchForm->guests = 1;
            $hotelSearchForm->checkout = $event->default_return_date;
			
			// If some of the details are in the post, set it.
			$get = Yii::$app->request->get();
			if(isset($get['numrooms'])) $hotelSearchForm->numrooms = $get['numrooms'];
			if(isset($get['checkin'])) $hotelSearchForm->checkin  = $get['checkin'];
			if(isset($get['guests'])) $hotelSearchForm->guests = $get['guests'];
			if(isset($get['checkout'])) $hotelSearchForm->checkout = $get['checkout'];

            // instantiate the search model and the provider with the prepopulated form
            $hotelSearchModel = new HotelSearch($hotelSearchForm, $load_page, $eventAddress, $eventAirport);

            // else instantiate HotelSearch without a param to show "No results found"
        } else {
            $hotelSearchModel = new HotelSearch(null, $load_page, $eventAddress, $eventAirport);
        }

        // if the user posted his own new search parameters, load them and replace
        // both the search model and the provider
        if ($hotelSearchForm->load(Yii::$app->request->get())) {
		
            $hotelSearchModel = new HotelSearch($hotelSearchForm, $load_page, $eventAddress, $eventAirport);
        }
		
		$hotelDataProvider = $hotelSearchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax) {
                return $this->renderPartial('search/_results', [
                    'event_id' => $event_id,
					'event' => $event,
                    'hotelSearchForm' => $hotelSearchForm,
                    'hotelSearchModel' => $hotelSearchModel,
                    'hotelDataProvider' => $hotelDataProvider,
                    'currency' => $hotelSearchModel->currency,
                    'status' => $hotelSearchModel->status,
					'next_page' => $hotelSearchModel->nextRequestReference
                    ]);
        }

        return $this->render('search/index', [
                'event_id' => $event_id,
				'event' => $event,
                'hotelSearchForm' => $hotelSearchForm,
                'hotelSearchModel' => $hotelSearchModel,
                'hotelDataProvider' => $hotelDataProvider,
                'currency' => $hotelSearchModel->currency,
                'status' => $hotelSearchModel->status,
				'next_page' => $hotelSearchModel->nextRequestReference
            ]);
    }

    public function actionPreload($event_id = null, $hotelCode, $hotelChain, $checkin, $numrooms, $guests, $checkout, $currencyCode = null)
    {
        $hotelDetails = new HotelDetails($event_id, $hotelCode, $hotelChain, $checkin, $numrooms, $guests, $checkout, null, [], $currencyCode);
		
        $this->redirect(['hotel/view', 'hK' => $hotelDetails->cacheKey]);
    }

    public function actionView($hK)
    {
        // get the hotel model from cache
        $model = Yii::$app->cache->get($hK);
        if (!$model) {
            throw new GoneHttpException("The hotel is no longer in cache. Please try another search.", 410);
        }

        $creditCardForm = new CreditCardForm();
        $guests = [];

        $guest = new Guest(false, ['scenario' => Guest::SCENARIO_BOOK]);
        // load the default params from the logged in user

        if (!Yii::$app->user->isGuest) {
            $guest->first_name = Yii::$app->user->identity->firstname;
            $guest->middle_name = Yii::$app->user->identity->middlename;
            $guest->last_name = Yii::$app->user->identity->lastname;
            $guest->type = Guest::TYPE_ADULT;
            $guest->date_of_birth = Yii::$app->user->identity->date_of_birth;
            $guest->number = Yii::$app->user->identity->phone;
            $guest->email = Yii::$app->user->identity->email;
            $guest->gender = Yii::$app->user->identity->gender;
        }

        $guests[] = $guest;

        // 1 because the first Guest is populated manually, above
        for ($i=1; $i < $model->guests; $i++) {
            $guest = new Guest(false, ['scenario' => Guest::SCENARIO_BOOK]);
            $guest->type = Guest::TYPE_ADULT;
            $guests[] = $guest;
        }
		
		if($model->hotelCode == null) {
			return $this->render('../site/error', [
				'name' => "Error Loading Hotel",
				'message' => "It is no longer be available or does not allow on-line booking for more than one room at a time. Please try another hotel.",
			]);
		}

        if ($model->event_id) {
            $event = $this->findEvent($model->event_id);            
            return $this->render('view/eventHotel', [
                    'hK' => $hK,
                    'model' => $model,
                    'creditCardForm' => $creditCardForm,
                    'guests' => $guests,
                    'event' => $event,
                ]);
        } else {
            return $this->render('view/nonEventHotel', [
                    'hK' => $hK,
                    'model' => $model,
                    'creditCardForm' => $creditCardForm,
                    'guests' => $guests,
                ]);
        }
    }

    public function actionBook($hK, $rate_plan)
    {
        // throw new NotFoundHttpException('The hotel booking system is temporarily offline.');

        // load the post params into models
        // here we take the user input and attept to book using the crappy Faregrabbr API
        // first, get the same hotel from cache
        $model = Yii::$app->cache->get($hK);
        // instantiate an equal amount of empty passenger models
        $count = count(Yii::$app->request->post('Guest', []));
        $guests = [new Guest(false, ['scenario' => Guest::SCENARIO_BOOK])];
        for ($i=1; $i < $count; $i++) { 
            $guests[] = new Guest(false, ['scenario' => Guest::SCENARIO_BOOK]);
        }
        // load multiple post guests into site passenger models 
        // there are 2 types of passenger models, one for the website and the database, 
        // one for the Faregrabbr API because god knows what the API is going to look like in the future
        if (Guest::loadMultiple($guests, Yii::$app->request->post()) && Guest::validateMultiple($guests)) {
            // for each site passenger now create a faregrabbr passenger; yes faregrabbr calls them passengers
            foreach ($guests as $index => $guest) {
                $faregrabberPassenger = new FaregrabberPassenger($index, $guest);
                $faregrabberPassengers[] = $faregrabberPassenger;
            }
        }
        // load credit card details into the payment model
        $creditCardForm = new CreditCardForm();
        if ($creditCardForm->load(Yii::$app->request->post()) && $creditCardForm->validate()) {
            // load the CreditCardForm into a payment
            $payment = new Payment($creditCardForm);
        }
        // check if hotel is attached to an event
        if ($model && $model->event_id) {
            $event = $this->findEvent($model->event_id);
        }

        // find the rate in the rates array
        foreach ($model->rates as $rateObject) {
            if ($rateObject->plan === $rate_plan) {
                // asign it
                $rate = $rateObject;
            }
        }

        $hotelBookRequest = new HotelBookRequest($model, $model->numrooms, $model->checkin, $model->checkout, $rate, $faregrabberPassengers, $payment);

        // book the hotel; if not false we got a response from the server
        if ($data = Yii::$app->faregrabbr->bookHotel($hotelBookRequest)) {
            // if status is true the booking was successful so we save the booking details to the database
            if ($data['status'] === true) {
                $bookedHotel = new BookedHotel($data); // pass the data to construct the model

                // connect the BookedHotel with the event and the booking if flight is to an event
                if ($event) {
                    $booking = Booking::find()->where(['user_id' => Yii::$app->user->id, 'event_id' => (int)$event->id ])->one();
                    $bookedHotel->event_id = $event->id;
                    if ($booking) {
                        $bookedHotel->booking_id = $booking->id;
                    }
                }

                if ($bookedHotel->validate() && $bookedHotel->save(false)) {
                    // use a foreach because we can't use loadMultiple
                    foreach ($data['Passengers'] as $guestData) {
                        $guest = new Guest($guestData, ['scenario' => Guest::SCENARIO_SAVE]);
                        if ($guest->validate()) {
                            $guest->link('bookedHotel', $bookedHotel);
                        }                  
                    }

                    // this will actually redirect the user to his booking on this event
                    $this->redirect(['booking/create', 'event_id' => $event->id]);

                } else {
                    return $this->render('../site/error', [
                            'name' => "Local Server Error",
                            'message' => "Your hotel was booked successfully but the hotel data could not be saved locally. Please contact a website administrator. Your order number is: " . $data['ordernumber'],
                        ]);
                }
            } elseif ($data['status'] === false) {
                return $this->render('../site/error', [
                        'name' => "Booking Error",
                        'message' => $data['statustext'],
                    ]);
            } else {
            // some unforseen shit like the server didn't send back valid JSON as it happened several times in the past
                return $this->render('../site/error', [
                        'name' => "Unforseen Error",
                        'message' => "Could not determine the nature of the error, please contact a website administrator.",
                    ]);        
            }
        // else we did not get a response from the server
        } else {
            return $this->render('../site/error', [
                    'name' => "Booking Failed",
                    'message' => "Failed to reach the hotel booking API server.",
                ]);        
        }    }

    protected function findEvent($id)
    {
        if (($model = Event::findOne(['id' => $id, 'deleted' => \common\models\EstData::NOT_DELETED, 'archived' => \common\models\EstData::NOT_ARCHIVED])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
