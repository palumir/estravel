<?php

namespace frontend\controllers;

use Yii;
use common\models\Country;
// use frontend\models\search\CountrySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class CountryController extends Controller
{
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['post'],
    //             ],
    //         ],
    //     ];
    // }

        public function actionAjaxList($q = null)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $out                         = ['results' => ['id' => '', 'name' => '']];

            if (!is_null($q)) {
                $query = new \yii\db\Query();
                $query->select('id, name')
                    ->from('country')
                    ->where(['like', 'name', $q])
                    ->limit(20);
                $command = $query->createCommand();
                $data = $command->queryAll();
                $out['results'] = array_values($data);
            }

            return $out;




            // $query = new \yii\db\Query();
            // $query->select('ap.code AS code, name AS name')
            //     ->from('country ap')
            //     ->where('ap.name LIKE "%' . $q . '%"')
            //     ->limit(20);
            // $command        = $query->createCommand();
            // $data           = $command->queryAll();
            // $out['results'] = array_values($data);
            // return $out;
        }

    /**
     * Finds the Country model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $code
     * @return Country the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($code)
    {
        if ($model = Country::findOne(['code' => $code])) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
