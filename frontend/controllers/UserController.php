<?php

namespace frontend\controllers;

use Yii;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\adapters\FaregrabbrTripSearch;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\ConfirmDeletionForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['post'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionAccount()
    {

        $model = Yii::$app->user->identity;

        return $this->render('account', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = Yii::$app->user->identity;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                return $this->redirect(['account']);
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to update your account. Please contact an admin if the problem persists. Thank you.');
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionTrips()
    {
        // feature not yet support by API
        Yii::$app->getSession()->setFlash('error', 'Feature not yet supported by our API.');
        return $this->goBack();
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->email === Yii::$app->user->identity->email) {
                if ($model->sendEmail()) {
                    Yii::$app->getSession()->setFlash('success', 'Please check your email for further instructions.');
                    return $this->goHome();
                } else {
                    Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
                }
            } else {
                    Yii::$app->getSession()->setFlash('error', 'Sorry, this is not the e-mail you are currently logged in with. Please check again or if you wish to change your e-mail please contact an admin by creating a new Support Ticket.');
            }

        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'Your password has been changed succesfully.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = new ConfirmDeletionForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->email === Yii::$app->user->identity->email) {
                if ($model->deleteUser(Yii::$app->user->id)) {
                    Yii::$app->user->logout();
                    Yii::$app->getSession()->setFlash('success', 'Your account has been deleted succesfully.');
                    return $this->goHome();
                } else {
                    Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to delete your account at this time. Please contact an admin by using our Support Ticket system. Thank you.');
                }
            } else {
                    Yii::$app->getSession()->setFlash('error', 'Sorry, this is not the e-mail you are currently logged in with. Please check again or if you wish to change your e-mail please contact an admin by creating a new Support Ticket.');
            }
        }

        return $this->render('confirmDeletion.php', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne(['id' => $id, 'deleted' => \common\models\EstData::NOT_DELETED, 'archived' => \common\models\EstData::NOT_ARCHIVED])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
