<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Booking;
use frontend\models\BookedFlight;
use frontend\models\BookedHotel;
use common\models\faregrabbr\Payment;
use frontend\models\search\BookedFlightSearch;
use frontend\models\search\BookedHotelSearch;
use frontend\models\Event;
use common\models\faregrabbr\HotelDetails;
use common\models\faregrabbr\Flight;
use frontend\models\search\BookingSearch;
use common\models\faregrabbr\Passenger as FaregrabberPassenger;
use frontend\models\Passenger;
use frontend\models\Guest;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\InvalidParamException;
use frontend\models\FaregrabbrFlightForm;
use linslin\yii2\curl\Curl;
use yii\helpers\Json;
use common\models\CreditCardForm;
use common\models\faregrabbr\search\HotelSearch;
use common\models\faregrabbr\HotelBookRequest;
use common\models\faregrabbr\Rate;

function userOwnsBooking($model) {
	return !(isset($model) && (($model->session_id != Yii::$app->session['booking_session_id']['value'] && Yii::$app->user->isGuest)
		|| (!Yii::$app->user->isGuest && Yii::$app->user->id != $model->user_id && $model->session_id != Yii::$app->session['booking_session_id']['value'])));
}

/**
 * BookingController implements the CRUD actions for Booking model.
 */
class BookingController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'delete','search','book'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'delete', 'book'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	
    /**
     * Lists all Booking models for a particular user only (check BookingSearch)
     * @return mixed
     */
    public function actionIndex()
    {
        $confirmedSearchModel = new BookingSearch();
        $confirmedDataProvider = $confirmedSearchModel->search(Yii::$app->request->queryParams, true);
		
		$outstandingSearchModel = new BookingSearch();
        $outstandingDataProvider = $outstandingSearchModel->search(Yii::$app->request->queryParams, false);

        return $this->render('index', [
            'confirmedSearchModel' => $confirmedSearchModel,
            'confirmedDataProvider' => $confirmedDataProvider,
			'outstandingSearchModel' => $outstandingSearchModel,
            'outstandingDataProvider' => $outstandingDataProvider,
        ]);
    }


    /**
     * Displays a single Booking model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id=NULL,$hK=NULL, $fK=NULL, $rate=NULL,$passengers=NULL,$guests=NULL,$event_id=NULL)
    {
        $event = Event::findOne($event_id);
		
		// Define these
		$hotelModel = NULL;
		$flightModel = NULL;
		
		// Decode stuff
		if(isset($rate)) $rate = json_decode($rate);
		if($guests != NULL || $passengers != NULL) {
			if($passengers != NULL) {
				$passengers = json_decode($passengers);
				if(count($passengers) > count($guests)) $guests = $passengers;
			}
			if(is_string($guests)) $guests = json_decode($guests);
			$count = count($guests);
			$guests = [new Guest(false, ['scenario' => Guest::SCENARIO_BOOK])];
			for ($i=1; $i < $count; $i++) { 
				$guests[] = new Guest(false, ['scenario' => Guest::SCENARIO_BOOK]);
			}
		}
		else {
			$guests = [new Guest(false, ['scenario' => Guest::SCENARIO_BOOK])];
		}
		
		// Find the booking if $id exists                            
		if(isset($id)) {
			$model = $this->findModel($id);	
			$event_id = $model->event_id;
			
			// Somebody changed the booking id in the URL. Send them an error.
			// You sneaky buggers.
			if(!userOwnsBooking($model)) {
				return $this->render('../site/error', [
					'name' => "Error",
					'message' => "You cannot view that booking ID, sorry."
					]);
			}
			
			
			// Is the user logged in?
			if(!Yii::$app->user->isGuest) {
				
				// Does it exist for their user id?
				$booking = Booking::find()->where(['user_id' => Yii::$app->user->id, 'event_id' => (int)$event_id ])->one();
				
				
				// Check if it exists based on session id
				if($booking==NULL) { // TODO: does this work?
					
					// Search for booking based on session id
					$booking = Booking::find()->where(['session_id' => Yii::$app->session['booking_session_id']['value'], 'event_id' => (int)$event_id ])->one();
					
						if($booking!=NULL) {
							$booking->user_id = Yii::$app->user->id;
							$booking->save();
						}
				}
			}
		}
				
		
		if(!isset($id)) {
			
			// Is the user logged in?
			if(!Yii::$app->user->isGuest) {
				
				// Does it exist for their user id?
				$model = Booking::find()->where(['user_id' => Yii::$app->user->id, 'event_id' => (int)$event_id ])->one();
				
				// Check if it exists based on session id
				if($model==NULL) {
					
					// Search for booking based on session id
					$model = Booking::find()->where(['session_id' => Yii::$app->session['booking_session_id']['value'], 'event_id' => (int)$event_id ])->one();
					if($model!=NULL) {
						$model->user_id = Yii::$app->user->id;
					}
				}
			}
			else {
				$model = Booking::find()->where(['session_id' => Yii::$app->session['booking_session_id']['value'], 'event_id' => (int)$event_id ])->one();
			}
			
			if($model) {
				// if the booking was previously deleted, revive it
				if ($model->deleted == Booking::DELETED) {
					$model->deleted = Booking::NOT_DELETED;
					$model->save(false);
				}
			}
			else {
				$model = new Booking();
			
				// Is the user logged in?
				$model->user_id = Yii::$app->user->id;
				$model->session_id = Yii::$app->session['booking_session_id']['value'];
				$model->event_id = (int)$event_id;
			}
			
			$id = $model->id;
		}
		
		// If they are already booked, load them.
		$bookedFlightSearch = new BookedFlightSearch($id);
		$bookedFlightProvider = $bookedFlightSearch->search(Yii::$app->request->queryParams);
		$bookedHotelSearch = new BookedHotelSearch($id);
		$bookedHotelProvider = $bookedHotelSearch->search(Yii::$app->request->queryParams);
		
		// Cache/save the hotel details if they are included.
		if($hK != null) {
			$hotelModel = Yii::$app->cache->get($hK); // Get the quick-cached hotel data.
			
			// Cache all the required hotel data for check-out for 2 hours.
			if(isset($hotelModel) && isset($guests) && isset($rate)) {
				$model->saved_hotel_rate = json_encode($rate);
				$model->saved_guests = json_encode($guests);
				$model->saved_hotel_model = json_encode($hotelModel);
			}
		}
		
				// Cache/save the hotel details if they are included.
		if($fK != null) {
			$flightModel = Yii::$app->cache->get($fK); // Get the quick-cached hotel data.
			
			if(isset($flightModel) && isset($guests)) {
				// Cache all the required hotel data for check-out for 2 hours.
				$model->saved_guests = json_encode($guests);
				$model->saved_flight_model = json_encode($flightModel);
			}
		}
		
		// If there is no booked data, we may have a cached/saved hotel.
		if($bookedHotelProvider->totalCount == 0) {
			$rate = json_decode($model->saved_hotel_rate);
			$guests = json_decode($model->saved_guests);
			
			// Decode guests. 
			if(is_string($guests)) $guests = json_decode($guests);
			$count = count($guests);
			$guests = [new Guest(false, ['scenario' => Guest::SCENARIO_BOOK])];
			for ($i=1; $i < $count; $i++) { 
				$guests[] = new Guest(false, ['scenario' => Guest::SCENARIO_BOOK]);
			}
			
			// Decode model.
			$hotelModel = json_decode($model->saved_hotel_model);
		}
		
		if($bookedFlightProvider->totalCount == 0) {
			$guests = $model->saved_guests;
			
			// Decode guests. 
			if(is_string($guests)) $guests = json_decode($guests);
			$count = count($guests);
			$guests = [new Guest(false, ['scenario' => Guest::SCENARIO_BOOK])];
			for ($i=1; $i < $count; $i++) { 
				$guests[] = new Guest(false, ['scenario' => Guest::SCENARIO_BOOK]);
			}
			
			// Decode model.
			$flightModel = json_decode($model->saved_flight_model);
		}
		
		$model->save();
		
		$cookies = Yii::$app->request->cookies;
		$getHotel = $cookies->getValue('getFlightHotel' . $event_id, 'none');
		if($getHotel!='none' && $hK==null) {
			$getHotel = json_decode($getHotel,true);
			$this->redirect(['hotel/search', 
			'event_id' => $event->id, 
			'numrooms' => $getHotel['numrooms'], 
			'guests' => $getHotel['guests'], 
			'checkout' => $getHotel['checkout'], 
			'checkin' => $getHotel['checkin'],
			'default' => 1,]
			);
		}
			
		return $this->render('view', [
			'model' => $model,
			'bookedFlightSearch' => $bookedFlightSearch,
			'bookedFlightProvider' => $bookedFlightProvider,
			'bookedHotelSearch' => $bookedHotelSearch,
			'bookedHotelProvider' => $bookedHotelProvider,
			'event_id' => $model->event_id,
			'rate' => $rate,
			'guests' => $guests,
			'hotelModel' => $hotelModel,
			'flightModel' => $flightModel,
			'id' => $id
		]);
    }
	
	// Delete a saved booking
	public function actionDeleteSavedBooking($type, $id) {
		
		// Find booking.
		$booking = Booking::find()->where(['id' => $id])->one();
		
		// Verify it's the user's booking.
		if(!userOwnsBooking($booking)) {
				return $this->render('../site/error', [
					'name' => "Error",
					'message' => "You cannot view that booking ID, sorry."
					]);
		}
		else {
			
			// Delete saved hotel.
			if($type=="hotel") {
				$booking->saved_hotel_model = "";
				$booking->save();
				$this->redirect(['booking/view', 'id' => $id]);
			}
			
			// Delete saved flight.
			else if($type=="flight") {
				$booking->saved_flight_model = "";
				$booking->save();
				$this->redirect(['booking/view', 'id' => $id]);
			}
			
			// Otherwise do nothing.
			else if($type=="all") {
				$booking->delete();
				$this->redirect(['booking/index']);
			}
		}
	}
	
	// Book hotel and flight at once.
	    public function actionBook($event_id, $id)
    {
		// Decode stuff.
		$event = Event::findOne($event_id);
		$booking = Booking::find()->where(['id' => $id])->one();
		$rate = $booking->saved_hotel_rate;
		if($rate!=NULL) {
			$rate = json_decode($rate,true);
			$rate = new Rate($rate);
		}
		
		// Lost post.
		$post = Yii::$app->request->post();
				
		// Load guests from post.
		$guestsFromPost = $post["Guest"];
		
		// Load credit card form.
		$creditCardFormFromPost = $post["CreditCardForm"];
		
		////////////////////////
		//// LOAD PASSENGERS ///
		////////////////////////
        // load multiple post passengers into site passenger models 
        // there are 2 types of passenger models, one for the website and the database, 
        // one for the Faregrabbr API because god knows what the API is going to look like in the future
		for($i = 0; $i < count($guestsFromPost); $i++) {
			$passengers[$i] = new Passenger($guestsFromPost[$i], ['scenario' => Passenger::SCENARIO_BOOK]);
		}
        if (Passenger::validateMultiple($passengers)) {
            // for each site passenger now create a faregrabbr passenger
            foreach ($passengers as $index => $passenger) {
                $faregrabberPassenger = new FaregrabberPassenger($index, $passenger);
                $faregrabberPassengers[] = $faregrabberPassenger;
            }
        }
		
        // load credit card details into the payment model
        $creditCardForm = new CreditCardForm();
		$creditCardForm->month = $creditCardFormFromPost["month"];
		$creditCardForm->year = $creditCardFormFromPost["year"];
		$creditCardForm->name = $creditCardFormFromPost["name"];
		$creditCardForm->number = $creditCardFormFromPost["number"];
		$creditCardForm->code = $creditCardFormFromPost["code"];
		$creditCardForm->addressLine1  = $creditCardFormFromPost["addressLine1"];
		$creditCardForm->city = $creditCardFormFromPost["city"];
		$creditCardForm->state = $creditCardFormFromPost["state"];
		$creditCardForm->zip  = $creditCardFormFromPost["zip"];
        if($creditCardForm->validate()) {
            // load the CreditCardForm into a payment
            $payment = new Payment($creditCardForm);
        }
		
		// Success?
		$flightResponseType = "Success";
		$flightResponse = "";
		$hotelResponseType = "Success";
		$hotelResponse = "";
		
		// Check if we already have bookings!
		$bookedFlightSearch = new BookedFlightSearch($id);
		$bookedFlightProvider = $bookedFlightSearch->search(Yii::$app->request->queryParams);
		$bookedHotelSearch = new BookedHotelSearch($id);
		$bookedHotelProvider = $bookedHotelSearch->search(Yii::$app->request->queryParams);
		
		$hotelModel = NULL;
		// Cast hotelModel to a HotelDetails class.
		if($booking->saved_hotel_model!=NULL) {
			$hotelModel = json_decode($booking->saved_hotel_model);
			$hotelModel = new HotelDetails($event_id, 
										   $hotelModel->hotelCode, 
										   $hotelModel->hotelChain, 
										   $hotelModel->checkin,
										   $hotelModel->numrooms,
										   $hotelModel->guests,
										   $hotelModel->checkout);
		}
		
		$flightModel = NULL;
		if($booking->saved_flight_model!=NULL) {
			
			// Cast flightModel to Flight
			$flightModel = json_decode($booking->saved_flight_model);
		
			$flightModel = new Flight($event_id,
									  $flightModel->origin,
									  $flightModel->destination,
									  $flightModel->departure_date,
									  $flightModel->adults,
									  $flightModel->children,
									  $flightModel->infants,
									  json_decode($booking->saved_flight_model, 1));
		}
		
		// Book the hotel
		if($hotelModel != NULL) $hotelBookRequest = new HotelBookRequest($hotelModel, $hotelModel->numrooms, $hotelModel->checkin, $hotelModel->checkout, $rate, $faregrabberPassengers, $payment);
		
		// Book the hotel; if not false we got a response from the server
        if (count($bookedHotelProvider->getModels()) == 0 && $hotelModel != NULL && $data = Yii::$app->faregrabbr->bookHotel($hotelBookRequest)) {
            // if status is true the booking was successful so we save the booking details to the database
            if ($data['status'] === true) {
                $bookedHotel = new BookedHotel($data); // pass the data to construct the model

                // connect the BookedHotel with the event and the booking if flight is to an event
                if ($event) {
                    $booking = Booking::find()->where(['user_id' => Yii::$app->user->id, 'event_id' => (int)$event->id ])->one();
                    $bookedHotel->event_id = $event->id;
                    if ($booking) {
                        $bookedHotel->booking_id = $booking->id;
                    }
                }

                if ($bookedHotel->validate() && $bookedHotel->save(false)) {
                    // use a foreach because we can't use loadMultiple
                    foreach ($data['Passengers'] as $guestData) {
                        $guest = new Guest($guestData, ['scenario' => Guest::SCENARIO_SAVE]);
                        if ($guest->validate()) {
                            $guest->link('bookedHotel', $bookedHotel);
                        }                  
                    }

                    $hotelResponseType = "Success";

                } else {
						$hotelResponseType = "Local Server Error";
						$hotelResponse = "Your hotel was booked successfully but the hotel data could not be saved locally. Please contact a website administrator. Your order number is: " . $data['ordernumber'];
                }
            } elseif ($data['status'] === false) {
                       $hotelResponseType = "Booking Error";
                       $hotelResponse = $data['statustext'];
            } else {
            // some unforseen shit like the server didn't send back valid JSON as it happened several times in the past
                        $hotelResponseType = "Unforseen Error";
                        $hotelResponse = "Could not determine the nature of the error, please contact a website administrator.";       
            }
        // else we did not get a response from the server
        } else if(!(count($bookedHotelProvider->getModels()) != 0) && $hotelModel != NULL) {
            $hotelResponseType = "Booking Failed";
            $hotelResponse = "Failed to reach the hotel booking API server.";  
        }

        // Book the flight.
        if (count($bookedFlightProvider->getModels()) == 0 && $flightModel != NULL && $data = Yii::$app->faregrabbr->bookFlight($flightModel, $payment, $faregrabberPassengers)) {
		   
            // if status is true the booking was successful so we save the booking details to the database
           if ($data['status'] === true) {
                $bookedFlight = new BookedFlight($data); // pass the data to construct the model

                // connect the BookedFlight with the event and the booking if flight is to an event
                if ($event) {
                    $booking = Booking::find()->where(['user_id' => Yii::$app->user->id, 'event_id' => (int)$event->id ])->one();
                    $bookedFlight->event_id = $event->id;
                    if ($booking) {
                        $bookedFlight->booking_id = $booking->id;
                    }
                }

                if ($bookedFlight->validate() && $bookedFlight->save(false)) {
                    // use a foreach because we can't use loadMultiple
                    foreach ($data['passenger'] as $passengerData) {
                        $passenger = new Passenger($passengerData, ['scenario' => Passenger::SCENARIO_SAVE]);
                        if ($passenger->validate()) {
                            $passenger->link('bookedFlight', $bookedFlight);
                        }                  
                    }

                    // this will actually redirect the user to his booking on this event
					$flightResponseType = "Success";

                } else {
                       $flightResponseType = "Local Server Error";
                       $flightResponse =   "Your flight was booked successfully but the flight data could not be saved locally. Please contact a website administrator. Your order number is: " . $data['ordernumber'];
                }
            }
			elseif ($data['status'] === false) {
                        $flightResponseType =  "Booking Error";
                        $flightResponse = $data['statustext'];
            } 
			
			else {
            // some unforseen shit like the server didn't send back valid JSON as it happened several times in the past
				$flightResponseType = "Unforseen Error";
				$flightResponse = "Could not determine the nature of the error, please contact a website administrator.";
            }
        // else we did not get a response from the server
        } 
		
		else if(!(count($bookedFlightProvider->getModels()) != 0) && $flightModel != NULL) {
			$flightResponseType = "Booking Failed";
			$flightResponse = "Failed to reach the flight booking API server.";
        }
		
		// One of the two bookings are successful? Then set booking to be confirmed.
		if($flightResponseType == "Success" || $hotelResponseType == "Success") {
			$booking->confirmed = true;
			$booking->save();
		}
		
		// Success!
		if($flightResponseType == "Success" && $hotelResponseType == "Success") {
			
			if($hotelModel != NULL && $flightModel != NULL) $successMessage = "Your flight and hotel were booked successfully.";
			else if($hotelModel == NULL && $flightModel != NULL) $successMessage = "Your flight was booked successfully.";
			else if($hotelModel != NULL && $flightModel == NULL) $successMessage = "Your hotel was booked successfully";
			Yii::$app->session->setFlash('success', $successMessage . ' Please check your e-mail for further details.');	
			$this->redirect(['booking/view', 'event_id' => $event->id]);
		}
		
		// Error 
		else {
			
			if($flightResponseType == "Success") {
				
				// We booked only a hotel.
				if($flightModel==NULL) {
					return $this->render('../site/error', [
					'name' => "Error",
					'message' =>	"Error Booking Hotel: " . $hotelResponseType . " - " . $hotelResponse
					]);
				}
				
				Yii::$app->session->setFlash('success', "Success Booking Flight: 'Your flight was booked successfully. Please check your e-mail for further details.");
				
				// Render the results.
				return $this->render('../site/error', [
				'name' => "Error",
				'message' => "Error Booking Hotel: " . $hotelResponseType . " - " . $hotelResponse
				]);
			}
			
			else if($hotelResponseType == "Success") {
				if($hotelModel==NULL) {
					// Render the results.
					return $this->render('../site/error', [
					'name' => "Error",
					'message' => "Error Booking Flight: " . $flightResponse
					]);
				}
				
				Yii::$app->session->setFlash('success', "Success Booking Hotel: Your hotel was booked successfully. Please check your e-mail for further details.");	
				
				// Render the results.
				return $this->render('../site/error', [
				'name' => "Error",
				'message' => "Error Booking Flight: " . $flightResponse
				]);
				
			}
			
			else {
				
				// Render the results.
				return $this->render('../site/error', [
				'name' => "Error",
				'message' => "Error Booking Flight and Hotel: " . $flightResponse . 
						     ". " . $hotelResponse
				]);
				
			}
			
		}
    }

    /**
     * Creates a new Booking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($event_id)
    {
		// Is the user logged in?
		if(!Yii::$app->user->isGuest) {
			
			// Does it exist for their user id?
			$model = Booking::find()->where(['user_id' => Yii::$app->user->id, 'event_id' => (int)$event_id ])->one();
			
			// Check if it exists based on session id
			if($model==NULL) {
				
				// Search for booking based on session id
				$model = Booking::find()->where(['session_id' => Yii::$app->session['booking_session_id']['value'], 'event_id' => (int)$event_id ])->one();
				if($model!=NULL) {
					$model->user_id = Yii::$app->user->id;
				}
			}
		}
		else {
			$model = Booking::find()->where(['session_id' => Yii::$app->session['booking_session_id']['value'], 'event_id' => (int)$event_id ])->one();
		}
		
		if($model == null) {
			$model = new Booking();
			$model->user_id = Yii::$app->user->id;
			$model->session_id = Yii::$app->session['booking_session_id']['value'];
			$model->event_id = (int)$event_id;
			$model->save();
		}
		
		return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Deletes an existing Booking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // deletion should not be possible if there are booked flights or booked hotels
        $booking = $this->findModel($id);
        $booking->deleted = Booking::DELETED;
        $booking->save(false);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Booking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Booking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Booking::findOne(['id' => $id, 'deleted' => \common\models\EstData::NOT_DELETED, 'archived' => \common\models\EstData::NOT_ARCHIVED])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
