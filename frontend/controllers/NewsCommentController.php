<?php

namespace frontend\controllers;

use Yii;
use frontend\models\NewsComment;
use frontend\models\search\NewsCommentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NewsCommentController implements the CRUD actions for NewsComment model.
 */
class NewsCommentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionPost($news_id)
    {

        if (Yii::$app->request->isAjax) {
            if (!Yii::$app->user->isGuest) {
                $model = new NewsComment();
                $model->load(Yii::$app->request->post());
                $model->user_id = Yii::$app->user->id;
                if ($model->save()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                Yii::$app->session->setFlash('error', 'Please log in before posting a comment. Thank you.');
                $this->goHome();
            }
        }

    }

    public function actionDelete($id, $user_id)
    {
        if (Yii::$app->request->isPjax) {
            if (Yii::$app->user->id !== (int)$user_id) {
                throw new UnauthorizedHttpException('You are not authorized to delete this comment.');
            } else {
                $comment = $this->findModel($id, $user_id);
                if (Yii::$app->user->id !== $comment->user_id) {
                    throw new UnauthorizedHttpException('You are not authorized to delete this comment.');
                } else {
                    $comment->delete();
                    return $this->renderAjax(['news/view', 'id' => $comment->news_id]);
                }
            }
        } else {
            if (Yii::$app->user->id !== (int)$user_id) {
                throw new UnauthorizedHttpException('You are not authorized to delete this comment.');
            } else {
                $comment = $this->findModel($id, $user_id);
                if (Yii::$app->user->id !== $comment->user_id) {
                    throw new UnauthorizedHttpException('You are not authorized to delete this comment.');
                } else {
                    if ($comment->delete()) {
                        return $this->redirect(['news/view', 'id' => $comment->news_id]);
                    } else {
                        return $this->redirect(['news/view', 'id' => $comment->news_id]);
                    }
                }
            }
        }
    }

    /**
     * Finds the NewsComment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $user_id
     * @return NewsComment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $user_id)
    {
        if (($model = NewsComment::findOne(['id' => $id, 'user_id' => $user_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
