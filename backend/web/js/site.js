jQuery(document).ready(function () {

	 $(document).on('submit', '.ticket-comment-form', function(e) {
	    $('.comments').addClass('loading');
	 	e.preventDefault();
	 	var action = $(this).attr('action');
	 		$.ajax({
				  type: "POST",
				  url: action,
				  data: $(this).serialize()
				})
				  .done(function( response ) {
					$("#ticketcomment-body").val('');
				    $.pjax.reload({container:'#pjax-comments'});
		            $('.comments').removeClass('loading');
				    }
				);
	 });

});