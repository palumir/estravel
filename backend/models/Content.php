<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "content".
 *
 * @property integer $id
 * @property string $action
 * @property string $menu
 * @property string $title
 * @property string $body
 * @property string $tags
 * @property string $banner
 * @property string $created_at
 * @property string $updated_at
 */
class Content extends \common\models\EstData
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'action', 'menu', 'title', 'body'], 'required'],
            [['id'], 'integer'],
            [['body'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['action', 'menu'], 'string', 'max' => 45],
            [['title', 'tags', 'banner'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action' => 'Action',
            'menu' => 'Menu',
            'title' => 'Title',
            'body' => 'Body',
            'tags' => 'Tags',
            'banner' => 'Banner',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
