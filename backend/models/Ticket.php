<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;


/**
 * This is the model class for table "ticket".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $admin_id
 * @property string $title
 * @property string $body
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property User $admin
 * @property TicketComment[] $ticketComments
 */
class Ticket extends \common\models\EstData
{


    const STATUS_SAVED = 1;
    const STATUS_ASSIGNED = 2;
    const STATUS_RESOLVED = 3;
    const STATUS_FAILED = 4;

    const SAVED = "Saved";
    const ASSIGNED = "Assigned";
    const RESOLVED = "Resolved";
    const FAILED = "Failed";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \common\models\EstData::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \common\models\EstData::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blame' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'admin_id', 'title', 'body'], 'required'],
            [['id', 'user_id', 'admin_id', 'status'], 'integer'],
            [['body'], 'string'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['updated_at'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'admin_id' => 'Admin ID',
            'title' => 'Title',
            'body' => 'Body',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(User::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketComments()
    {
        return $this->hasMany(TicketComment::className(), ['ticket_id' => 'id', 'ticket_user_id' => 'user_id']);
    }

    public static function getUnassigned()
    {
        return self::find()->where(['admin_id' => NULL])->count();
    }

    public function getStatusString()
    {
        switch ($this->status) {
            case self::STATUS_SAVED;
                return self::SAVED;
                break;
            case self::STATUS_ASSIGNED;
                return self::ASSIGNED;
                break;
            case self::STATUS_RESOLVED;
                return self::RESOLVED;
                break;
            case self::STATUS_FAILED;
                return self::FAILED;
                break;
            default:
                return false;
                break;
        }
    }

    // returns an array of bootstrap3 classes based on status to color table rows
    public function colorText()
        {
            switch ($this->status) {
                case self::STATUS_SAVED:
                    $htmlAttr = 'text-info';
                    break;
                case self::STATUS_ASSIGNED:
                    $htmlAttr = 'text-primary';
                    break;
                case self::STATUS_RESOLVED:
                    $htmlAttr = 'text-success';
                    break;
                case self::STATUS_FAILED:
                    $htmlAttr = 'text-danger';
                    break;
                default:
                    $htmlAttr = 'text-default';
                    break;
            }
            return $htmlAttr;
        }
}
