<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "carousel".
 *
 * @property integer $id
 * @property integer $rank
 * @property string $title
 * @property string $image
 * @property string $summary
 * @property string $description
 */
class Carousel extends \common\models\EstData
{

    public $_image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carousel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rank'], 'integer'],
            [['event_id'], 'integer'],
            [['rank'], 'unique'],
            [['rank'], 'in', 'range' => range(1, 99)],
            [['title'], 'required'],
            [['title', 'image'], 'string', 'max' => 255],
            [['summary'], 'string', 'max' => 900],
            [['link'], 'string', 'max' => 2000],
            [['description'], 'string', 'max' => 5000],
            ['_image', 'image', 'extensions' => 'png, jpg',
                'minWidth' => 1140, 'maxWidth' => 1140,
                'minHeight' => 360, 'maxHeight' => 360,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'rank' => 'Rank',
            'title' => 'Title',
            'image' => 'Image',
            'summary' => 'Summary',
            'description' => 'Description',
            'link' => 'Link',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

}
