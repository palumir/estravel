<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use common\models\Country;
use backend\models\Game;
use backend\models\EventGame;
use backend\models\Carousel;
use common\models\Airport;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property string $start_time
 * @property string $end_time
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $city
 * @property string $address_line1
 * @property string $address_line2
 * @property string $zipcode
 *
 * @property Booking[] $bookings
 * @property User $createdBy
 * @property User $updatedBy
 * @property EventComment[] $eventComments
 * @property EventsAirports[] $eventsAirports
 */
class Event extends \common\models\EstData
{

    const STATUS_SAVED = 1;
    const STATUS_UPCOMING = 2;
    const STATUS_ONGOING = 3;
    const STATUS_FINISHED = 4;

    const SAVED = "Saved";
    const UPCOMING = "Upcoming";
    const ONGOING = "Ongoing";
    const FINISHED = "Finished";

    // Simple Array of all the games in the event
    public $_gamesArray;
    public $_oldGamesArray;

    public $_logo;
    public $_thumbnail;
    public $_image;
    public $_background;

    function __construct($config = [])
    {
        parent::__construct($config);
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \common\models\EstData::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \common\models\EstData::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blame' => [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'intro', 'start_time', 'end_time', 'country_id', 'city', 'address_line1', 'zipcode', 'timezone', '_gamesArray', 'airport_id'], 'required'],
            [['default_departure_date', 'default_return_date'], 'required'],
            [['id', 'status', 'created_by', 'updated_by', 'country_id', 'airport_id'], 'integer'],
            [['intro', 'logo', 'summary', 'description'], 'string'],
            [['start_time', 'end_time', 'created_at', 'updated_at', 'default_departure_date', 'default_return_date', ], 'safe'],
            [['name', 'city', 'venue', 'address_line1', 'address_line2', 'zipcode'], 'string', 'max' => 255],
            [['logo', 'website', 'facebook', 'background', 'image', 'thumbnail'], 'string', 'max' => 255],
            ['_logo', 'image', 'extensions' => 'png, jpg',
                'minWidth' => 300, 'maxWidth' => 300,
                'minHeight' => 300, 'maxHeight' => 300,
            ],
            ['_thumbnail', 'image', 'extensions' => 'png, jpg',
                'minWidth' => 150, 'maxWidth' => 150,
                'minHeight' => 150, 'maxHeight' => 150,
            ],
            ['_image', 'image', 'extensions' => 'png, jpg',
                'minWidth' => 500, 'maxWidth' => 500,
                'minHeight' => 500, 'maxHeight' => 500,
            ],
            ['_background', 'image', 'extensions' => 'png, jpg',
                'minWidth' => 1140, 'maxWidth' => 1140,
                'minHeight' => 360, 'maxHeight' => 360,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [ 
            'id' => 'ID',
            'name' => 'Name',
            'intro' => 'Intro',
            'logo' => 'Logo',
            'website' => 'Website',
            'facebook' => 'Facebook',
            'background' => 'Background',
            'image' => 'Image',
            'thumbnail' => 'Thumbnail',
            'summary' => 'Summary',
            'description' => 'Description',
            'status' => 'Status',
            'timezone' => 'Timezone',
            'airport_id' => 'Default Destination Airport',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'default_departure_date' => 'Default Departure Date',
            'default_return_date' => 'Default Return Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'city' => 'City',
            'venue' => 'Venue',
            'address_line1' => 'Address Line1',
            'address_line2' => 'Address Line2',
            'zipcode' => 'Zipcode',
            'country_id' => 'Country',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarousel()
    {
        return $this->hasOne(Carousel::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventComments()
    {
        return $this->hasMany(EventComment::className(), ['event_id' => 'id']);
    }

    public function getEventGames()
    {
        return $this->hasMany(EventGame::className(), ['event_id' => 'id']);
    }

    public function getGames()
    {
        return $this->hasMany(Game::className(), ['id' => 'game_id'])
            ->via('eventGames');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAirport()
    {
        return $this->hasOne(Airport::className(), ['id' => 'airport_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public static function statusDropDownArray()
    {
        return [
            self::STATUS_SAVED => self::SAVED, 
            self::STATUS_UPCOMING => self::UPCOMING, 
            self::STATUS_ONGOING => self::ONGOING, 
            self::STATUS_FINISHED => self::FINISHED, 
            ];
    }

    public function getStatusString()
    {
        switch ($this->status) {
            case self::STATUS_SAVED;
                return self::SAVED;
                break;
            case self::STATUS_UPCOMING;
                return self::UPCOMING;
                break;
            case self::STATUS_ONGOING;
                return self::ONGOING;
                break;
            case self::STATUS_FINISHED;
                return self::FINISHED;
                break;
            default:
                return false;
                break;
        }
    }

    public function afterFind()
    {
        $this->_gamesArray = ArrayHelper::getColumn($this->games, 'id');
        $this->_oldGamesArray = ArrayHelper::getColumn($this->games, 'id');
        if (parent::afterFind()) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }

    public function afterValidate()
    {
        if (parent::afterValidate()) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (parent::afterSave($insert, $changedAttributes)) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }

    public function afterDelete()
    {
        if (parent::afterDelete()) {
            // noob code here
            return true;
        } else {
            return false;
        }
    }

}
