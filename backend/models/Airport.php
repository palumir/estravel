<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "airport".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $city
 *
 * @property EventsAirports[] $eventsAirports
 * @property UsersAirports[] $usersAirports
 */
class Airport extends \common\models\EstData
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'airport';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'code'], 'required'],
            [['id'], 'integer'],
            [['name', 'city'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'city' => 'City',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsAirports()
    {
        return $this->hasMany(EventsAirports::className(), ['airport_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersAirports()
    {
        return $this->hasMany(UsersAirports::className(), ['airport_id' => 'id']);
    }
}
