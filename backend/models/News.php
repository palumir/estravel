<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $admin_id
 * @property string $title
 * @property string $body
 * @property string $tags
 * @property integer $published
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $admin
 * @property NewsComment[] $newsComments
 */
class News extends \common\models\EstData
{

    const STATUS_SAVED = 1;
    const STATUS_PUBLISHED = 2;

    const SAVED = "Saved";
    const PUBLISHED = "Published";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \common\models\EstData::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \common\models\EstData::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blame' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'admin_id',
                'updatedByAttribute' => false,
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'body'], 'required'],
            [['id', 'admin_id', 'published'], 'integer'],
            [['body'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'tags'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'admin_id' => 'Admin ID',
            'title' => 'Title',
            'body' => 'Body',
            'tags' => 'Tags',
            'published' => 'Published',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(User::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsComments()
    {
        return $this->hasMany(NewsComment::className(), ['news_id' => 'id']);
    }

    public static function publishedDropDownList()
    {
        return [
            self::STATUS_SAVED => self::SAVED, 
            self::STATUS_PUBLISHED => self::PUBLISHED, 
            ];
    }

    public function getPublishedString()
    {
        switch ($this->published) {
            case self::STATUS_SAVED;
                return self::SAVED;
                break;
            case self::STATUS_PUBLISHED;
                return self::PUBLISHED;
                break;
            default:
                return false;
                break;
        }
    }

}
