<?php

namespace backend\controllers;

use Yii;
use backend\models\Carousel;
use backend\models\Event;
use backend\models\search\CarouselSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use backend\models\User;

/**
 * CarouselController implements the CRUD actions for Carousel model.
 */
class CarouselController extends Controller
{
    public function behaviors()
    {
        return [
		        'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'delete','update'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'delete','update'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return !Yii::$app->user->isGuest && Yii::$app->user->identity->role==2;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Carousel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarouselSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Carousel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Carousel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($event_id = null)
    {
        $model = new Carousel();

        // carousel is to be attached to an event
        if ($event_id !== null) {
            if ($event = $this->findEvent($event_id)) {
                $model->event_id = $event->id;
                $model->title = $event->name;
                $model->summary = $event->summary;
                $model->description = $event->description;
            }
            // This event already has a carousel
            if ($event->carousel) {
                return $this->redirect(["update", 'id' => $event->carousel->id]);
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->_image = UploadedFile::getInstance($model, '_image');
                if ($model->_image !== NULL) {
                    $model->image = "images/carousel_images" . "/" . str_replace(' ', '', $model->_image->baseName) . "_" . time() . '.' . $model->_image->extension;
                }
                if ($model->save()) {
                    if ($model->_image !== NULL) {                              
                        $model->_image->saveAs(Yii::getAlias('@frontend') . "/web" . "/images/carousel_images" . "/" . str_replace(' ', '', $model->_image->baseName) . "_" . time() . '.' . $model->_image->extension);
                    }       
                }  
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Carousel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->_image = UploadedFile::getInstance($model, '_image');
                if ($model->_image !== NULL) {
                    $model->image = "images/carousel_images" . "/" .  str_replace(' ', '', $model->_image->baseName) . "_" . time() . '.' . $model->_image->extension;
                }
                if ($model->save()) {
                    if ($model->_image !== NULL) {                              
                        $model->_image->saveAs(Yii::getAlias('@frontend') . "/web" . "/images/carousel_images" . "/" . str_replace(' ', '', $model->_image->baseName) . "_" . time() . '.' . $model->_image->extension);
                    }    
                    return $this->redirect(['view', 'id' => $model->id]);                
                }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Carousel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {
            throw new \yii\web\UnauthorizedHttpException("You are not authorized to perform this action", 401);
        } else {
            $carousel = $this->findModel($id);
            $carousel->delete();
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Carousel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Carousel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Carousel::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findEvent($id)
    {
        if (($model = Event::findOne(['id' => $id, 'deleted' => \common\models\EstData::NOT_DELETED, 'archived' => \common\models\EstData::NOT_ARCHIVED])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
