<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use backend\models\search\BookingSearch;
use backend\models\search\EventSearch;
use backend\models\search\CommentSearch;
use backend\models\User;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
					[
                        'actions' => ['index'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return !Yii::$app->user->isGuest && Yii::$app->user->identity->role==2;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {

        $bookingSearchModel = new BookingSearch();
        $bookingDataProvider = $bookingSearchModel->listLatest();
        $eventSearchModel = new EventSearch();
        $eventDataProvider = $eventSearchModel->listLatest();
        $commentSearchModel = new CommentSearch();
        $commentDataProvider = $commentSearchModel->listLatest();

        return $this->render('index', [
            'bookingSearchModel' => $bookingSearchModel,
            'bookingDataProvider' => $bookingDataProvider,
            'eventSearchModel' => $eventSearchModel,
            'eventDataProvider' => $eventDataProvider,
            'commentSearchModel' => $commentSearchModel,
            'commentDataProvider' => $commentDataProvider,
            ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
			if(!Yii::$app->user->isGuest && Yii::$app->user->identity->role!=2) {
				Yii::$app->user->logout();
				return $this->render('../site/error', [
					'name' => "Error 322",
					'message' => "You aren't permitted to view to the backend.",
				]);
			}
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
