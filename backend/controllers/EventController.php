<?php

namespace backend\controllers;

use Yii;
use backend\models\Event;
use backend\models\EventGame;
use backend\models\Game;
use backend\models\search\EventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use backend\models\User;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'delete','update'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'delete','update'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return !Yii::$app->user->isGuest && Yii::$app->user->identity->role==2;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Event();

        if ($model->load(Yii::$app->request->post())) {
            $model->_logo = UploadedFile::getInstance($model, '_logo');
            $model->_thumbnail = UploadedFile::getInstance($model, '_thumbnail');
            $model->_image = UploadedFile::getInstance($model, '_image');
            $model->_background = UploadedFile::getInstance($model, '_background');
            if ($model->validate()) {
                if ($model->_logo !== NULL) {
                    $model->logo = "images/event_logos" . "/" . str_replace(' ', '', $model->_logo->baseName) . "_" . time() . '.' . $model->_logo->extension;
                }
                if ($model->_thumbnail !== NULL) {
                    $model->thumbnail = "images/event_thumbnails" . "/" . str_replace(' ', '', $model->_thumbnail->baseName) . "_" . time() . '.' . $model->_thumbnail->extension;
                }
                if ($model->_image !== NULL) {
                    $model->image = "images/event_images" . "/" . str_replace(' ', '', $model->_image->baseName) . "_" . time() . '.' . $model->_image->extension;
                }
                if ($model->_background !== NULL) {
                    $model->background = "images/event_backgrounds" . "/" . str_replace(' ', '', $model->_background->baseName) . "_" . time() . '.' . $model->_background->extension;
                }
                if ($model->save()) {
                    if ($model->_logo !== NULL) {                              
                        $model->_logo->saveAs(Yii::getAlias('@frontend') . "/web" . "/images/event_logos" . "/" . str_replace(' ', '', $model->_logo->baseName) . "_" . time() . '.' . $model->_logo->extension);
                    }   
                    if ($model->_thumbnail !== NULL) {                              
                        $model->_thumbnail->saveAs(Yii::getAlias('@frontend') . "/web" . "/images/event_thumbnails" . "/" . str_replace(' ', '', $model->_thumbnail->baseName) . "_" . time() . '.' . $model->_thumbnail->extension);
                    }  
                    if ($model->_image !== NULL) {                              
                        $model->_image->saveAs(Yii::getAlias('@frontend') . "/web" . "/images/event_images" . "/" . str_replace(' ', '', $model->_image->baseName) . "_" . time() . '.' . $model->_image->extension);
                    }  
                    if ($model->_background !== NULL) {                              
                        $model->_background->saveAs(Yii::getAlias('@frontend') . "/web" . "/images/event_backgrounds" . "/" . str_replace(' ', '', $model->_background->baseName) . "_" . time() . '.' . $model->_background->extension);
                    }  
                    foreach ($model->_gamesArray as $id) {
                        $game = Game::findOne((int)$id);
                        $model->link('games', $game);
                    }     
                    return $this->redirect(['view', 'id' => $model->id]);
                }  
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->_logo = UploadedFile::getInstance($model, '_logo');
            $model->_thumbnail = UploadedFile::getInstance($model, '_thumbnail');
            $model->_image = UploadedFile::getInstance($model, '_image');
            $model->_background = UploadedFile::getInstance($model, '_background');

            if ($model->validate()) {
                if ($model->_logo !== NULL) {
                    $model->logo = "images/event_logos" . "/" .  str_replace(' ', '', $model->_logo->baseName) . "_" . time() . '.' . $model->_logo->extension;
                }
                if ($model->_thumbnail !== NULL) {
                    $model->thumbnail = "images/event_thumbnails" . "/" .  str_replace(' ', '', $model->_thumbnail->baseName) . "_" . time() . '.' . $model->_thumbnail->extension;
                }
                if ($model->_image !== NULL) {
                    $model->image = "images/event_images" . "/" .  str_replace(' ', '', $model->_image->baseName) . "_" . time() . '.' . $model->_image->extension;
                }
                if ($model->_background !== NULL) {
                    $model->background = "images/event_backgrounds" . "/" .  str_replace(' ', '', $model->_background->baseName) . "_" . time() . '.' . $model->_background->extension;
                }

                if ($model->save()) {
                    if ($model->_logo !== NULL) {                              
                        $model->_logo->saveAs(Yii::getAlias('@frontend') . "/web" . "/images/event_logos" . "/" . str_replace(' ', '', $model->_logo->baseName) . "_" . time() . '.' . $model->_logo->extension);
                    }    
                    if ($model->_thumbnail !== NULL) {                              
                        $model->_thumbnail->saveAs(Yii::getAlias('@frontend') . "/web" . "/images/event_thumbnails" . "/" . str_replace(' ', '', $model->_thumbnail->baseName) . "_" . time() . '.' . $model->_thumbnail->extension);
                    }   
                    if ($model->_image !== NULL) {                              
                        $model->_image->saveAs(Yii::getAlias('@frontend') . "/web" . "/images/event_images" . "/" . str_replace(' ', '', $model->_image->baseName) . "_" . time() . '.' . $model->_image->extension);
                    }   
                    if ($model->_background !== NULL) {                              
                        $model->_background->saveAs(Yii::getAlias('@frontend') . "/web" . "/images/event_backgrounds" . "/" . str_replace(' ', '', $model->_background->baseName) . "_" . time() . '.' . $model->_background->extension);
                    }   
                    foreach ($model->_gamesArray as $id) {
                        $game = Game::findOne((int)$id);
                        if (!EventGame::find()->where(['event_id' => $model->id, 'game_id' => (int)$id])->exists()) {
                            $model->link('games', $game);
                        }
                    }
                    $toRemove = array_diff($model->_oldGamesArray, $model->_gamesArray);
                    foreach ($toRemove as $id) {
                        $game = Game::findOne((int)$id);
                        $model->unlink('games', $game, true);
                    }
                    return $this->redirect(['view', 'id' => $model->id]);                
                }
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $event = $this->findModel($id);
        $event->deleted = Event::DELETED;
        $event->save(false);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne(['id' => $id, 'deleted' => \common\models\EstData::NOT_DELETED, 'archived' => \common\models\EstData::NOT_ARCHIVED])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
