<?php

namespace backend\controllers;

use Yii;
use backend\models\Airport;
use backend\models\search\AirportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AirportController implements the CRUD actions for Airport model.
 */
class AirportController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }




        public function actionAjaxList($q = null, $iata_code = null)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $out                         = ['results' => ['id' => '', 'name' => '']];

            if (!is_null($q)) {
                $query = new \yii\db\Query();
                $query->select(['id, CONCAT(name, ", ", city, ", ", country, " - ", iata_code) AS name'])
                    ->from('airport')
                    ->where(['like', 'name', $q])
                    ->orWhere(['like', 'city', $q])
                    ->orWhere(['like', 'iata_code', $q])
                    ->groupBy('iata_code')
                    // ->orderBy()
                    ->limit(20);
                $command = $query->createCommand();
                $data = $command->queryAll();
                $out['results'] = array_values($data);
            }
            elseif ($iata_code) {
                $out['results'] = ['iata_code' => $iata_code, 'name' => $this->findModel($iata_code)->name];
            }
            return $out;




            // $query = new \yii\db\Query();
            // $query->select('ap.code AS code, name AS name')
            //     ->from('airport ap')
            //     ->where('ap.name LIKE "%' . $q . '%"')
            //     ->limit(20);
            // $command        = $query->createCommand();
            // $data           = $command->queryAll();
            // $out['results'] = array_values($data);
            // return $out;
        }







    /**
     * Lists all Airport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AirportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Airport model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Airport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Airport();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Airport model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Airport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Airport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Airport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Airport::findOne(['id' => $id, 'deleted' => \common\models\EstData::NOT_DELETED, 'archived' => \common\models\EstData::NOT_ARCHIVED])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
