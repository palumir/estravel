<?php

/**
 * @author Mircea Silviu Itu <decemvre@gmail.com>
 */

namespace backend\widgets;

use yii\helpers\Html;

use yii\widgets\InputWidget;
use yiidreamteam\widgets\timezone\Picker;

// class KraftTimeZonePicker extends InputWidget
// {
//     /**
//      * @inheritdoc
//      */
//     public function run()
//     {
//         $systemTimeZone = system('date +%Z');
//         $timeZones = [];
//         foreach (\DateTimeZone::listIdentifiers(\DateTimeZone::ALL) as $timezone) {
//             $dtz = "dateTimeZone".$timezone;
//             $$dtz = new \DateTimeZone($timezone);
//             $dateTime = new \DateTime($systemTimeZone);
//             $offset = $$dtz->getOffset($dateTime);
//             $sign = ($offset < 0)?"-":"+";
//             $timeZones[$offset . "_" . $timezone] = $timezone . " $sign" . gmdate("H:i", $offset);
//         }

//         echo Html::activeDropDownList($this->model, $this->attribute, $timeZones, ['class' => 'form-control']);
//     }
// }


class KraftTimeZonePicker extends Picker
{

    const SORT_NAME   = 0;
    const SORT_OFFSET = 1;

    public $template = '{name} {offset}';

    public $sortBy = 0;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $timeZones = [];
        $timeZonesOutput = [];
        $now = new \DateTime('now', new \DateTimeZone('UTC'));

        foreach (\DateTimeZone::listIdentifiers(\DateTimeZone::ALL) as $timeZone) {
            $now->setTimezone(new \DateTimeZone($timeZone));
            $timeZones[] = [$now->format('P'), $timeZone];
        }

        if($this->sortBy == static::SORT_OFFSET)
            array_multisort($timeZones);
        
        $timeZonesOutput[$timeZone[1]] = ['' => ''];
        foreach ($timeZones as $timeZone) {
            $content = preg_replace_callback("/{\\w+}/", function ($matches) use ($timeZone) {
                switch ($matches[0]) {
                    case '{name}':
                        return $timeZone[1];
                    case '{offset}':
                        return $timeZone[0];
                    default:
                        return $matches[0];
                }
            }, $this->template);
            $timeZonesOutput[$timeZone[1]] = $content;
        }

        echo Html::activeDropDownList($this->model, $this->attribute, $timeZonesOutput, $this->options);
    }
}