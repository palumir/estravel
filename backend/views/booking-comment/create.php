<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\BookingComment */

$this->title = 'Create Booking Comment';
$this->params['breadcrumbs'][] = ['label' => 'Booking Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-comment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
