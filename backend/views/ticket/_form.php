<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\User;
use backend\models\Ticket;

/* @var $this yii\web\View */
/* @var $model backend\models\Ticket */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ticket-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'admin_id')->dropDownList(ArrayHelper::map(User::find()->select('id, username')->where(['role' => [User::ROLE_ADMIN]])->all(), 'id', 'username'))->label('Assigned Admin') ?>
    
    <?= $form->field($model, 'status')->dropDownList([Ticket::STATUS_ASSIGNED => Ticket::ASSIGNED, Ticket::STATUS_RESOLVED => Ticket::RESOLVED, Ticket::STATUS_FAILED => Ticket::FAILED]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Assign', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
