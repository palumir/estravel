<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ticket */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'user_id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-hover table-condensed'],
        'attributes' => [
            'id',
                [
                    'attribute' => 'admin_id',
                    'format' => 'text',
                    'label' => 'Assigned Admin',
                    'value' => ($model->admin)?$model->admin->username:"Not Yet Assigned",
                ],      
            'title',
            'body:html:Ticket Text',
                [
                    'attribute' => 'status',
                    'format' => 'html',
                    'label' => 'Ticket Status',
                    'value' => "<p class=".$model->colorText().">".$model->getStatusString()."</p>",
                ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>

<?php $form = ActiveForm::begin([
    'action' => ['ticket-comment/post', 'ticket_id' => $model->id],
    'options' => ['class' => 'ticket-comment-form'],
]); ?>

<?= $form->field($comment, 'body')->textarea(['maxlength' => 1000])->label(false)->error(false) ?>
<?= $form->field($comment, 'ticket_id')->hiddenInput(['value' => $model->id])->label(false)->error(false); ?>

<div class="form-group">
    <?= Html::submitButton('Post Comment', ['class' => 'btn btn-primary btn-csgo btn-block']) ?>
</div>

<?php ActiveForm::end(); ?>

<?php Pjax::begin(['id' => 'pjax-comments']); ?>

    <?=
        ListView::widget([
            'summary' => false,
            'dataProvider' => $commentDataProvider,
            'itemOptions' => ['class' => 'comment'],
            'itemView' => '_comment',
            'viewParams' => ['ticket' => $model],
        ]);
    ?>
<?php Pjax::end(); ?>