<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tickets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ticket', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        // 'filterPosition' => false,
        'columns' => [
            ['class' => 'backend\widgets\KraftActionColumn',
            'header' => 'Administrative Actions',
            'filter' => false,
            ],

            'id',
                [
                    'attribute' => 'user_id',
                    'format' => 'text',
                    'label' => 'User',
                    'value' => function($model){
                        return $model->user->username;
                    },
                ],                
                [
                    'attribute' => 'admin_id',
                    'format' => 'text',
                    'label' => 'Assigned Admin',
                    'value' => function($model){
                        return ($model->admin)?$model->admin->username:"Not Yet Assigned";
                    },
                ],
            'title',
                [
                    'attribute' => 'status',
                    'format' => 'html',
                    'label' => 'Ticket Status',
                    'value' => function($model){
                        return "<p class=".$model->colorText().">".$model->getStatusString()."</p>";
                    },
                ],            
            'created_at',
            'updated_at',
        ],
    ]); ?>

</div>
