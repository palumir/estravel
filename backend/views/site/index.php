<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

/* @var $this yii\web\View */

$this->title = 'Travel';
?>
<div class="site-index">

    <div class="body-content">
        <div class="row">
            <div class="col-lg-4">
                <h2 class="text-center">New Bookings</h2>
                <?php
					echo ListView::widget([
                    'summary' => false,
                    'dataProvider' => $bookingDataProvider,
                    'itemView' => function ($model, $key, $index, $widget) {
						$username = "";
						if(isset($model->user)) {
							$username = "User: " . $model->user->username;
						}
						else {
							$username = "Logged-out Session";
						}
                        return "<a href='".Url::to(['booking/view', 'id' => $model->id])."'><p class='text-center'>" . $username ."</p></a>";
                    },
                    ]);
                ?>
                <p><?= Html::a("View more", ['booking/index'], ['class' => 'btn btn-sm btn-block btn-default']); ?></p>
            </div>
            <div class="col-lg-4">
                <h2 class="text-center">Upcoming events</h2>
                <?php
                echo ListView::widget([
                    'summary' => false,
                    'dataProvider' => $eventDataProvider,
                    'itemView' => function ($model, $key, $index, $widget) {
                        return "<a href='".Url::to(['event/view', 'id' => $model->id])."'><p class='text-center'>" . $model->name."</p></a>";
                    },
                    ]);
                ?>
                <p><?= Html::a("View more", ['event/index'], ['class' => 'btn btn-sm btn-block btn-default']); ?></p>
            </div>
            <div class="col-lg-4">
                <h2 class="text-center">Latests Comments</h2>
                <?php
                echo ListView::widget([
                    'summary' => false,
                    'dataProvider' => $commentDataProvider,
                    'itemView' => function ($model, $key, $index, $widget) {
                        return "<a href='".Url::to(['comment/view', 'id' => $model->id])."'><p>".$model->body."</p></a>";
                    },
                    ]);
                ?>
                <p><?= Html::a("View more", ['comment/index'], ['class' => 'btn btn-sm btn-block btn-default']); ?></p>
            </div>
        </div>

    </div>
</div>
