<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Article;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Write Article', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'admin.username:text:Author',
            'title',
            [
                'attribute' => 'published',
                'filter' => Article::publishedDropDownList(),
                'value' => function ($model) {
                    return $model->getPublishedString();
                },
            ],

            ['class' => 'backend\widgets\KraftActionColumn'],
        ],
    ]); ?>

</div>
