<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Event */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a('Carousel', ['carousel/create', 'event_id' => $model->id], ['class' => 'btn btn-success pull-right']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'intro',
            'logo',
            'image',
            'thumbnail',
            'background',
            'summary',
            'description',
            'status',
            'timezone',
            [
                'label' => 'Default Airport',
                'value' => ($model->airport)?$model->airport->name . ", " . $model->airport->city . ", " . $model->airport->country . " - " . $model->airport->iata_code:NULL,
            ],
            'start_time',
            'end_time',
            'default_departure_date',
            'default_return_date',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'city',
            'venue',
            'address_line1',
            'address_line2',
            'zipcode',
            'country.name',
        ],
    ]) ?>

</div>
