<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Event;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        // 'id' => null,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        // 'afterRow' => null,
        // 'beforeRow' => null,
        // 'caption' => null,
        // 'captionOptions' => null,
        // 'dataColumnClass' => null,
        // 'emptyCell' => null, // HTML
        // 'emptyText' => null, // HTML
        // 'emptyTextOptions' => null, 
        // 'filterErrorOptions' => null, 
        // 'filterErrorSummaryOptions' => null, 
        // 'filterPosition' => null, 
        // 'filterRowOptions' => null, 
        // 'filterSelector' => null, 
        // 'filterUrl' => null, 
        // 'footerRowOptions' => null, 
        // 'formatter' => null, 
        // 'headerRowOptions' => [], 
        // 'layout' => null, 
        // 'options' => null, 
        // 'pager' => null, 
        // 'rowOptions' => null, 
        // 'showFooter' => false, 
        // 'showHeader' => null, 
        // 'showOnEmpty' => null, 
        // 'sorter' => null, 
        // 'stack' => null, 
        // 'summary' => null, 
        // 'summaryOptions' => null, 
        // 'tableOptions' => null, 
        // 'view' => null, 
        // 'viewPath' => null, 
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            [
                'attribute' => 'status',
                'format' => 'text',
                // 'header' => null,
                // 'footer' => null,
                // 'visible' => true,
                'label' => 'Status',
                'filter' => Event::statusDropDownArray(),
                'value' => function($model) {
                    return $model->getStatusString();
                },
                // 'valueOptions' => "",
                // 'filterOptions' => "",
                'footer' => null,
                // 'footerOptions' => null,
                // 'headerOptions' => null,
                // 'grid' => null, // the gridview object this column belongs to
                'options' => null,
                'visible' => true,
            ],
            'start_time',
            'end_time',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
            // 'country',
            // 'city',
            // 'address_line1',
            // 'address_line2',
            // 'zipcode',

            ['class' => 'backend\widgets\KraftActionColumn'],
        ],
    ]); ?>

</div>
