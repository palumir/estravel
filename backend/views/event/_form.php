<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use common\models\Country;
use backend\models\Event;
use backend\models\Game;
use backend\widgets\KraftTimeZonePicker;
use dosamigos\datetimepicker\DateTimePicker;
use dosamigos\multiselect\MultiSelect;
use kartik\select2\Select2;
use yii\web\JsExpression;
use common\models\Airport;

/* @var $this yii\web\View */
/* @var $model backend\models\Event */
/* @var $form yii\widgets\ActiveForm */

$airport_url = \yii\helpers\Url::to(['airport/ajax-list']);

?>

<div class="event-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'row',
            'enctype' => 'multipart/form-data',
            ],
        ]); ?>

    <?= $form->field($model, 'name', ['options' => ['class' => 'col-xs-12 col-sm-6']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'intro', ['options' => ['class' => 'col-xs-12 col-sm-12']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, '_logo', ['options' => ['class' => 'col-xs-12 col-sm-3']])->fileInput() ?>

    <?= $form->field($model, '_thumbnail', ['options' => ['class' => 'col-xs-12 col-sm-3']])->fileInput() ?>

    <?= $form->field($model, '_image', ['options' => ['class' => 'col-xs-12 col-sm-3']])->fileInput() ?>

    <?= $form->field($model, '_background', ['options' => ['class' => 'col-xs-12 col-sm-3']])->fileInput() ?>

    <?= $form->field($model, 'summary', ['options' => ['class' => 'col-xs-12 col-sm-12']])->widget('yii\imperavi\Widget') ?>

    <?= $form->field($model, 'description', ['options' => ['class' => 'col-xs-12 col-sm-12']])->widget('yii\imperavi\Widget') ?>

    <?= $form->field($model, 'timezone', ['options' => ['class' => 'col-xs-12 col-sm-4']])->widget(KraftTimeZonePicker::className(), ['options' => ['class' => 'form-control'],]) ?>

    <?= $form->field($model, 'start_time', ['options' => ['class' => 'col-xs-12 col-sm-4']])->widget(DateTimePicker::className()) ?>

    <?= $form->field($model, 'end_time', ['options' => ['class' => 'col-xs-12 col-sm-4']])->widget(DateTimePicker::className()) ?>

    <?php echo $form->field($model, 'airport_id', ['options' => ['class' => 'col-xs-12 col-sm-4']])->widget(Select2::classname(), [
                // 'initValueText' => $airport, // set the initial display name
                // 'theme' => Select2::THEME_DEFAULT,
                // 'options' => ['placeholder' => 'Your preferred airport ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => $airport_url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(airport) { return airport.name; }'),
                    'templateSelection' => new JsExpression('function (airport) { return airport.name; }'),
                ],
            ]); ?>

    <?= $form->field($model, 'default_departure_date', ['options' => ['class' => 'col-xs-12 col-sm-4']])->widget(DateTimePicker::className()) ?>

    <?= $form->field($model, 'default_return_date', ['options' => ['class' => 'col-xs-12 col-sm-4']])->widget(DateTimePicker::className()) ?>

    <?= $form->field($model, 'country_id', ['options' => ['class' => 'col-xs-12 col-sm-3']])->dropDownList(Country::find()->select(['name', 'id'])->indexBy('id')->column(), ['prompt' => 'Select Country'] ) ?>

    <?= $form->field($model, 'city', ['options' => ['class' => 'col-xs-12 col-sm-3']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'venue', ['options' => ['class' => 'col-xs-12 col-sm-3']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zipcode', ['options' => ['class' => 'col-xs-12 col-sm-3']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_line1', ['options' => ['class' => 'col-xs-12 col-sm-6']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_line2', ['options' => ['class' => 'col-xs-12 col-sm-6']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, '_gamesArray', ['options' => ['class' => 'col-xs-12 col-sm-12']])->widget(MultiSelect::className(), ['data' => Game::getDropDownArray(), 'value' => $model->_gamesArray, 'options' => ['multiple' => "multiple"], 'clientOptions' => ['numberDisplayed' => 10]]) ?>

    <?= $form->field($model, 'status', ['options' => ['class' => 'col-xs-12 col-sm-4 col-sm-offset-4']])->dropDownList(Event::statusDropDownArray(), []) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-block btn-success' : 'btn btn-block btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
