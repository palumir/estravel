<?php

namespace common\adapters;

use common\adapters\BookingAdapter;
use yii\helpers\Json;

abstract class FaregrabbrAdapter implements Faregrabbr {

    // various URLS used in child classes
    const REMOTE_FLIGHT_SEARCH_URL = "http://dev.faregrabbr.com/remoteflightsearch.php";
    const REMOTE_FLIGHT_BOOKING_URL = "http://dev.faregrabbr.com/remoteairbook.php";
    const REMOTE_HOTEL_SEARCH_URL = "http://dev.faregrabbr.com/remotehotelsearch.php";
    const REMOTE_HOTEL_DETAILS_URL = "http://dev.faregrabbr.com/remotehoteldetails.php";
    const REMOTE_HOTEL_BOOKING_URL = "";
    const REMOTE_TRIP_SEARCH_URL = "http://dev.faregrabbr.com/remotegettrips.php";


    public $request;
    public $response;
    // holds the params for the request
    public $params;
    // holds the decoded json
    public $data;

    // handle errors
    public $error;
    public $message;

    public function validate()
    {
        return true;
    }

    public function buildContext()
    {
       // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($this->params),
            ),
        );
        $context = stream_context_create($options);

        return $context;
    }

    public function queryFaregrabbr($url)
    {
        $context = $this->buildContext();
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { 
            $this->addError("Faregrabbr response error", "Faregrabbr API failed to respond.");
            return false;
        } else {
            $this->data = Json::decode($result);
            return true;
        }
    }

    public function hasError()
    {
        if (isset($this->error)) {
            return true;
        }
    }

    public function addError($name, $message)
    {
        $this->error = $name;
        $this->message = $message;
    }

}