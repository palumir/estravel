<?php

namespace common\adapters;

use yii\base\Model;
use common\adapters\FaregrabbrAdapter;

class FaregrabbrHotelDetails extends FaregrabbrAdapter
{

    public $url;

    public function __construct($hotelCode, $hotelChain, $checkin, $numrooms, $checkout)
    {
        $this->url = parent::REMOTE_HOTEL_DETAILS_URL;
        $this->setRequestParams($hotelCode, $hotelChain, $checkin, $numrooms, $checkout);
            if ($this->validate() && $this->queryFaregrabbr($this->url)) {
            }

    }

    public function validate()
    {
        if (parent::validate()) {
            if (isset($this->params)) {
                return true;
            } else {
                $this->addError("Validation error", "Your input could not be validated; please contact the website Administrator.");
                return false;
            }
        }
    }

    private function setRequestParams($hotelCode, $hotelChain, $checkin, $numrooms, $checkout)
    {
        $this->params['hotelCode'] = $hotelCode;
        $this->params['hotelChain'] = $hotelChain;
        $this->params['checkin'] = $checkin;
        $this->params['numrooms'] = $numrooms;
        $this->params['checkout'] = $checkout;
    }

}