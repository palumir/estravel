<?php

namespace common\adapters;

use common\adapters\FaregrabbrAdapter;
use yii\data\ArrayDataProvider;
use Yii;

class FaregrabbrFlightSearch extends FaregrabbrAdapter
{

    public $airlines;
    public $searchresult;
    public $summary;

    private $url;

    public function __construct($form)
    {
        $this->url = parent::REMOTE_FLIGHT_SEARCH_URL;
        $this->setRequestParams($form);
        $cacheKey = self::cacheKey($form);

        if (Yii::$app->cache->exists($cacheKey)) {
            $this->data = Yii::$app->cache->get($cacheKey);
                $this->searchresult = $this->data['searchresult'];
                $this->airlines = $this->data['airlines'];
                $this->summary = $this->data['summary'];
        } else {
            if ($this->validate() && $this->queryFaregrabbr($this->url)) {
                // give each flight a temporary search id to be used or stored inside the cache
                foreach ($this->data['searchresult'] as $key => $value) {
                    $this->data['searchresult'][$key]['search_id'] = $key;
                }
                $this->searchresult = $this->data['searchresult'];
                $this->airlines = $this->data['airlines'];
                $this->summary = $this->data['summary'];
                Yii::$app->cache->set($cacheKey, $this->data, 360);
            }
        }

    }

    public function validate()
    {
        if (parent::validate()) {
            if (isset($this->params)) {
                return true;
            } else {
                $this->addError("Validation error", "Your input could not be validated; please contact the website Administrator.");
                return false;
            }
        }
    }

    public function getDataProvider($sort = false)
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->searchresult,
            'sort' => $sort,
            'pagination' => false,
            ]);

        return $dataProvider;
    }

    public static function cacheKey($form)
    {
        return Yii::$app->user->id . "-" 
            . $form->origin . "-"
            . $form->destination . "-"
            . $form->departure . "-"
            . $form->return . "-"
            . $form->adults . "-"
            . $form->children . "-"
            . $form->infants . "-"
            . $form->class;
    }

    private function setRequestParams($form)
    {
        $this->params['origin'] = $form->origin;
        $this->params['destination'] = $form->destination;
        $this->params['departure'] = $form->departure;
        // API requires return to not be sent to return search without return flights
        if (!empty($form->return)) {$this->params['return'] = $form->return;}
        $this->params['adults'] = $form->adults;
        $this->params['children'] = $form->children;
        $this->params['infants'] = $form->infants;
        $this->params['class'] = $form->class;
    }

}