<?php

namespace common\adapters;

use common\adapters\FaregrabbrAdapter;
use yii\data\ArrayDataProvider;
use Yii;

class FaregrabbrHotelSearch extends FaregrabbrAdapter
{

    public $url;

    public function __construct($form)
    {
        $this->url = parent::REMOTE_HOTEL_SEARCH_URL;
        $this->setRequestParams($form);

        if ($this->validate() && $this->queryFaregrabbr($this->url)) {
            $this->currency = $this->data['currency'];
            $this->searchresult = $this->data['searchresult'];
            $this->status = $this->data['status'];
        }
    }

    public function validate()
    {
        if (parent::validate()) {
            if (isset($this->params)) {
                return true;
            } else {
                $this->addError("Validation error", "Your input could not be validated; please contact the website Administrator.");
                return false;
            }
        }
    }


    public function getDataProvider($sort = false)
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->searchresult,
            'sort' => $sort,
            'pagination' => false,
            ]);

        return $dataProvider;
    }

    private function setRequestParams($form)
    {
        $this->params['location'] = $form->location;
        $this->params['locationType'] = $form->locationType;
        $this->params['checkin'] = $form->checkin;
        $this->params['numrooms'] = $form->numrooms;
        $this->params['checkout'] = $form->checkout;
    }

}